﻿using System.Web;
using System.Web.Optimization;

namespace DAMASIO.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = true;

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.css",
                      "~/Content/bootstrap-datepicker.css"));

            bundles.Add(new ScriptBundle("~/bundles/inputmask").Include(
                     "~/Scripts/jquery.inputmask/inputmask.js",
                     "~/Scripts/jquery.inputmask/jquery.inputmask.js",
                                 "~/Scripts/jquery.inputmask/inputmask.extensions.js",
                                 "~/Scripts/jquery.inputmask/inputmask.date.extensions.js",
                                 "~/Scripts/jquery.inputmask/inputmask.numeric.extensions.js"));

            bundles.Add(new ScriptBundle("~/js/formulario")
               .Include("~/Scripts/jquery.validate.js")
               .Include("~/Scripts/jquery.validate.unobtrusive.js")
               .Include("~/Scripts/jquery.validate.custom.pt-br.js")
               .Include("~/Scripts/jquery.mask.js")
               .Include("~/Scripts/bootstrap-datepicker.js")
               .Include("~/Scripts/bootstrap-datepicker.pt-BR.min.js"));
        }
    }
}
