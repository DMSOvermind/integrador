﻿using System.Web;
using System.Web.Mvc;

using DAMASIO.Integracao.ActionFilters;

namespace DAMASIO.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new IsDominioAPI());
        }
    }
}
