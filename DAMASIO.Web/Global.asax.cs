﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using DAMASIO.Integracao.Utils;

using DAMASIO.Web.Models;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using FluentScheduler;
using DAMASIO.Integracao.Negocio.Schedulers;

namespace DAMASIO.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Incluido para validar os posts via AJAX
            GlobalFilters.Filters.Add(new ValidateAntiForgeryTokenPost() );

            //GlobalConfiguration.Configuration.Formatters.Insert(0, new XmlMediaTypeFormatter());

            //ValueProviderFactories.Factories.Add(new DAMASIO.Integracao.Utils.JsonValueProviderFactory());
            //ValueProviderFactories.Factories.Add(new XmlValueProviderFactory());


            ModelBinderProviders.BinderProviders.Add(new XmlModelBinderProvider());


            //Permitir que seja exportado json ou xml
            //ex: http://site.com.br/controler/param/?output=json
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.MediaTypeMappings.Add(new QueryStringMapping("output", "json", new MediaTypeHeaderValue("application/json")));
            GlobalConfiguration.Configuration.Formatters.XmlFormatter.MediaTypeMappings.Add(new QueryStringMapping("output", "xml", new MediaTypeHeaderValue("application/xml")));


          

            //Inicializa o scheduler
            //JobManager.Initialize(new taskGetPedidos()); 
            //---------------------------------------------------


        }
    }
}
