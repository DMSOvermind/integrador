﻿using DAMASIO.Integracao.DAL;
using DAMASIO.Integracao.Dominio.API;
using DAMASIO.Integracao.Dominio.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAMASIO.Web.Models.API
{
    public class GridPedidos
    {
        public List<GridPedido> Pedidos { get; set; }
        public List<HistoricoItemPedido> HistoricoItem { get; set; }

    }


    public class GridLogs
    {
        public List<GridLog> Logs { get; set; }
        public List<HistoricoItemLog> HistoricoItemLog { get; set; }


    }


    public class ModelGridHistoricoItem {

        public List<HistoricoItemPedido> Historico { get; set; }
    
    }

    public class FiltroGRID {

        public bool Pendente { get; set; }
        public bool ErroEnvio { get; set; }
        public bool EnviadoSucesso { get; set; }
        public bool Cancelado { get; set; }
        public bool NaoEnviado { get; set; }
        public string DataMin { get; set; }
        public string DataMax { get; set; }
    
    }


    public class FiltroGRIDlog
    {

        public bool Erro { get; set; }
        public bool Novo { get; set; }
        public bool IntegradoSucesso { get; set; }
        public bool IntegracaoPendente { get; set; }
        public bool TransacoesDestinoSIGA { get; set; }
        public bool TransacoesDestinoSITE { get; set; }
        public string DataMin { get; set; }
        public string DataMax { get; set; }

    }


    public class FiltroHistorico {

        public Int64 CodigoItem { get; set; }
        public Int64 CodigoPedido { get; set; }
    
    }

    public class PedidoStatus
    {

        public Int64 CodigoItem { get; set; }
        public Int64 CodigoPedido { get; set; }
        public int CodigoStatus { get; set; }
        public string MotivoCancelamento { get; set; }

    }

    public class ProdutoReenvio
    {
        public Int64 CodigoPedido { get; set; }
        public Int64 CodigoProduto { get; set; }

    }



    public class ModelAlunoDetalhe {

        public Aluno aluno { get; set; }
        public PEDIDO Pedido { get; set; }
            
    }


    public class ModelPedidoDetalhe
    {
        public Retorno retorno { get; set; }
        public PedidoSite pedido { get; set; }

    }

}