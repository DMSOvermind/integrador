﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DAMASIO.Web.Controllers
{
    public class FormsController : Controller
    {
        public ActionResult Error() {

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Bad Request");
        
        }
    }
}