﻿using DAMASIO.Integracao.Dominio.API;
using DAMASIO.Integracao.Negocio;
using DAMASIO.Integracao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DAMASIO.Web.Controllers
{
    public class AtualizarInfoTurmaController : ApiController
    {
        
        public HttpResponseMessage Post(Int16 CodigoEmpresa,Int32 CodigoTurma, Int32 CodigoCurso, string Conteudo, Int32 CargaHoraria, string TipoCurso, Decimal ValorTurma , int CodigoCategoria1,int CodigoCategoria2, int CodigoCategoria3)
            {
                var integraSiga = new wsSiga.WsIntegracaoPortalSoapClient();
                Retorno retorno = new Retorno();

                String StrXmlCategoria = "";
                String StrxmlCat1 = "";
                String StrxmlCat2 = "";                
                String StrxmlCat3 = "";

                Logs log = new Logs { DATA_CRIACAO = DateTime.Now, ROTINA = "AtualizarInfoTurma", NOME_USUARIO = "n/a", IND_ERRO = false, PARAMETROS_INTEGRACAO = "-", DESTINO_INTEGRACAO = "INTEGRADOR", IND_EXIBE_LOG = true };

                try
                {

                    string codigoTurmaFormatado = String.Concat("00000", CodigoTurma);
                    codigoTurmaFormatado = codigoTurmaFormatado.Substring(codigoTurmaFormatado.Length -5);

                    string NomeTurma, strDataInicio = "";
                    var turma = integraSiga.ObterTurma(CodigoEmpresa, codigoTurmaFormatado);


                    if (turma != null){
                        NomeTurma = turma.Nome.Replace("'",""); // Por limitação do BO da BSeller o nome da turma é importado originalmente com 30 caracteres, então, ao atualizar a turma, devemos recolocar o nome original no front
                        strDataInicio = turma.Inicio.ToShortDateString(); //pegar o formato dd/MM
                    }
                    else
                    {
                        retorno.codigo = CodigoTurma;
                        retorno.message = "Falha ao consultar a turma no SIGA";
                        retorno.success = false;
                        
                        log.PARAMETROS_INTEGRACAO = "-";
                        log.MENSAGEM = retorno.message;
                        log.IND_ERRO = true;
                        log.Inserir();

                        return Request.CreateResponse(HttpStatusCode.BadRequest, retorno);
                    }

                    var rotina = new Rotinas().GetByID((int)Listas.Rotinas.AtualizarProduto);
                    string vlrFormatadoTurma = "0.01";
                    if (ValorTurma > 0)
                        vlrFormatadoTurma = ValorTurma.ToString().Replace(",", ".");
                    
                    if (rotina != null)
                    {

                        var produto = new Produtos().ConsultarIDexterno(codigoTurmaFormatado);

                        if (produto == null)
                        {

                            retorno.codigo = CodigoTurma;
                            retorno.message = "Código da Turma (produto) não encontrado no site";
                            retorno.success = false;

                            log.PARAMETROS_INTEGRACAO = "-";
                            log.MENSAGEM = retorno.message;
                            log.IND_ERRO = true;
                            log.Inserir();

                            return Request.CreateResponse(HttpStatusCode.BadRequest, retorno);
                        
                        }

                        if (NomeTurma.Length == 0)
                            NomeTurma = produto.name;
                        
                        Empresa empresa = new Empresas().ObterPeloCodigoSiga(CodigoEmpresa);
                        if (empresa == null)
                        {
                            retorno.codigo = CodigoEmpresa;
                            retorno.message = "Empresa Inválida";
                            retorno.success = false;

                            log.PARAMETROS_INTEGRACAO = "-";
                            log.MENSAGEM = retorno.message;
                            log.IND_ERRO = true;
                            log.Inserir();

                            return Request.CreateResponse(HttpStatusCode.BadRequest, retorno);

                        }

                        String vender = "venda";
                        if (Decimal.Compare(ValorTurma, (decimal)0.02) < 0)
                            vender = "gratuito";
                        //implementacao Categoria -------------------------------------------------

                        if (CodigoCategoria1 != 0)
                        {
                            StrxmlCat1 = String.Concat("<category id='", CodigoCategoria1, "' />");

                            if (CodigoCategoria2 != 0)
                                StrxmlCat2 = String.Concat("<secondaryCategories><category id='", CodigoCategoria2, "' />{0}</secondaryCategories>");
                            else
                            {
                                if (CodigoCategoria3 != 0)
                                {
                                    StrxmlCat3 = String.Concat("<category id='", CodigoCategoria3, "' />");
                                    StrxmlCat2 = String.Format(StrxmlCat2, StrxmlCat3);
                                }
                                else
                                    StrxmlCat2 = String.Format(StrxmlCat2, " ");
                            }
                        }
                        else
                        {
                            retorno.codigo = CodigoCategoria1;
                            retorno.message = "Categoria Inválida";
                            retorno.success = false;

                            log.PARAMETROS_INTEGRACAO = "-";
                            log.MENSAGEM = retorno.message;
                            log.IND_ERRO = true;
                            log.Inserir();

                            return Request.CreateResponse(HttpStatusCode.BadRequest, retorno);
                        }


                        //{

                        //    StrxmlCat1 = String.Concat("<category id='", produto.category.id, "' />");

                        //    var listSecCat = produto.secondaryCategories;
                        //    if (listSecCat.Count() > 0)
                        //    {

                        //        StrxmlCat2 = "<secondaryCategories>";

                        //        String categ = "";
                        //        foreach (var cat in listSecCat)
                        //        {

                        //            categ += String.Format("<category id='{0}' />", cat.id);

                        //        }

                        //        StrxmlCat2 = String.Concat(StrxmlCat2, categ, "</secondaryCategories>");
                        //    }

                        //}



                        //-------------------------------------------------------------------------

                        StrXmlCategoria = String.Concat(StrxmlCat1, StrxmlCat2);

                        var xml = String.Format(rotina.XML_REQUISICAO, produto.id, produto.externalId, NomeTurma, vlrFormatadoTurma, Conteudo, CargaHoraria.ToString(), CodigoCurso.ToString(), vender, TipoCurso.ToString(), empresa.CodigoMarcaBSeller.ToString(), StrXmlCategoria, "true"); //3181=Paulista - marca
                        xml = xml.Replace("dd-mm-yyyy", strDataInicio); //Ateração para incluir o campo data de inicio da turma

                        xml = xml.Replace("<description></description>", "<description>" + " descrição da turma" + "</description>");
                        xml = xml.Replace("<shortName></shortName>", "<shortName>" + " " + "</shortName>");
                        xml = xml.Replace("<images>", "");

                        //Alteração da ficha técnica

                                xml = xml.Replace("MarketingDiferenciais", "Marketing");

                                xml = xml.Replace("MarketingObjetivos", "Marketing");

                                xml = xml.Replace("MarketingRequisitos", "Marketing");
                                xml = xml.Replace("MarketingVideo", "Marketing");


                        

                        //------------------------




                        Retorno ret = (Retorno)new EventosREST().Request(xml, rotina, (Int32)produto.id, false);


                        //retorno.codigo = produto.id;
                        //retorno.message = xml;
                        //retorno.success = false;

                        //return Request.CreateResponse(HttpStatusCode.BadRequest, retorno);

                        //new Logs().Inserir(

                        //    new Integracao.DAL.LOG_INTEGRACAO
                        //    {
                        //        DATA_CRIACAO = DateTime.Now,
                        //        COD_PRODUTO = CodigoTurma,
                        //        MENSAGEM = "Requisicão de atualização do produto - SIGA",
                        //        XML = xml,
                        //        ROTINA = "AtualizarInfoTurma"
                        //    }

                       // );

                        retorno.message = "Falha na atualização da turma(produto)";
                        retorno.MessageRequest = ret.MessageRequest;
                        retorno.StatusCodeRequest = ret.StatusCodeRequest;

                        log.COD_PRODUTO = CodigoTurma;
                        log.DESCRICAO_INTEGRACAO = String.Concat(turma.IdTurma, "-", turma.Nome);

                        log.PARAMETROS_INTEGRACAO = xml;
                        if (ret.StatusCodeRequest != "Bad Request")
                        {
                            
                            log.MENSAGEM = "Turma atualizada com sucesso!";
                            log.IND_ERRO = false;
                            log.Inserir();
                            return Request.CreateResponse(HttpStatusCode.OK, ret);
                        }

                        else
                        {
                            log.MENSAGEM = "Falha na atualização da turma(produto)";
                            log.IND_ERRO = true;
                            log.Inserir();

                            return Request.CreateResponse(HttpStatusCode.BadRequest, retorno);
                        }


                            

                    }
                    else
                    {

                        retorno.codigo = CodigoTurma;
                        retorno.message = "Rotina Inválida";
                        retorno.success = false;

                        return Request.CreateResponse(HttpStatusCode.BadRequest, retorno);

                    }


                }
                catch (Exception ex)
                {

                    retorno.codigo = CodigoTurma;
                    retorno.message = ex.Message;
                    retorno.success = false;
                    return Request.CreateResponse(HttpStatusCode.OK, retorno);
                }

            }
        }


    
}
