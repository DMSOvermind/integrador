﻿using DAMASIO.Integracao.ActionFilters;
using DAMASIO.Integracao.Dominio.API;
using DAMASIO.Integracao.Negocio;
using DAMASIO.Integracao.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DAMASIO.Web.Controllers
{
    public class ContratoController : ApiController
    {

        [IsDominioAPI]
        public HttpResponseMessage Post(Int64 CodigoContrato)
        {
            Retorno retorno = new Retorno();
            try {
                retorno.success = false;

                if(CodigoContrato==0 || CodigoContrato == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, retorno);

                var resp = new Pedidos().ObterDetalhePedidoPeloContrato(CodigoContrato);
                if (resp != null)
                {
                    retorno.codigo = resp.codigo;
                    retorno.message = resp.message;
                    retorno.success = resp.success;
                    retorno.ResponseServer = resp.ResponseServer;
                    retorno.StatusCodeRequest = HttpStatusCode.OK.ToString();
                }
                else
                {
                    retorno.StatusCodeRequest = HttpStatusCode.BadRequest.ToString();
                    retorno.message = "Ocorreu alguma falha não identificada";
                    retorno.success = false;
                }

                return Request.CreateResponse(HttpStatusCode.OK, retorno);

            
            }
            catch (Exception ex) {

                retorno.success = false;
                retorno.message = ex.Message;
                retorno.StatusCodeRequest = HttpStatusCode.BadRequest.ToString();

                return Request.CreateResponse(HttpStatusCode.InternalServerError, retorno);
            
            
            }

        
        
        }


    }
}
