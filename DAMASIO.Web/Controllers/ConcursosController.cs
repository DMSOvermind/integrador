﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Web.Services;

using System.Web.Http.Filters;

using DAMASIO.Integracao.Dominio.API;
using DAMASIO.Web.wsSiga;


using DAMASIO.Integracao.ActionFilters;

namespace DAMASIO.Web.Controllers
{
    

    public class ConcursosController : ApiController
    {
       
        private string getEscolaridade(EscolaridadeDTO escolaridade) {


            switch ((int)escolaridade) {
            
                case 1:
                    return "Fundamental Completo";
                case 2:
                    return "Ensino médio incompleto";
                case 3:
                    return "Ensino médio completo";
                case 4:
                    return "Superior Incompleto";
                case 5:
                    return "Superior Completo";
                case 6:
                    return "Especialização Residência";
                case 7:
                    return "Mestrado";
                case 8:
                    return "Doutorado";
                default:
                    return "-";
            
            }

            
        
        
        }
        
        public HttpResponseMessage Post([FromBody] DAMASIO.Integracao.Dominio.dto.PesquisaConcursoDTO value)
        {

            var siga = new wsSiga.WsIntegracaoPortalSoapClient();
            FaixaSalarioDTO faixa = new FaixaSalarioDTO();
            StatusConcursoDTO statusConcurso = new StatusConcursoDTO();
            AbrangenciaDTO abrangencia = new AbrangenciaDTO();
            EscolaridadeDTO escolaridade = new EscolaridadeDTO();


            if (value != null) {

                try
                {
                    if (value.SalarioDE != null)
                        faixa.De = Convert.ToDecimal(value.SalarioDE);

                    if (value.SalarioATE != null)
                        faixa.Ate = Convert.ToDecimal(value.SalarioATE);

                    if (value.SalarioDE == null && value.SalarioATE == null)
                        faixa = null;
                    else
                        if (faixa.De > faixa.Ate)
                            return Request.CreateResponse(HttpStatusCode.OK, "Verifique a faixa salarial informada");

                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Erro na conversão da faixa salarial");
                }


                try
                {
                    statusConcurso = (StatusConcursoDTO)value.Status;
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Erro na conversão do status do concurso");
                }


                try
                {
                    abrangencia = (AbrangenciaDTO)value.Abrangencia;
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Erro na conversão do status da abrangência");
                }


                try
                {
                    escolaridade = (EscolaridadeDTO)value.Escolariadade;
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Erro na conversão do status da escolaridade");
                }
            
            }

            try {

                var param = new wsSiga.PesquisaConcursoDTO();

                if (value != null)
                {

                    param.NomeConcurso = value.NomeConcurso;
                    param.Abrangencia = abrangencia;
                    param.BancaOrganizadora = value.Banca;
                    param.Escolaridade = escolaridade;
                    param.FaixaSalario = faixa;
                    param.Localidade = value.Localidade;
                    param.NomeCargo = value.NomeCargo;
                    param.StatusConcurso = statusConcurso;

                }
                else 
                    param = null;

                var Concursos = siga.ObterConcursos(param);

                List<Concursos> ListaConcurso = new List<Concursos>();

                if (Concursos != null) {

                    var lstConcursos = Concursos.OrderBy(x => x.Nome).ToList();

                    foreach (var concurso in lstConcursos)
                    {

                        Concursos c = new Concursos();
                        c.NomeConcurso = concurso.Nome;
                        c.Status = concurso.Status.ToString();

                        if (concurso.Cargos != null) {
                            c.Cargo = new List<Cargo>();

                            foreach (var cargo in concurso.Cargos)
                            {
                                if (cargo.GruposCurso != null)
                                {
                                    c.GrupoDeCurso = new List<GrupoDeCurso>();

                                    foreach (var grpCurso in cargo.GruposCurso) {

                                        var grpC = new GrupoDeCurso();
                                        grpC.Nome = grpCurso;

                                        c.GrupoDeCurso.Add(grpC);
                                    }
                                
                                }

                                if (cargo.Cursos != null)
                                {
                                    c.Cursos = new List<Curso>();

                                    foreach (var cs in cargo.Cursos)
                                    {
                                        var curso = new Curso();
                                        
                                        curso.Codigo = cs.IdCurso;
                                        curso.Descricao = cs.Nome;
                                        
                                        c.Cursos.Add(curso);

                                    }

                                }

                                if (cargo.Turmas != null) {

                                    c.Turmas = new List<Turma>();

                                    foreach (var tu in cargo.Turmas)
                                    {
                                        var turma = new Turma();
                                        turma.Codigo = tu.IdTurma;
                                        turma.Descricao = tu.Nome;

                                        c.Turmas.Add(turma);
                                    }
                                
                                }

                                Cargo Cargo = new Cargo();

                                Cargo.Nome = cargo.Nome;
                                Cargo.NumeroDeVagas = cargo.NumVagas;


                                //Cargo.Escolaridade = cargo.Escolaridade.ToString();
                                Cargo.Escolaridade = getEscolaridade(cargo.Escolaridade);

                                if (cargo.FaixaSalario != null)
                                {
                                    Cargo.SalarioDE = Convert.ToDecimal(cargo.FaixaSalario.De);
                                    Cargo.SalarioATE = Convert.ToDecimal(cargo.FaixaSalario.Ate);
                                }
                                else
                                {
                                    Cargo.SalarioDE = 0;
                                    Cargo.SalarioATE = 0;
                                }

                                Cargo.Requisitos = cargo.Requisitos ?? "";

                                c.Cargo.Add(Cargo);

                            }
                        
                        }



                        if (concurso.Editais != null)
                        {

                            foreach (var edital in concurso.Editais)
                            {

                                c.InicioInscricoes = edital.DataInicioInscricao.ToString();
                                c.TerminoInscricoes = edital.DataTerminoInscricao.ToString();

                                c.TaxaInscricao = edital.TaxaInscricao; // verificar

                                c.UrlOrganizadora = edital.UrlEdital;

                                c.UrlEdital_1 = edital.UrlArquivo1;
                                c.UrlEdital_2 = edital.UrlArquivo2;
                                c.UrlEdital_3 = edital.UrlArquivo3;
                                c.UrlEdital_4 = edital.UrlArquivo4;

                                c.Localidade = edital.Localidade;
                                c.Abrangencia = edital.Abrangencia.ToString();


                                c.Fases = new List<FasesConcurso>();

                                var FasesOrdernadas = edital.Fases.OrderBy(x => Convert.ToDateTime(x.DataProva)).ToList();

                                foreach (var fase in FasesOrdernadas)
                                {


                                    var Fase = new FasesConcurso();
                                    Fase.Nome = fase.Nome ?? "";
                                    Fase.Data = fase.DataProva.ToString() ?? "";

                                    c.Fases.Add(Fase);

                                }



                            }
                        
                        
                        }

                        

                        ListaConcurso.Add(c);
                    };

                
                }else
                    return Request.CreateResponse(HttpStatusCode.NoContent, "");

                return Request.CreateResponse(HttpStatusCode.OK, ListaConcurso);
            
            }
            catch (Exception ex) {
                return Request.CreateResponse(HttpStatusCode.OK, "Falha no processamento: " + ex.Message);
            }

            }

        }


    }

