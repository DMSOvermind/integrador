﻿using DAMASIO.Integracao.Dominio.BSeller;
using DAMASIO.Integracao.Negocio;
using DAMASIO.Integracao.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using System.Xml;

using DAMASIO.Integracao.ActionFilters;
using System.Web.Http.Filters;
using System.Xml.Linq;
using DAMASIO.Integracao.DAL;
using DAMASIO.Integracao.Dominio.API;



namespace DAMASIO.Web.Controllers
{
    public class WorkflowBSellerController : ApiController
    {
        Logs log = new Logs { DATA_CRIACAO = DateTime.Now, NOME_USUARIO = "Envio Automático/BSELLER", IND_ERRO = true, ROTINA = "API - WorkflowBSeller" };

        [LogAPI]
        public HttpStatusCodeResult Post(HttpRequestMessage request)
        {
            //--------------
            //Rotina responsável por fazer as atualizações de status vindas do Workflow da BSeller
            //--------------
            try
            {
                var a = request.Content.ReadAsStreamAsync().Result;
                var xmlNotifica = new StreamReader(a).ReadToEnd();
                a.Position = 0;

                XDocument doc = XDocument.Parse(xmlNotifica);
                doc.Declaration = new XDeclaration("1.0", "ISO-8859-1", null);

                XElement orderNotification = doc.Root;
                var orderElements = orderNotification.Elements();
                var NotificacaoRecebida = new NotificacaoBSeller();
                NotificacaoRecebida.Codigo = orderElements.ElementAt(0).Value;
                NotificacaoRecebida.Status = orderElements.ElementAt(1).Value;
                NotificacaoRecebida.HoraAtualizacao = Convert.ToDateTime(orderElements.ElementAt(2).Value);

                NotificacaoRecebida.Xml = xmlNotifica;

                var Notificacao = new Notificacoes().Inserir(NotificacaoRecebida);

                //NewOrder é processado em outro controller, Cancelled não deve ser atualizado no sistema para manter o Status de pagamento recusado

                if (NotificacaoRecebida.Status != "NEW_ORDER" && NotificacaoRecebida.Status != "CANCELLED")
                {
                    if (Notificacao != null)
                    {
                        if (AtualizarPedidoBSeller(new Pedidos().ConsultarPedido(Notificacao.COD_PEDIDO), NotificacaoRecebida.Status, "Envio Automático"))
                        {
                            //se o pedido for atualizado com sucesso, devemos gerar o contrato
                            if (NotificacaoRecebida.Status == "PAYMENT_APPROVED")
                            {
                                PEDIDO pedidoGravadoSite = new Pedidos().ObterPedido(Convert.ToInt64(NotificacaoRecebida.Codigo));

                                //25-09-2015: Não permitir que reenvios do site alterem o status do pedido para Erro quando o contrato estiver gerado
                                //if(pedidoGravadoSite.COD_CONTRATO_SIGA != null)
                                //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Já existe um contrato gerado para este pedido");

                                //----------------------------------------------------------------------------------------------

                                Order pedidoSite = new Pedidos().ConsultarPedido(NotificacaoRecebida.Codigo);

                                var contrato = new Pedidos().GerarContratoSIGA(pedidoSite, pedidoGravadoSite, "Envio Automático/BSELLER");
                                if (contrato == false)
                                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Falha na geração do contrato");
                            }


                            return new HttpStatusCodeResult(HttpStatusCode.OK, String.Concat("Notificação Recebida com sucesso", "- Código Pedido: ", orderElements.ElementAt(0).Value));
                        }
                        new LOG_INTEGRACAO { COD_PEDIDO_BSELLER = Convert.ToInt64(NotificacaoRecebida.Codigo), DATA_CRIACAO = DateTime.Now, MENSAGEM = "Falha ao atualizar o pedido", ROTINA = "WorkflowBSeller", IND_ERRO = true,DESTINO_INTEGRACAO = "SIGA"};
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest, String.Concat("Falha na atualização do pedido", "- Código Pedido: ", orderElements.ElementAt(0).Value));
                    }
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Falha na atualização");
                }
                if (NotificacaoRecebida.Status == "CANCELLED")
                    AtualizarPedidoCanceladoBSeller(new Pedidos().ConsultarPedido(Notificacao.COD_PEDIDO), "Envio Automático", NotificacaoRecebida.Status);

                return new HttpStatusCodeResult(HttpStatusCode.OK, "Ok");
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                throw new HttpResponseException(response);
            }
        }

        private Boolean AtualizarPedidoCanceladoBSeller(Order order, string nomeUsuario, string descNotificacao)
        {

            try
            {
                //var ret = new Pedidos().ConsultarPedidoPeloCodigo(order.id.ToString());
                var pedido = new Pedido { Codigo = order.id.ToString() };
                return new Pedidos().AtualizarStatusSigaPedidoPeloCodigoBSeller(pedido, (int)Listas.StatusPedidoSiga.NaoEnviado, descNotificacao);
            }
            catch (Exception)
            {
                return false;
            }
        }

        private Boolean AtualizarPedidoBSeller(Order order, string novoStatusPedido, string nomeUsuario)
        {
            try
            {
                var ret = new Pedidos().ObterPedido(Convert.ToInt64(order.id));
                var paymentReceived = order.payment;
                string Autorizacao = "";
                String MsgAutorizacao = "";
                string acTID = "";
                string retCODE = "";

                //Removido porque agora só está vindo na criação do pedido
                if (paymentReceived.income != null) { Autorizacao = paymentReceived.income.authorizationCode; MsgAutorizacao = paymentReceived.income.authorizationMessage; }

                if (order.payment.paymentProps.Count > 0)
                {
                    retCODE = order.payment.paymentProps.Find(p => p.key == "returnCode").value ?? " ";
                    acTID = order.payment.paymentProps.Find(p => p.key == "acquirerTransactionId").value ?? " ";

                }

                var pedido = new Pedidos().AtualizarPedido(new Pedido
                {
                    Codigo = ret.COD_PEDIDO.ToString(),
                    Situacao = novoStatusPedido,
                    CodigoSitucaoBSELLER = new Pedidos().SituacaoBSeller(novoStatusPedido),
                    DataAtualizacao = Convert.ToDateTime(ret.DAT_ATUALIZACAO),
                    Pagamento = new Integracao.Dominio.BSeller.Pagamento
                    {
                        NumeroClienteCartao = order.payment.creditCard.number ?? "",
                        NumeroAutorizacao = Autorizacao,
                        MensagemAutorizacao = MsgAutorizacao,
                        IdTransacao = order.payment.transactionId,
                        AcquirerTransactionID = acTID,
                        ReturnCode = retCODE
                    }

                });

                if (pedido.COD_SITUACAO_BSELLER == (int)Listas.StatusPedidoBSeller.PagamentoRecusado || pedido.COD_SITUACAO_BSELLER == (int)Listas.StatusPedidoBSeller.Cancelado)
                    new Pedidos().AtualizarStatusItensPedido(new Pedido { Codigo = order.id.ToString(), Pagamento = new Integracao.Dominio.BSeller.Pagamento { IdTransacao = order.payment.transactionId, NumeroAutorizacao = "" } }, (int)Listas.StatusPedidoSiga.NaoEnviado, nomeUsuario);

                return pedido != null;
            }
            catch (Exception ex)
            {
                log.COD_PEDIDO_BSELLER = Convert.ToInt64(order.id);
                log.MENSAGEM = "ERRO: Não foi possível atualizar o cabeçalho do pedido.";
                log.ROTINA = "AtualizarPedidoBSeller";
                log.DESCRICAO_INTEGRACAO = ex.Message;
                log.DESTINO_INTEGRACAO = "SIGA";
                log.PARAMETROS_INTEGRACAO = " ";
                log.Inserir();

                //new LOG_INTEGRACAO { COD_PEDIDO_BSELLER = Convert.ToInt64(order.id), DATA_CRIACAO = DateTime.Now, MENSAGEM = "FALHA AO ATUALIZAR PEDIDO - " + ex.Message.ToString(), ROTINA = "WorkflowBSeller" };
                return false;
            }

        }
    }
}
