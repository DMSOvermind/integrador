﻿using DAMASIO.Integracao.Dominio.BSeller;
using DAMASIO.Integracao.Dominio.dto;
using DAMASIO.Integracao.Negocio;
using DAMASIO.Integracao.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace DAMASIO.Web.Controllers
{

    
    public class TurmasController : Controller
    {

        [AllowAnonymous]
        [HttpPost]
        public ActionResult getInfoCompleta(string value)
        {

            try {

                string codigoTurmaFormatado = String.Concat("00000", value);
                codigoTurmaFormatado = codigoTurmaFormatado.Substring(codigoTurmaFormatado.Length - 5);

                ProdutoBSeller produto = new Produtos().getInfoProdutoCompletoBSeller(codigoTurmaFormatado);

                if (produto != null)
                {

                    ProdutoStaMonica retProduto = new ProdutoStaMonica
                    {

                        Nome = produto.name,
                        CodigoTurma = produto.externalId,
                        IdBseller = produto.id,
                        Descricao = produto.description,
                        CategoriaPrincipal = produto.category,
                        Images = produto.images

                    };


                    return Json(retProduto);

                }
                else return Json("Turma não foi encontrada.");
            
            
            }
            catch (Exception ex) { throw new HttpException(400,"Falha no processamento"); }





        
        
        
        }







        
    }
}