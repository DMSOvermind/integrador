﻿using DAMASIO.Integracao.Dominio.API;
using DAMASIO.Integracao.Negocio;
using DAMASIO.Integracao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DAMASIO.Web.Controllers
{
    public class AlterarVisibilidadeTurmaController : ApiController
    {

        public HttpResponseMessage Post(Int32 CodigoTurma, Boolean Visivel)
        {
            var log = new Logs { DATA_CRIACAO = DateTime.Now, ROTINA = "AlterarVisibilidadeTurma", NOME_USUARIO = "Siga/Envio Automático/API", IND_ERRO = false, PARAMETROS_INTEGRACAO = "-", DESTINO_INTEGRACAO = "SITE", IND_EXIBE_LOG = true };
            try
            {
                var integraSiga = new wsSiga.WsIntegracaoPortalSoapClient();

                var rotina = new Rotinas().GetByID((int)Listas.Rotinas.AtualizarProduto);
                var retorno = new Retorno();

                if (rotina != null)
                {
                    string codigoTurmaFormatado = String.Concat("00000", CodigoTurma);
                    codigoTurmaFormatado = codigoTurmaFormatado.Substring(codigoTurmaFormatado.Length - 5);

                    var produto = new Produtos().ConsultarIDexterno(codigoTurmaFormatado,true);

                    if (produto == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent, "Código da turma não encontrado");
                    }
                       

                    string vlrFormatadoTurma = "0.01";
                    if (produto.price.salePrice > 0)
                    {
                        vlrFormatadoTurma = produto.price.salePrice.ToString().Replace(",", ".");
                    }

                    string NomeTurma, strDataInicio = "";
                    var turma = integraSiga.ObterTurma(15, codigoTurmaFormatado);


                    if (turma != null)
                        strDataInicio = turma.Inicio.ToShortDateString().Substring(turma.Inicio.ToShortDateString().Length - 5); //pegar o formato dd/MM
                    else
                    {
                        retorno.codigo = CodigoTurma;
                        retorno.message = "Falha ao consultar a turma no SIGA";
                        retorno.success = false;

                        return Request.CreateResponse(HttpStatusCode.BadRequest, retorno);
                    }
                    String vender;
                    //var hidden = !Visivel;


                    vender = Visivel ? "venda" : "lead";

                    if (Decimal.Compare(produto.price.salePrice, (decimal)0.02) < 0)
                    {
                        vender = "gratuito";
                    }
                       
                    //var xml = String.Format(rotina.XML_REQUISICAO, CodigoTurma, produto.name, produto.price.salePrice.ToString().Replace(",", "."), hidden.ToString().ToLower(), vender.ToString().ToLower());

                    if(produto.info == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Turma não possui atualização comercial enviada para o site");
                    }
                        

                    //var xml = String.Format(rotina.XML_REQUISICAO, produto.id, produto.externalId, produto.name, vlrFormatadoTurma, produto.info.group.property[0].Conteudo.Valor, produto.info.group.property[0].CargaHoraria.Valor, produto.info.group.property[0].Curso.Valor, vender, produto.info.group.property[0].Curso.Valor, produto.brand.id, String.Concat("<category id='", produto.category.id, "' />"), Visivel.ToString().ToLower()); //3181=Paulista


                    var xml = String.Format(rotina.XML_REQUISICAO, produto.id, produto.externalId, produto.name, vlrFormatadoTurma, produto.info.group.property[0].Conteudo.Valor, produto.info.group.property[0].CargaHoraria.Valor, produto.info.group.property[0].Curso.Valor, vender, produto.info.group.property[0].Tipo.Valor, produto.brand.id, String.Concat("<category id='", produto.category.id, "' />"), Visivel.ToString().ToLower()); //3181=Paulista - marca
                    xml = xml.Replace("dd-mm-yyyy", turma.Inicio.ToShortDateString()); //Ateração para incluir o campo data de inicio da turma
                    xml = xml.Replace("<description></description>", "<description>" + produto.description + "</description>");
                    xml = xml.Replace("<shortName></shortName>", "<shortName>" + produto.shortName + "</shortName>");
                   

                    if(Visivel)
                    {
                        xml = xml.Replace("<hidden>true</hidden>", "<hidden>false</hidden>"); 
                    }
                      

                    

                    //Alteração da ficha técnica

                    if (produto.info.group.property[0].Diferenciais != null)
                    {
                        xml = xml.Replace("MarketingDiferenciais", produto.info.group.property[0].Diferenciais.Valor);
                    }
                       

                    if (produto.info.group.property[0].Objetivos != null)
                    {
                        xml = xml.Replace("MarketingObjetivos", produto.info.group.property[0].Objetivos.Valor);
                    }
                        

                    if (produto.info.group.property[0].Requisitos != null)
                    {
                        xml = xml.Replace("MarketingRequisitos", produto.info.group.property[0].Requisitos.Valor);
                    }
                        

                    if (produto.info.group.property[0].Video!=null)
                    {
                        xml = xml.Replace("MarketingVideo", produto.info.group.property[0].Video.Valor);
                    }
                        

                    //------------------------



                    string strImages = "";
                    if (produto.images.Count > 0) {

                        strImages += "<images>";

                        foreach (var img in produto.images)
                        {

                            strImages += "<image>";

                            strImages += "<name>" + img.name + "</name>";
                            strImages += "<type>" + img.type + "</type>";
                            strImages += "<link rel='image'>" + img.link + "</link>";


                            strImages += "</image>";

                        
                        }

                        strImages += "</images>";

                    
                    
                    }
                    xml = xml.Replace("<images>", strImages);
                    xml = xml.Replace("<br>", "<br/>");

                    var ret = new EventosREST().Request(xml, rotina, (Int32)produto.id,false);
                    var objGravado = xml;
                    log.PARAMETROS_INTEGRACAO = string.Concat( "CODIGO TURMA RECEBIDO: ", codigoTurmaFormatado , " Exibir:" + Visivel , " XML MONTADO:" +  objGravado);
                    log.COD_PRODUTO = CodigoTurma;
                    log.DESCRICAO_INTEGRACAO = String.Concat(turma.IdTurma, " - ", turma.Nome);
                    

                    if (ret != null)
                    {
                        log.IND_ERRO = false;
                        log.MENSAGEM = !Visivel ? "Bloqueio de turma efetuado com sucesso." : "Desbloqueio de turma efetuado com sucesso.";
                        
                        log.Inserir();
                        return Request.CreateResponse(HttpStatusCode.OK, ret);
                    }

                    log.IND_ERRO = true;
                    log.MENSAGEM = "Falha ao tentar bloquear turma no site.";
                    log.Inserir();

                    var resp = integraSiga.AtualizarIntegracaoTurma(codigoTurmaFormatado, 1, true);
                    if (!resp)
                    {
                        log.MENSAGEM = "Não foi possível atualizar o siga para reagendamento de tentativa de envio.";
                        log.Inserir();
                    }

                    return Request.CreateResponse(HttpStatusCode.NoContent, "Não foi atualizado (Código da turma não foi encontrado)");
                }
                return Request.CreateResponse(HttpStatusCode.NoContent, "Código da turma não encontrado");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Falha no processamento: " + ex.Message);
            }

        }
    }




}
