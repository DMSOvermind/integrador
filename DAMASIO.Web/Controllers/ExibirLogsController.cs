﻿using DAMASIO.Web.Models;
using DAMASIO.Web.Models.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAMASIO.Integracao.Negocio;
using DAMASIO.Integracao.Dominio.API;

namespace DAMASIO.Web.Controllers
{
    public class ExibirLogsController : Controller
    {
        

        public ActionResult Index()
        {
            if (SiteHelper.Usuario == null)
                RedirectToAction("Login", "Account");
            else
                if (string.IsNullOrEmpty(SiteHelper.Usuario.NomeUsuario))
                    SiteHelper.Usuario.NomeUsuario = "Usuário";


            return View();
        }


        [HttpPost]
        public ActionResult ObterLogs(FiltroGRIDlog filtro)
        {


            var model = new GridLogs
            {
                Logs = new Logs().ObterLogs(filtro.Erro,filtro.Novo,filtro.IntegradoSucesso,filtro.IntegracaoPendente,filtro.TransacoesDestinoSIGA,filtro.TransacoesDestinoSITE,filtro.DataMin,filtro.DataMax)
            };

            return PartialView("_PartialGridLogs", model);

        }

        


        [HttpPost]
        public void DesabilitarTarefa(String codigo, string dataLog, string rotinaIntegracao)
        {
            var integraSiga = new wsSiga.WsIntegracaoPortalSoapClient();
            var log = new Logs();

            int tipoAtualizacao;
            String Rotina;

            switch(rotinaIntegracao){
            
                case "Alteração Info. Turma":
                    tipoAtualizacao = 1;
                    Rotina = "AtualizarInfoTurma";
                    break;
                case "Atualização Valor Turma":
                    tipoAtualizacao=2;
                    Rotina = "AtualizarValorTurma";
                    break;
                default:
                    tipoAtualizacao = 0;
                    Rotina = "";
                    break;
            
            }


            
            var integra = integraSiga.AtualizarIntegracaoTurma(codigo, tipoAtualizacao,false);
            log.COD_PRODUTO = Convert.ToInt32(codigo);
            log.DATA_CRIACAO = DateTime.Now;
            log.DESCRICAO_INTEGRACAO = "-";
            log.DESTINO_INTEGRACAO = "SITE";
            log.IND_ERRO = !integra;
            log.IND_EXIBE_LOG = true;
            log.NOME_USUARIO = SiteHelper.Usuario.NomeUsuario;
            log.ROTINA = rotinaIntegracao;
            log.PARAMETROS_INTEGRACAO = "DESABILITOU SCHEDULER TURMA - " + log.COD_PRODUTO.ToString().PadLeft(5, Convert.ToChar("0")) + " tipo Atualização: " + tipoAtualizacao + " Rotina: " + Rotina;
            log.MENSAGEM = "Desabilitou Scheduler Turma - " + rotinaIntegracao;
            log.Inserir();



        }

        [HttpPost]
        public ActionResult ObterDetalheLog(Int64 codigo,string destinoIntegracao, string descricao)
        {

            var model = new GridLogs
            {

                HistoricoItemLog = new Logs().ObterHistoricoLog(codigo,destinoIntegracao, descricao)
                
            };

            return PartialView("_PartialHistoricoItemLog", model);

        }





    }
}