﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Web.Services;

using DAMASIO.Integracao.Dominio.dto;

using DAMASIO.Integracao.Dominio.API;
using DAMASIO.Web.wsSiga;
using System.Web.Mvc;

namespace DAMASIO.Web.Controllers
{
    public class UnidadesController : ApiController
    {
        public Object Post([FromBody] DAMASIO.Integracao.Dominio.dto.PesquisaUnidadeDTO value)
        {


            try {
                
                var siga = new wsSiga.WsIntegracaoPortalSoapClient();

                if (value == null)
                {
                    value = new PesquisaUnidadeDTO {
                     CodigoUnidade = null,
                     Uf=null
                    };
                }

                var Unidades = siga.ObterEmpresas(value.CodigoUnidade, value.Uf);
                List<Unidade> ListaUnidades = new List<Unidade>();

                if (Unidades != null)
                {

                    foreach (var u in Unidades)
                    {

                        Unidade unidade = new Unidade();

                        unidade.Codigo = u.IdEmpresa;
                        unidade.Nome = u.Nome;

                        unidade.Logradouro = u.Logradouro;
                        unidade.Numero = u.Numero;
                        unidade.Complemento = u.Complemento;
                        unidade.Bairro = u.Bairro;
                        unidade.Cidade = u.Cidade;
                        unidade.Estado = u.Estado;
                        unidade.CEP = u.CEP;

                        unidade.Telefone1 = u.DDDTelefone1 + u.Telefone1;
                        unidade.Telefone2 = u.DDDTelefone2 + u.Telefone2;
                        unidade.Telefone3 = u.DDDTelefone3 + u.Telefone3;

                        unidade.Email = u.Email;

                        unidade.Twitter = u.Twitter;
                        unidade.Facebook = u.Facebook;
                        unidade.GooglePlus = u.GooglePlus;

                        unidade.Convenio = new List<ConvenioUnidade>();
                        foreach (var convenio in u.Convenios)
                        {

                            ConvenioUnidade Convenio = new ConvenioUnidade();
                            Convenio.Codigo = convenio.IdConvenio;
                            Convenio.Nome = convenio.Nome;
                            Convenio.Beneficio = convenio.PercentualDesconto.ToString();
                            Convenio.Vigencia = convenio.Vigencia.ToShortDateString();

                            unidade.Convenio.Add(Convenio);
                        }

                        ListaUnidades.Add(unidade);

                    }

                }
                else
                    return Request.CreateResponse(HttpStatusCode.NoContent, "");


                return Request.CreateResponse(HttpStatusCode.OK, ListaUnidades);



            }
            catch (Exception ex) {

                return Request.CreateResponse(HttpStatusCode.OK, "Falha no processamento: " + ex.Message);
            
            }




        }







    }
}
