﻿using DAMASIO.Integracao.ActionFilters;
using DAMASIO.Integracao.Dominio.BSeller;
using DAMASIO.Integracao.Negocio;
using DAMASIO.Web.Models;
using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using System.Xml.Linq;

namespace DAMASIO.Web.Controllers
{
    public class NovoPedidoBSellerController : ApiController
    {


        [LogAPI]
        public HttpStatusCodeResult Post(HttpRequestMessage request)
        {

            var a = request.Content.ReadAsStreamAsync().Result;
            var xmlNovoPedido = new StreamReader(a).ReadToEnd();
            a.Position = 0;


            XmlDeserializer xml = new XmlDeserializer();
            var responseXML = new RestSharp.RestResponse();
            responseXML.Content = xmlNovoPedido;
            var xDes = xml.Deserialize<Order>(responseXML);


            var pedido = new Pedidos().ProcessarPedido(xDes,"Envio Automático");

            if (pedido != null)
                return new HttpStatusCodeResult(HttpStatusCode.OK, String.Concat(pedido.message, " - Código Pedido: ", pedido.codigo.ToString()));
            else 
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Falha ao gravar o pedido");
        }


    }
}
