﻿using DAMASIO.Integracao.Dominio.API;
using DAMASIO.Integracao.Negocio;
using DAMASIO.Integracao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DAMASIO.Web.Controllers
{
    public class AtualizarValorTurmaController : ApiController
    {

        public HttpResponseMessage Post(Int32 CodigoTurma, Decimal NovoValor)
        {
            var integraSiga = new wsSiga.WsIntegracaoPortalSoapClient();
            Retorno retorno = new Retorno();
            Logs log = new Logs { DATA_CRIACAO = DateTime.Now, ROTINA = "AtualizarValorTurma", NOME_USUARIO = "Siga/Envio Automático/API",IND_ERRO = true, PARAMETROS_INTEGRACAO="-",DESTINO_INTEGRACAO="SITE",IND_EXIBE_LOG=true };

            try
            {

                /* 614 */
                if (CodigoTurma == null || CodigoTurma == 0)
                {
                    retorno.success = false;
                    retorno.message = "Código da turma inválido.";

                    log.MENSAGEM = retorno.message;
                    log.Inserir();

                    return Request.CreateResponse(HttpStatusCode.BadRequest, retorno);
                
                }

                //if (NovoValor == null || NovoValor == 0)
                //{
                //    retorno.success = false;
                //    retorno.message = "Novo Valor inválido.";

                //    log.MENSAGEM = retorno.message;
                //    log.Inserir();

                //    return Request.CreateResponse(HttpStatusCode.BadRequest, retorno);

                //}

                log.COD_PRODUTO = CodigoTurma;

                var turma = integraSiga.ObterTurma(15, CodigoTurma.ToString());
                if (turma == null)
                {
                    retorno.codigo = CodigoTurma;
                    retorno.success = false;
                    retorno.message = "Turma " + CodigoTurma.ToString() + " não foi localizada no SIGA";
                    
                    
                    log.MENSAGEM = retorno.message;
                    log.Inserir();

                    return Request.CreateResponse(HttpStatusCode.BadRequest, retorno);
                }
                /*----------------------*/

                log.DESCRICAO_INTEGRACAO = turma.IdTurma.ToString().PadLeft(5,Convert.ToChar("0")) + " - " + turma.Nome;
                
                var rotina = new Rotinas().GetByID((int)Listas.Rotinas.AtualizarValorProduto);
                string vlrFormatadoTurma = "0.01";
                if (NovoValor > 0)
                    vlrFormatadoTurma = NovoValor.ToString().Replace(",", ".");

                if (rotina != null)
                {

                    string codigoTurmaFormatado = String.Concat("00000", CodigoTurma);
                    codigoTurmaFormatado = codigoTurmaFormatado.Substring(codigoTurmaFormatado.Length - 5);

                    var produto = new Produtos().ConsultarIDexterno(codigoTurmaFormatado);

                    if (produto == null)
                    {
                        retorno.message = "ERRO API BSELLER: Falha ao consultar a turma (produto) pela API";
                        retorno.codigo = CodigoTurma;
                        retorno.success = false;
                        
                        //log.MENSAGEM = retorno.message;
                        //log.Inserir();

                        return Request.CreateResponse(HttpStatusCode.BadRequest, retorno);
                    
                    }


                    if (produto.info == null)
                    {
                        retorno.message = "Turma não possui atualização comercial enviada para o site";
                        retorno.codigo = CodigoTurma;
                        retorno.success = false;

                        log.MENSAGEM = retorno.message;
                        log.Inserir();


                        return Request.CreateResponse(HttpStatusCode.BadRequest, retorno);
                    }


                    var xml = String.Format(rotina.XML_REQUISICAO, produto.id, produto.externalId, vlrFormatadoTurma);
                    var ret = new EventosREST().Request(xml, rotina, null, false);

                    log.XML = xml;

                    if (ret != null)
                    {

                        log.IND_ERRO = false;
                        log.MENSAGEM = "Atualização de valor realizada com sucesso.";
                        log.Inserir();
                        return Request.CreateResponse(HttpStatusCode.OK, ret);
                    
                    
                    }


                        
                    else
                    {
                        retorno.success = false;
                        retorno.message = "Preço não foi atualizado devido a falha na comunicação com a BSELLER.";
                        retorno.codigo = CodigoTurma;

                        log.MENSAGEM = retorno.message;
                        log.Inserir();
                        
                        return Request.CreateResponse(HttpStatusCode.NoContent, retorno);
                    }
                    

                }
                else
                {
                    retorno.message = "Rotina não cadastrada";
                    retorno.codigo = (int)Listas.Rotinas.AtualizarValorProduto;
                    retorno.success = false;
                    return Request.CreateResponse(HttpStatusCode.NoContent, retorno);
                }


            }
            catch (Exception ex)
            {

                log.MENSAGEM = ex.Message;
                log.Inserir();

                return Request.CreateResponse(HttpStatusCode.OK, "Falha no processamento: " + ex.Message);
            }

        }
















    }
}
