﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAMASIO.Integracao.Negocio;
using DAMASIO.Integracao.DAL;
using DAMASIO.Integracao.Dominio.API;
using DAMASIO.Integracao.ActionFilters;
using System.Net;
using System.Web.Script.Serialization;

namespace DAMASIO.Web.Controllers
{
    [AllowAnonymous]
    public class LeadsController : Controller
    {
        [HttpPost]
        [LogAPI]
        public ActionResult Cadastro(Lead lead)
        {
            Logs log = new Logs { DATA_CRIACAO = DateTime.Now, ROTINA = "CadasdroLeads", NOME_USUARIO = "Envio Automático", IND_ERRO = false, PARAMETROS_INTEGRACAO = "-", DESTINO_INTEGRACAO = "INTEGRADOR", IND_EXIBE_LOG = true };

            try {
                Retorno ret = new Retorno();
                ret.success = false;

                if (lead == null)
                {
                    ret.message = "Requisição inválida !";
                    return Json(ret);
                }


                if ((lead.Nome == null && lead.Email == null)  || (lead.Nome == null && lead.Telefone == null))
                {
                    ret.message = "Dados inválidos, por favor preencha seus contatos";
                    return Json(ret);
                }



                LEADS ld = new LEADS
                {
                    NOME = lead.Nome,
                    EMAIL = lead.Email,
                    TELEFONE = lead.Telefone,
                    MENSAGEM = lead.Mensagem,
                    CARREIRAS_INTERNACIONAIS = lead.CarreirasInternacionais,
                    CARREIRAS_PUBLICAS = lead.CarreirasPublicas,
                    EXAME_ORDEM = lead.ExameOrdem,
                    GRADUACAO = lead.Graduacao,
                    POS_GRADUACAO = lead.PosGraduacao,
                    CARREIRAS_JURIDICAS = lead.CarreirasJuridicas,
                    CURSO_SELECIONADO = lead.CursoSelecionado ?? " ",
                    DATA_INCLUSAO = DateTime.Now,
                    CODIGO_UNIDADE = lead.CodigoUnidade,
                    CODIGO_EMPRESA = 15

                };

                var retorno = new Leads().Inserir(ld);

                var objGravado = new JavaScriptSerializer().Serialize(ld);
                log.PARAMETROS_INTEGRACAO = objGravado;
                log.Inserir();


                if (retorno == true)
                {
                    ret.success = true;
                    ret.message = "Dados cadastrados com sucesso";
                    return Json(ret);

                }
                else
                {
                    ret.message = "Algum problema aconteceu ao tentar gravar seu contato ";
                    return Json(ret);
                
                }
            }
            catch (Exception ex) {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "false");
            
            }

        }
    }
}