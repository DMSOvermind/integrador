﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAMASIO.Integracao.Dominio.dto;
using DAMASIO.Integracao.ActionFilters;
using DAMASIO.Integracao.Utils;
using DAMASIO.Integracao.Negocio;
using DAMASIO.Integracao.DAL;
using DAMASIO.Integracao.Dominio.BSeller;
using System.IO;
using System.Xml;
using System.Net;

namespace DAMASIO.Web.Controllers
{
    [AllowAnonymous]
    public class NotificacoesController : Controller
    {
        [Log]
        public ActionResult FrontBseller()
        {
            //Responsável por gravar as notificações de alteração de status no Front da Bseller
            var xmlNotifica = "";
            try {

                xmlNotifica = new StreamReader(Request.InputStream).ReadToEnd();

                Request.InputStream.Position = 0;

                if (xmlNotifica.Length == 0)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Bad Request");

                XmlDocument xmlDoc = new Converters().ConverterXmlStringParaDocument(xmlNotifica);
                var NotificacaoRecebida = new NotificacaoBSeller();

                const string xpath = "orderStateChangeNotification";
                var nodes = xmlDoc.SelectNodes(xpath);

                foreach (XmlNode childrenNode in nodes)
                {
                    NotificacaoRecebida.Codigo = childrenNode.SelectSingleNode("orderNumber").InnerText;
                    NotificacaoRecebida.Status = childrenNode.SelectSingleNode("orderState").InnerText;
                    NotificacaoRecebida.HoraAtualizacao = Convert.ToDateTime(childrenNode.SelectSingleNode("timestamp").InnerText);
                }

#pragma warning disable 168
                var Notificacao = new Notificacoes().Inserir(NotificacaoRecebida);
#pragma warning restore 168
                var pedido = new Pedidos().ConsultarPedido(NotificacaoRecebida.Codigo);

                if (pedido !=null)
                {
                    if (pedido.status == "PAYMENT_APPROVED")
                    {
                        PEDIDO PedidoSiga = new Pedidos().ObterPedido(pedido.id);
                        var contrato = new Pedidos().GerarContratoSIGA(pedido, PedidoSiga,"Envio Automático");
                        if(contrato == false)
                           return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Falha na geração do contrato");
                    }
                }else
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Não foi possível encontrar o pedido");

                return new HttpStatusCodeResult(HttpStatusCode.OK, "Atualização realizada com sucesso");
            
            }
            catch (Exception ex)
            {
                var log = new LOG_INTEGRACAO { DATA_CRIACAO = DateTime.Now, MENSAGEM = xmlNotifica + "///Mensagem ERRO: " + ex.Message, ROTINA="NOTIFICACOES" };
                new Logs().Inserir(log);
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Falha ao atualizar o pedido: " + ex.Message);
            }

            

        }
    }
}