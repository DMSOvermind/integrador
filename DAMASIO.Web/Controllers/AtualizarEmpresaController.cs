﻿using DAMASIO.Integracao.ActionFilters;
using DAMASIO.Integracao.Dominio.API;
using DAMASIO.Integracao.Dominio.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace DAMASIO.Web.Controllers
{
    public class AtualizarEmpresaController : ApiController
    {
        [IsDominioAPI]
        public HttpResponseMessage Post(ConcursoDTO Concurso)
        {

            Retorno retorno = new Retorno();
            retorno.success = false;

            try
            {

                if (Concurso == null)
                {
                    retorno.message = "Não pode receber nulo";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, retorno);
                }










                return Request.CreateResponse(HttpStatusCode.OK, retorno);
            }
            catch (Exception ex)
            {
                retorno.message = ex.Message;
                return Request.CreateResponse(HttpStatusCode.OK, retorno);
            }
        }




    }
    
}