﻿using DAMASIO.Integracao.ActionFilters;
using DAMASIO.Integracao.Dominio.API;
using DAMASIO.Integracao.Negocio;
using DAMASIO.Integracao.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DAMASIO.Web.Controllers
{

    public class AtualizarVagasController : ApiController
    {

        [IsDominioAPI]
        public HttpResponseMessage Post(Int32 CodigoTurma, Int32 NumeroVagasDisponiveis)
        {

            try
            {
                var produto = new Produtos().ConsultarIDexterno(CodigoTurma.ToString());

                if (produto != null) {
                    var rotina = new Rotinas().GetByID((int)Listas.Rotinas.AtualizarEstoqueBSeller);

                    var xml = String.Format(rotina.XML_REQUISICAO, produto.externalId, NumeroVagasDisponiveis.ToString()); //
                    var ret = new EventosREST().Request(xml, rotina,null,false);

                    if (ret !=null)
                       return Request.CreateResponse(HttpStatusCode.OK, ret);
                    else
                       return Request.CreateResponse(HttpStatusCode.NoContent, "Não foi atualizado (Código da turma não foi encontrado)");
                
                } else
                    return Request.CreateResponse(HttpStatusCode.NoContent, "Código da turma não encontrado");


            }
            catch (Exception ex) {
                return Request.CreateResponse(HttpStatusCode.OK, "Falha no processamento: " + ex.Message);
            }
        
        }

    }
}
