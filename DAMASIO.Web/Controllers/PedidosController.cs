﻿using DAMASIO.Integracao.ActionFilters;
using DAMASIO.Integracao.DAL;
using DAMASIO.Integracao.Dominio.API;
using DAMASIO.Integracao.Dominio.BSeller;
using DAMASIO.Integracao.Dominio.dto;
using DAMASIO.Integracao.Negocio;
using DAMASIO.Web.Models;
using DAMASIO.Web.Models.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DAMASIO.Web.Controllers
{
    [Authorize, IsDominioAPI]
    public class PedidosController : Controller
    {
        Logs log = new Logs { DATA_CRIACAO = DateTime.Now, NOME_USUARIO = "Envio Automático", IND_ERRO = true };


        [HttpPost]
        public ActionResult ObterPedidos(FiltroGRID filtro)
        {

            var model = new GridPedidos
            {
                Pedidos = new Pedidos().ObterPedidosGRID(filtro.DataMin, filtro.DataMax, filtro.Pendente, filtro.ErroEnvio, filtro.EnviadoSucesso, filtro.Cancelado, filtro.NaoEnviado)
            };

            return PartialView("_PartialGridPedidos", model);

        }

        [HttpPost]
        public ActionResult setStatusItemPedido(PedidoStatus pedidoStatus)
        {
            String nomeUsuario;
            nomeUsuario = SiteHelper.Usuario != null ? SiteHelper.Usuario.NomeUsuario : "";


            pedidoStatus.CodigoStatus = (int)Listas.StatusPedidoSiga.Cancelado;
            var itemPedido = new Pedidos().AtualizarStatusItemPedido(new Produto { Codigo = pedidoStatus.CodigoItem }, new Pedido { Codigo = pedidoStatus.CodigoPedido.ToString() }, pedidoStatus.CodigoStatus, pedidoStatus.MotivoCancelamento, nomeUsuario);


            var model = new ModelPedidoDetalhe
            {
                pedido = new Pedidos().ObterDetalhePedido(pedidoStatus.CodigoPedido)

            };

            return PartialView("_PartialItensPedido", model);

        }

        [HttpPost]
        public ActionResult reenviarItem(ProdutoReenvio produto)
        {

            var order = new Pedidos().ConsultarPedido(produto.CodigoPedido.ToString());
            var prd = new Produtos().ConsultarIDexterno(produto.CodigoProduto.ToString());

            var pedidoAPI = new Pedidos().ObterPedido(produto.CodigoPedido);

            String nomeUsuario;
            nomeUsuario = SiteHelper.Usuario != null ? SiteHelper.Usuario.NomeUsuario : "";

            log.COD_PEDIDO_BSELLER = order.id;
            log.MENSAGEM = "AVISO: Reenvio de pedido manual para o SIGA.";
            log.ROTINA = "reenviarItem";
            log.COD_PRODUTO = Convert.ToInt64(prd.externalId);
            log.COD_PEDIDO_API = pedidoAPI.COD_PEDIDO;
            log.DESTINO_INTEGRACAO = "SIGA";
            log.IND_ERRO = false;
            log.NOME_USUARIO = nomeUsuario;
            log.Inserir();

            var contrato = new Produtos().GerarContratoProdutoSIGA(order, prd, pedidoAPI.COD_PEDIDO, nomeUsuario);

            var model = new ModelPedidoDetalhe
            {
                pedido = new Pedidos().ObterDetalhePedido(pedidoAPI.COD_PEDIDO),
            };

            model.retorno = new Retorno();

            if (contrato==false)
            {
                model.retorno.success = false;
                model.retorno.message = "Item não foi enviado para o SIGA";
            }
            else
            {
                model.retorno.success = true;
                model.retorno.message = "Item enviado com sucesso para o SIGA";
            }

            return PartialView("_PartialItensPedido", model);
        }

        [HttpPost]
        public ActionResult ObterStatusItem(ProdutoReenvio produto)
        {
            var retorno = new Retorno();

            var pedido = new Pedidos().ObterStatusItemPedido(produto.CodigoPedido, produto.CodigoProduto);

            switch (pedido)
            {

                case 99:
                case 1:
                    retorno.success = false;
                    retorno.message = "Item não foi enviado para o SIGA";
                    break;
                default:
                    retorno.success = true;
                    retorno.message = "Item enviado com sucesso para o SIGA";
                    break;

            }

            return Json(retorno);

        }

        [HttpPost]
        public ActionResult ObterHistorico(FiltroHistorico filtro)
        {


            var model = new ModelGridHistoricoItem
            {
                Historico = new Pedidos().ObterHistorico(filtro.CodigoItem, filtro.CodigoPedido)
            };

            return PartialView("_PartialHistoricoItem", model);

        }

        [HttpPost]
        public ActionResult ObterDadosAlunoPedido(BuscaAluno aluno)
        {

            var model = new ModelAlunoDetalhe
            {

                aluno = new Alunos().ObterCadastro(aluno),
                Pedido = new Pedidos().ObterPedidoSiga(aluno.CodigoPedido)

            };

            return PartialView("_PartialDetalheAluno", model);

        }

        [HttpPost]
        public ActionResult ObterDetalhePedido(Int64 codigo)
        {

            var model = new ModelPedidoDetalhe
            {
                pedido = new Pedidos().ObterDetalhePedido(codigo)

            };

            return PartialView("_PartialDetalhePedido", model);

        }

        [HttpPost]
        public string Create(ModelAlunoDetalhe objAluno, ModelAlunoDetalhe objAlunoResp)
        {
            var retorno = "";
            if (objAluno.aluno != null)
            {
                objAluno.aluno.Responsavel = new Aluno();

                objAluno.aluno.Responsavel.ID = objAlunoResp.aluno.ID;
                objAluno.aluno.Responsavel.Logradouro = objAlunoResp.aluno.Logradouro;
                objAluno.aluno.Responsavel.Nome = objAlunoResp.aluno.Nome;
                objAluno.aluno.Responsavel.Numero = objAlunoResp.aluno.Numero;
                objAluno.aluno.Responsavel.Sexo = objAlunoResp.aluno.Sexo;
                objAluno.aluno.Responsavel.Telefone1 = objAlunoResp.aluno.Telefone1;
                objAluno.aluno.Responsavel.Telefone2 = objAlunoResp.aluno.Telefone2;
                objAluno.aluno.Responsavel.Estado = objAlunoResp.aluno.Estado;
                objAluno.aluno.Responsavel.DataNascimento = objAlunoResp.aluno.DataNascimento;
                objAluno.aluno.Responsavel.Complemento = objAlunoResp.aluno.Complemento;
                objAluno.aluno.Responsavel.Cidade = objAlunoResp.aluno.Cidade;
                objAluno.aluno.Responsavel.Codigo = objAlunoResp.aluno.Codigo;
                objAluno.aluno.Responsavel.CodigoBSeller = objAlunoResp.aluno.CodigoBSeller;
                objAluno.aluno.Responsavel.CEP = objAlunoResp.aluno.CEP;
                objAluno.aluno.Responsavel.Bairro = objAlunoResp.aluno.Bairro;
                objAluno.aluno.Responsavel.Documento = objAlunoResp.aluno.Documento;
                objAluno.aluno.Responsavel.Email = objAlunoResp.aluno.Email;

                retorno = new Pedidos().AtualizaDadosAlunoPedido(objAluno.aluno);
            }

            return retorno;
        }
    }
}