﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Microsoft.Security.Application;
using DAMASIO.Web.Models;

namespace DAMASIO.Web.Controllers
{

    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (SiteHelper.Usuario == null)
                RedirectToAction("Login", "Account");
            else
                if (SiteHelper.Usuario.NomeUsuario == null || SiteHelper.Usuario.NomeUsuario == "")
                    SiteHelper.Usuario.NomeUsuario = "Usuário";

            return View();
        }




        public ActionResult Erro() {

            return View();
        
        }


        
    }
}