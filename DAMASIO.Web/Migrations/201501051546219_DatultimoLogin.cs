namespace DAMASIO.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DatultimoLogin : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "DataUltimoLogin", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "DataUltimoLogin");
        }
    }
}
