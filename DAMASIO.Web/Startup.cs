﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DAMASIO.Web.Startup))]
namespace DAMASIO.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
