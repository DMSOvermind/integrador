﻿using DAMASIO.Integracao.DAL;
using DAMASIO.Integracao.Dominio.API;
using DAMASIO.Integracao.Dominio.BSeller;
using DAMASIO.Integracao.Negocio;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace DAMASIO.Integracao.Utils
{
    public class EventosREST
    {
        Logs log = new Logs { DATA_CRIACAO = DateTime.Now, NOME_USUARIO = "Envio Automático/BSELLER", IND_ERRO = true, ROTINA = "API - WorkflowBSeller", DESTINO_INTEGRACAO = "SITE" };

        private String baseURL
        {
            set { baseURL = ""; }
        }

        public Object Request(string conteudo, CONFIG_ROTINA rotina, Int32? codigo, bool ExibeFicha)
        {
            if (rotina == null)
            {
                return null;
            }

            var reqDestino = rotina.DESTINO;
            if (rotina.TIPO_REQUISICAO.TrimEnd() == "PARAM") // no GET é enviada a requisição na URL alguns PUTs também
            {
                if (ExibeFicha)
                    reqDestino = String.Concat(reqDestino, conteudo, "?showInfo=true");
                else
                    if (rotina.COD_CONFIG_ROTINA == (int)Listas.Rotinas.ObterDadosProdutoPeloIdExterno)
                        reqDestino = String.Concat(reqDestino, conteudo, "");
                    else
                        reqDestino = String.Concat(reqDestino, conteudo, "?version=14");
            }
            else
                if (rotina.TIPO_REQUISICAO.TrimEnd() == "XML" && rotina.INSERIR_CODIGO_URL == true)
                    //reqDestino = String.Concat(reqDestino, codigo);
                    reqDestino = String.Concat(reqDestino, codigo);

            var request = new RestRequest(reqDestino, (Method)rotina.METODO);
            if (rotina.TIPO_REQUISICAO.TrimEnd() == "XML") // no caso de POST e PUT o XML precisa ser inserido no body da requisição
                request.AddParameter("text/xml", conteudo, ParameterType.RequestBody);

            var client = new RestClient(rotina.ENDPOINT);
            //client.AddDefaultHeader("Authorization", "Basic cmljYXJkby5kdWFydGVAaW1wZXRvLmNvbS5icjoxMjM0NTY=");
            client.Authenticator = new HttpBasicAuthenticator(WebConfigurationManager.AppSettings["BSELLER_USERNAME"], WebConfigurationManager.AppSettings["BSELLER_PASSWORD"]);

            //request.AddHeader("Authorization", "Basic cmljYXJkby5kdWFydGVAaW1wZXRvLmNvbS5icjoxMjM0NTY=");


            switch (rotina.COD_CONFIG_ROTINA)
            {

                case 1:
                    return PutEstoque(client, request);
                case 2:
                    return GetProdutoIDexterno(client, request, ExibeFicha);
                case 3:
                    return setProduto(client, request);
                case 4:
                    return setValorProduto(client, request);
                case 5:
                    return getProdutoPeloID(client, request);
                case 6:
                    return getPedido(client, request);
                default:
                    return null;

            }

        }

        private Order getPedido(RestClient client, RestRequest request)
        {

            try
            {

                IRestResponse<Order> response = client.Execute<Order>(request);
                if (response == null)
                    return null;


                var xmlContent = response.Content.Replace("<![CDATA[", null).Replace("]]>", null).Replace("\"", "'").Replace("&bull;", "-");
                xmlContent = xmlContent.Replace("<br>", "<br/>");
                try
                {

                    XDocument orderXMLParsed = XDocument.Parse(xmlContent);
                    XElement orderRoot = orderXMLParsed.Root;

                    XNamespace oRn = "http://kanlo.com.br/order";

                    var xmlOrder = (from i in orderXMLParsed.Descendants(oRn + "payments")
                                    select i).ToList();

                    var _paymentProps = orderXMLParsed.Descendants(oRn + "paymentProps").ToList();
                    XDocument _propsParsed = XDocument.Parse(_paymentProps[0].ToString());

                    var _props = (from p in _propsParsed.Descendants() select p).ToList();

                    response.Data.payment.paymentProps = new List<prop>();

                    foreach (var p in _props)
                    {

                        if (p.Name.LocalName == "prop")
                        {

                            var _prop = (from p1 in p.Descendants() select p1).ToList();
                            var _PropSite = new prop
                                {
                                    key = _prop[0].Value,
                                    value = _prop[1].Value

                                };

                            response.Data.payment.paymentProps.Add(_PropSite);
                        }

                    }



                }
                catch (Exception)
                {
                    return null;
                }

                return response.Data;

            }
            catch (Exception) { return null; }

        }

        private ProdutoBSeller getProdutoPeloID(RestClient client, RestRequest request)
        {

            try
            {

                IRestResponse<ProdutoBSeller> response = client.Execute<ProdutoBSeller>(request);
                if (response == null)
                    return null;
                return response.Data;

            }
            catch (Exception) { return null; }

        }

        private Retorno setValorProduto(RestClient client, RestRequest request)
        {

            try
            {

                IRestResponse<Retorno> response = client.Execute<Retorno>(request);
                //if (response == null)
                //    return null;

                if (response.Data == null)
                    response.Data = new Retorno();

                response.Data.StatusCodeRequest = response.StatusCode.ToString();
                response.Data.MessageRequest = response.ErrorMessage;
                return response.Data;

            }
            catch (Exception) { return null; }

        }

        private Retorno setProduto(RestClient client, RestRequest request)
        {

            try
            {

                IRestResponse<Retorno> response = client.Execute<Retorno>(request);
                //if (response == null)
                //    return null;

                if (response.Data == null)
                    response.Data = new Retorno();

                response.Data.StatusCodeRequest = response.StatusDescription;
                response.Data.MessageRequest = response.Content;
                return response.Data;

            }
            catch (Exception) { return null; }

        }

        private ProdutoBSeller GetProdutoIDexterno(RestClient client, RestRequest request, Boolean FichaTecnica)
        {
            client.Encoding = Encoding.GetEncoding("ISO-8859-1");
            IRestResponse<ProdutoBSeller> response = client.Execute<ProdutoBSeller>(request);
            if (response == null)
            {
                return null;
            }

            if (FichaTecnica)
            {
                //Pegar a ficha técnica do produto, se houver
                var xmlContent = response.Content.Replace("<![CDATA[", null).Replace("]]>", null).Replace("\"", "'").Replace("&bull;", "-");
                xmlContent = xmlContent.Replace("<br>", "<br/>");

                try
                {
                    XDocument produtoXMLParsed = XDocument.Parse(xmlContent);
                    XElement produtoRoot = produtoXMLParsed.Root;

                    XNamespace pd = "http://kanlo.com.br/product";

                    var grp = (from i in produtoXMLParsed.Descendants(pd + "group") select i).ToList();

                    if (grp.Count != 0)
                    {
                        var parametros = grp[0].Nodes().ToList()[0].ElementsAfterSelf();

                        var grupo = new Group();
                        grupo.property = new List<Property>();
                        var property = new Property();

                        foreach (XElement item in parametros)
                        {
                            var prop = item.Nodes().ToList();
                            string valueProp;
                            item.Document.Declaration = new XDeclaration("1.0", "ISO-8859-1", null);

                            switch (((XElement)prop[0]).Value)
                            {

                                case "carga_horaria":
                                    property.CargaHoraria = new Propriedades();
                                    property.CargaHoraria.NomeCampo = ((XElement)prop[0]).Value;
                                    //property.CargaHoraria.Valor = ((XElement)prop[1]).Value.Replace("\n", "").Replace("\"", "'").Trim();
                                    valueProp = prop[1].ToString().Replace("\"", "'");
                                    property.CargaHoraria.Valor = HttpUtility.HtmlEncode(valueProp.Replace("<value xmlns='http://kanlo.com.br/product'>", "").Replace("</value>", ""));
                                    break;
                                case "conteudo":
                                    property.Conteudo = new Propriedades();
                                    property.Conteudo.NomeCampo = ((XElement)prop[0]).Value;
                                    valueProp = prop[1].ToString().Replace("\"", "'");
                                    property.Conteudo.Valor = HttpUtility.HtmlEncode(valueProp.Replace("<value xmlns='http://kanlo.com.br/product'>", "").Replace("</value>", ""));
                                    break;
                                case "Tipo":
                                    property.Tipo = new Propriedades();
                                    property.Tipo.NomeCampo = ((XElement)prop[0]).Value;
                                    //property.Tipo.Valor = ((XElement)prop[1]).Value.Replace("\n", "").Trim();
                                    valueProp = prop[1].ToString().Replace("\"", "'");
                                    property.Tipo.Valor = HttpUtility.HtmlEncode(valueProp.Replace("<value xmlns='http://kanlo.com.br/product'>", "").Replace("</value>", ""));
                                    break;
                                case "venda":
                                    property.Venda = new Propriedades();
                                    property.Venda.NomeCampo = ((XElement)prop[0]).Value;
                                    //property.Venda.Valor = ((XElement)prop[1]).Value.Replace("\n", "").Trim();
                                    valueProp = prop[1].ToString().Replace("\"", "'");
                                    property.Venda.Valor = HttpUtility.HtmlEncode(valueProp.Replace("<value xmlns='http://kanlo.com.br/product'>", "").Replace("</value>", ""));
                                    break;
                                case "curso":
                                    property.Curso = new Propriedades();
                                    property.Curso.NomeCampo = ((XElement)prop[0]).Value;
                                    //property.Curso.Valor = ((XElement)prop[1]).Value.Replace("\n", "").Replace("\"", "'").Trim();
                                    valueProp = prop[1].ToString().Replace("\"", "'");
                                    property.Curso.Valor = HttpUtility.HtmlEncode(valueProp.Replace("<value xmlns='http://kanlo.com.br/product'>", "").Replace("</value>", ""));
                                    break;
                                case "diferenciais":
                                    property.Diferenciais = new Propriedades();
                                    property.Diferenciais.NomeCampo = ((XElement)prop[0]).Value;
                                    //property.Diferenciais.Valor = ((XElement)prop[1]).Value.Replace("\n", "").Replace("\"", "'").Trim();
                                    valueProp = prop[1].ToString().Replace("\"", "'");
                                    property.Diferenciais.Valor = HttpUtility.HtmlEncode(valueProp.Replace("<value xmlns='http://kanlo.com.br/product'>", "").Replace("</value>", ""));
                                    break;
                                case "objetivos":
                                    property.Objetivos = new Propriedades();
                                    property.Objetivos.NomeCampo = ((XElement)prop[0]).Value;
                                    //property.Objetivos.Valor = ((XElement)prop[1]).Value.Replace("\n", "").Replace("\"", "'").Trim();
                                    valueProp = prop[1].ToString().Replace("\"", "'");
                                    property.Objetivos.Valor = HttpUtility.HtmlEncode(valueProp.Replace("<value xmlns='http://kanlo.com.br/product'>", "").Replace("</value>", ""));
                                    break;
                                case "publico":
                                    property.Publico = new Propriedades();
                                    property.Publico.NomeCampo = ((XElement)prop[0]).Value;
                                    //property.Publico.Valor = ((XElement)prop[1]).Value.Replace("\n", "").Replace("\"", "'").Trim();
                                    valueProp = prop[1].ToString().Replace("\"", "'");
                                    property.Publico.Valor = HttpUtility.HtmlEncode(valueProp.Replace("<value xmlns='http://kanlo.com.br/product'>", "").Replace("</value>", ""));
                                    break;
                                case "requisitos":
                                    property.Requisitos = new Propriedades();
                                    property.Requisitos.NomeCampo = ((XElement)prop[0]).Value;
                                    //property.Requisitos.Valor = ((XElement)prop[1]).Value.Replace("\n", "").Replace("\"", "'").Trim();
                                    valueProp = prop[1].ToString().Replace("\"", "'");
                                    property.Requisitos.Valor = HttpUtility.HtmlEncode(valueProp.Replace("<value xmlns='http://kanlo.com.br/product'>", "").Replace("</value>", ""));
                                    break;
                                case "video":
                                    property.Video = new Propriedades();
                                    property.Video.NomeCampo = ((XElement)prop[0]).Value;
                                    //property.Video.Valor = ((XElement)prop[1]).Value.Replace("\n", "").Trim();
                                    valueProp = prop[1].ToString().Replace("\"", "'");
                                    property.Video.Valor = HttpUtility.HtmlEncode(valueProp.Replace("<value xmlns='http://kanlo.com.br/product'>", "").Replace("</value>", ""));
                                    break;
                            }
                        }

                        grupo.property.Add(property);

                        if (grupo.property.Count() > 0)
                        {
                            response.Data.info.group = grupo;
                        }

                    }
                }
                catch (Exception) { }
            }

            return response.Data;

        }

        private RetPostEstoque PutEstoque(RestClient client, RestRequest request)
        {

            try
            {

                IRestResponse<RetPostEstoque> response = client.Execute<RetPostEstoque>(request);

                if (response == null)
                    return null;

                return response.Data;


            }
            catch (Exception) { return null; }

        }

        public string Request(string endPoint, string requestMethod)
        {
            HttpWebRequest request = CreateWebRequest(endPoint, requestMethod);

            var UsuarioSenhaConn = "ricardo.duarte@impeto.com.br,123456"; //alterar

            var encodeString = Converters.Base64Encode(UsuarioSenhaConn);
            //request.Headers.Add("Authorization", String.Concat("Basic ", encodeString));
            request.Headers.Add("Authorization", "Basic cmljYXJkby5kdWFydGVAaW1wZXRvLmNvbS5icjoxMjM0NTY=");

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                var responseValue = string.Empty;

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                    throw new ApplicationException(message);
                }

                using (var responseStream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(responseStream))
                    {
                        responseValue = reader.ReadToEnd();
                    }
                }

                return responseValue;
            }
        }

        private HttpWebRequest CreateWebRequest(string endPoint, string requestMethod, string contentType = "text/xml")
        {
            var request = (HttpWebRequest)WebRequest.Create(endPoint);

            request.Method = requestMethod;
            request.ContentLength = 0;
            request.ContentType = contentType;

            return request;
        }

        //public T ExecuteRequest<T>(string resource,
        //                    RestSharp.Method httpMethod,
        //                    IEnumerable<Parameter> parameters = null,
        //                    string body = null) where T : new()
        //{
        //    RestClient client = new RestClient(baseURL);
        //    RestRequest req = new RestRequest(resource, httpMethod);

        //    // Add all parameters (and body, if applicable) to the request
        //    req.AddParameter("api_key", this.APIKey);
        //    if (parameters != null)
        //    {
        //        foreach (Parameter p in parameters) req.AddParameter(p);
        //    }

        //    if (!string.IsNullOrEmpty(body)) req.AddBody(body); // <-- ISSUE HERE

        //    RestResponse<T> resp = client.Execute<T>(req);
        //    return resp.Data;
        //}

    }
}
