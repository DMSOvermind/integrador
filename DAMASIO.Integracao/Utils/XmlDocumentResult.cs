﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DAMASIO.Integracao.Utils
{
    public class XmlDocumentResult : ActionResult
    {
        private readonly string strXhtmlDocument;
        public XmlDocumentResult(string strXhtmlDocument)
        {
            this.strXhtmlDocument = strXhtmlDocument;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.Response;
            response.ContentType = "text/xml";
        }


    }
}
