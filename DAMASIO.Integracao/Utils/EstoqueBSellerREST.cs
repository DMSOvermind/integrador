﻿using DAMASIO.Integracao.Dominio.BSeller;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Utils
{
    public class EstoqueBSellerREST : EventosREST
    {

        public EstoqueBSellerREST()
            : base(1) 
        {

        }

        public override ProdutoBSeller Get(RestClient client, RestRequest request) { return null; }

        private RetPostEstoque Set(RestClient client, RestRequest request)
        {

            try
            {

                var response = client.Execute<RetPostEstoque>(request);

                if (response == null)
                    return null;

                return new RetPostEstoque
                {
                    message = response.Data.message,
                    success = response.Data.success
                };

            }
            catch (Exception ex) { return null; }

        }

    }
}
