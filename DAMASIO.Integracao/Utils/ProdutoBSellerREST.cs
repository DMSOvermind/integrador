﻿using DAMASIO.Integracao.Dominio.BSeller;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Utils
{
    public class ProdutoBSellerREST : EventosREST
    {
        public ProdutoBSellerREST()
            : base(2) 
        {

        }
        
        public override ProdutoBSeller Get(RestClient client, RestRequest request)
        {
            var response = client.Execute<ProdutoBSeller>(request);
            if (response == null)
                return null;

            ProdutoBSeller produto = new ProdutoBSeller
            {

                id = response.Data.id,
                externalId = response.Data.externalId,
                name = response.Data.name,
                shortName = response.Data.shortName,
                saleable = response.Data.saleable,
                hidden = response.Data.hidden,
                price = response.Data.price,
                dateCreated = response.Data.dateCreated,
                lastUpdated = response.Data.lastUpdated,
                variationMultiplicity = response.Data.variationMultiplicity

            };

            return produto;
        }
    }
}
