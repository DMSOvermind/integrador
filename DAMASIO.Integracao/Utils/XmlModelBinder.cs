﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml.Serialization;

using DAMASIO.Integracao.Dominio.dto;
using System.Web;
using System.Xml;
using System.Xml.Linq;


namespace DAMASIO.Integracao.Utils
{

    [System.Xml.Serialization.XmlRootAttribute("orderStateChangeNotification", Namespace = "http://kanlo.com.br/orderStateChangeNotification", IsNullable = false)]
    public class XmlModelBinder : IModelBinder
    {
        public object BindModel(
            ControllerContext controllerContext,
            ModelBindingContext bindingContext)
        {
            var serializer = new XmlSerializer(typeof(XmlDTO));
            try
            {

                //HttpRequestBase request = controllerContext.HttpContext.Request;
                //string orderNumber = request.Form.Get("orderNumber");

                XDocument xml;
                var xmlReaderSettings = new XmlReaderSettings { DtdProcessing = DtdProcessing.Prohibit };
                var xmlReader = XmlReader.Create(controllerContext.HttpContext.Request.InputStream, xmlReaderSettings);
                xml = XDocument.Load(xmlReader);


                var boundModel = serializer.Deserialize(controllerContext.HttpContext.Request.InputStream);
                return boundModel;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}

//public partial class orderStateChangeNotification { }