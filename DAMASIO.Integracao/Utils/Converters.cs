﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DAMASIO.Integracao.Utils
{
    public class Converters
    {
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }



        public XmlDocument ConverterXmlStringParaDocument(string xml)
        {

            var xmlRet = xml.Replace("<?xml version='1.0' encoding='UTF-8'?>", "").Replace("xmlns='http://kanlo.com.br/product'", "").Replace("xmlns:xmlns='http://kanlo.com.br/orderStateChangeNotification'", "").Replace("xmlns=\"http://kanlo.com.br/orderStateChangeNotification\"", "").Replace("xmlns='http://kanlo.com.br/orderStateChangeNotification'", "");

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlRet);

            return xmlDoc;


        }



    }
}
