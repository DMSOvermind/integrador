﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Web.Script.Serialization;


namespace DAMASIO.Integracao.Utils
{
    public class JsonValueProviderFactory : ValueProviderFactory
    {
        public override IValueProvider GetValueProvider(ControllerContext controllerContext)
        {
            var deserializedJson = GetDeserializedJson(controllerContext);

            if (deserializedJson == null) return null;

            var backingStore = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            AddToBackingStore(backingStore, string.Empty, deserializedJson);

            return new DictionaryValueProvider<object>(backingStore, CultureInfo.CurrentCulture);
        }

        private static void AddToBackingStore(Dictionary<string, object> backingStore, string prefix, object value)
        {
            var dictionary = value as IDictionary<string, object>;
            if (dictionary != null)
            {
                foreach (KeyValuePair<string, object> keyValuePair in dictionary)
                    AddToBackingStore(backingStore, MakePropertyKey(prefix, keyValuePair.Key), keyValuePair.Value);
            }
            else
            {
                var list = value as IList;
                if (list != null)
                {
                    for (var index = 0; index < list.Count; ++index)
                        AddToBackingStore(backingStore, MakeArrayKey(prefix, index), list[index]);
                }
                else backingStore[prefix] = value;
            }
        }

        private static object GetDeserializedJson(ControllerContext controllerContext)
        {
            var contentType = controllerContext.HttpContext.Request.ContentType;
            if (!contentType.StartsWith("text/json", StringComparison.OrdinalIgnoreCase) &&
                !contentType.StartsWith("application/json", StringComparison.OrdinalIgnoreCase))
                return null;

            var input = new StreamReader(controllerContext.HttpContext.Request.InputStream).ReadToEnd();

            if (string.IsNullOrEmpty(input)) return null;

            return new JavaScriptSerializer().DeserializeObject(input);
        }

        private static string MakeArrayKey(string prefix, int index)
        {
            return prefix + "[" + index.ToString(CultureInfo.InvariantCulture) + "]";
        }

        private static string MakePropertyKey(string prefix, string propertyName)
        {
            if (!string.IsNullOrEmpty(prefix))
                return prefix + "." + propertyName;
            return propertyName;
        }



    }
}
