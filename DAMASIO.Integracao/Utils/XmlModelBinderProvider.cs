﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DAMASIO.Integracao.Utils
{
    public class XmlModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(Type modelType)
        {
            var requestType =
                HttpContext.Current.Request.ContentType.ToLower();

            return (requestType == "application/xml"
                || requestType == "text/xml")
                    ? new XmlModelBinder()
                    : null;
        }
    }
}
