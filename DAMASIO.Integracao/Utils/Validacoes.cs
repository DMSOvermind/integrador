﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Utils
{
    public class Validacoes
    {
        public static bool ValidaCPF(string vrCPF)
        {

            string valor = vrCPF;
            if (valor.Length != 11)
            {
                return false;
            }

            bool igual = true;

            for (int i = 1; i < 11 && igual; i++)
            {
                if (valor[i] != valor[0])
                {
                    igual = false;
                }
            }

            if (igual || valor == "12345678909")
            {
                return false;
            }

            var numeros = new int[11];

            for (int i = 0; i < 11; i++)
            {
                numeros[i] = int.Parse(valor[i].ToString());
            }

            int soma = 0;
            for (int i = 0; i < 9; i++)
            {
                soma += (10 - i) * numeros[i];
            }

            int resultado = soma % 11;
            if (resultado == 1 || resultado == 0)
            {
                if (numeros[9] != 0)
                {
                    return false;
                }
            }
            else if (numeros[9] != 11 - resultado)
            {
                return false;
            }

            soma = 0;
            for (int i = 0; i < 10; i++)
            {
                soma += (11 - i) * numeros[i];
            }

            resultado = soma % 11;
            if (resultado == 1 || resultado == 0)
            {
                if (numeros[10] != 0)
                {
                    return false;
                }
            }
            else if (numeros[10] != 11 - resultado)
            {
                return false;
            }

            return true;
        }

        public static bool ValidaCNPJ(string vrCNPJ)
        {
            string CNPJ = vrCNPJ;

            int[] digitos, soma, resultado;
            int nrDig;
            string ftmt;
            bool[] CNPJOk;

            ftmt = "6543298765432";
            digitos = new int[14];
            soma = new int[2];
            soma[0] = 0;
            soma[1] = 0;
            resultado = new int[2];
            resultado[0] = 0;
            resultado[1] = 0;
            CNPJOk = new bool[2];
            CNPJOk[0] = false;
            CNPJOk[1] = false;
            try
            {
                for (nrDig = 0; nrDig < 14; nrDig++)
                {
                    digitos[nrDig] = int.Parse(CNPJ.Substring(nrDig, 1));

                    if (nrDig <= 11)
                    {
                        soma[0] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig + 1, 1)));
                    }

                    if (nrDig <= 12)
                    {
                        soma[1] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig, 1)));
                    }
                }

                for (nrDig = 0; nrDig < 2; nrDig++)
                {
                    resultado[nrDig] = (soma[nrDig] % 11);

                    if ((resultado[nrDig] == 0) || (resultado[nrDig] == 1))
                    {
                        CNPJOk[nrDig] = (digitos[12 + nrDig] == 0);
                    }
                    else
                    {
                        CNPJOk[nrDig] = (digitos[12 + nrDig] == (11 - resultado[nrDig]));
                    }
                }

                return (CNPJOk[0] && CNPJOk[1]);
            }
            catch
            {
                return false;
            }
        }

        public static bool ValidarEmail(string email)
        {
            string emailRegex = string.Format("{0}{1}", @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))", @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$");

            try
            {
                return Regex.IsMatch(email, emailRegex);
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

        }

        public static bool ValidarData(string data)
        {
            DateTime resultado = DateTime.MinValue;
            return DateTime.TryParse(data, out resultado);
        }

        public static bool ValidaCEP(string cep)
        {
            var Rgx = new Regex(@"^\d{5}-\d{3}$");
            return Rgx.IsMatch(cep);
        }

        public static string TrataTelefone(string telefone, int tipo)
        {
            if (telefone == null)
            {
                return telefone;
            }

            var tel = telefone.Replace("(", "");
            tel = tel.Replace(")", "@");
            tel = tel.Replace("_", "");
            tel = tel.Replace("-", "");

            var item1 = tel.Split('@');

            return tipo == 1 ? item1[0] : item1[1];
        }

        public static string TrataCpfCnpj(string CpfCnpj)
        {
            if (CpfCnpj == null)
            {
                return CpfCnpj;
            }

            var retorno = CpfCnpj.Replace(".", "");
            retorno = retorno.Replace("-", "");
            retorno = retorno.Replace("/", "");

            return retorno;
        }

        public static string TrataCep(string cep)
        {
            if (cep == null)
            {
                return cep;
            }

            var retorno = cep.Replace("-", "");

            return retorno;
        }

        public static string TrataNumero(string numero)
        {
            if (numero == null)
            {
                return numero;
            }

            var retorno = numero.Replace("_", "");

            return retorno;
        }

    }
}
