﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DAMASIO.Integracao.DAL;

namespace DAMASIO.Integracao.Negocio
{
    public class Rotinas
    {

        INTEGRACAOEntities db = new INTEGRACAOEntities();

        public List<CONFIG_ROTINA> Get()
        {

            var lista = (from r in db.CONFIG_ROTINA select r).ToList();
            return lista;
        
        }

        public CONFIG_ROTINA GetByID(Int32 id)
        {

            var rotina = (from r in db.CONFIG_ROTINA
                         where r.COD_CONFIG_ROTINA == id
                         select r).FirstOrDefault();
            return rotina;

        }


    }
}
