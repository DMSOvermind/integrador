﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DAMASIO.Integracao.DAL;
using DAMASIO.Integracao.Dominio.dto;
using System.Globalization;

namespace DAMASIO.Integracao.Negocio
{
    public class Alunos
    {
        INTEGRACAOEntities db = new INTEGRACAOEntities();


        public Aluno ObterCadastro(BuscaAluno al) {


            var aluno = new Aluno();
            var ret = (from a in db.VW_PEDIDOS_CLIENTES
                       where a.COD_CLIENTE_BSELLER == al.CodigoAlunoBSeller && a.COD_PEDIDO == al.CodigoPedido
                       select a).ToList();

            if (ret != null)
            {
                //var culture = new CultureInfo("pt-BR");

                foreach (var alu in ret)
                {
                    var pedido = db.PEDIDO.SingleOrDefault(x => x.COD_PEDIDO == alu.COD_PEDIDO);

                    if (alu.NOME_ENDERECO == "Envio")
                    {
                        aluno.ID = Convert.ToInt64(alu.CODIGO_PEDIDO_CLIENTE);
                        aluno.CodigoBSeller = Convert.ToInt64(alu.COD_CLIENTE_BSELLER);
                        aluno.Codigo = Convert.ToInt64(alu.COD_CLIENTE_EXT);
                        aluno.DataCadastro = String.Format("{0:dd/MM/yyyy}", alu.DAT_CADASTRO_CLIENTE);
                        aluno.Nome = alu.NOME;
                        aluno.DataNascimento = alu.DAT_ANIVERSARIO;
                        aluno.Documento = alu.CPF_CNPJ;
                        aluno.Email = alu.EMAIL;
                        aluno.Logradouro = alu.ENDERECO;
                        aluno.Bairro = alu.BAIRRO;
                        aluno.Cidade = alu.CIDADE;
                        aluno.CEP = alu.CEP;
                        aluno.Sexo = alu.SEXO;
                        aluno.Telefone1 = alu.TELEFONE_1;
                        aluno.Telefone2 = alu.TELEFONE_2;
                        aluno.Complemento = alu.COMPLEMENTO;
                        aluno.Numero = alu.NUMERO;
                        /*if (pedido.COD_SITUACAO_SIGA == 1)
                        {
                            aluno.Logradouro = alu.ENDERECOBD;
                            aluno.Complemento = alu.COMPLEMENTO;
                            aluno.Numero = alu.NUMERO;
                        }
                        */
                    }
                    else
                    {
                        aluno.Responsavel = new Aluno();
                        aluno.Responsavel.ID = Convert.ToInt64(alu.CODIGO_PEDIDO_CLIENTE);
                        aluno.Responsavel.CodigoBSeller = Convert.ToInt64(alu.COD_CLIENTE_BSELLER);
                        aluno.Responsavel.Codigo = Convert.ToInt64(alu.COD_CLIENTE_EXT);
                        aluno.Responsavel.DataCadastro = String.Format("{0:dd/MM/yyyy}", alu.DAT_CADASTRO_CLIENTE);
                        aluno.Responsavel.Nome = alu.NOME;
                        aluno.Responsavel.DataNascimento = alu.DAT_ANIVERSARIO;
                        aluno.Responsavel.Documento = alu.CPF_CNPJ;
                        aluno.Responsavel.Email = alu.EMAIL;
                        aluno.Responsavel.Logradouro = alu.ENDERECO;
                        aluno.Responsavel.Bairro = alu.BAIRRO;
                        aluno.Responsavel.Cidade = alu.CIDADE;
                        aluno.Responsavel.CEP = alu.CEP;
                        aluno.Responsavel.Sexo = alu.SEXO;
                        aluno.Responsavel.Telefone1 = alu.TELEFONE_1;
                        aluno.Responsavel.Telefone2 = alu.TELEFONE_2;
                        aluno.Responsavel.Complemento = alu.COMPLEMENTO;
                        aluno.Responsavel.Numero = alu.NUMERO;
                        /*if (pedido.COD_SITUACAO_SIGA == 1)
                        {
                            aluno.Responsavel.Logradouro = alu.ENDERECOBD;
                            aluno.Responsavel.Complemento = alu.COMPLEMENTO;
                            aluno.Responsavel.Numero = alu.NUMERO;
                        }
                        */
                    }
                }
            }
            else
            {
                aluno = null;  
            }

            return aluno;
        }
    }
}
