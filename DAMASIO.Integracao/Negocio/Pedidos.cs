﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using DAMASIO.Integracao.DAL;
using DAMASIO.Integracao.Utils;
using System.Xml;

using DAMASIO.Integracao.Dominio.BSeller;
using System.Xml.Linq;
using DAMASIO.Integracao.Dominio.API;
using DAMASIO.Integracao.wsSiga;
using DAMASIO.Integracao.Dominio.dto;
using System.Data.Entity.Validation;
using DAMASIO.Integracao.Utils;

namespace DAMASIO.Integracao.Negocio
{
    public class Pedidos
    {
        INTEGRACAOEntities db = new INTEGRACAOEntities();

        private int codProduto;
        Logs log = new Logs { DATA_CRIACAO = DateTime.Now, NOME_USUARIO = "Envio Automático/BSELLER", IND_ERRO = true, IND_EXIBE_LOG = true, DESTINO_INTEGRACAO = "SIGA", PARAMETROS_INTEGRACAO = " ", DESCRICAO_INTEGRACAO = "-" };

        public Retorno ProcessarPedido(Order order, string nommeUsuario)
        {

            try
            {

                var retorno = new Retorno();

                var result = ObterPedido(order.id);
                if (result != null)
                {
                    var log = new LOG_INTEGRACAO
                    {
                        COD_PEDIDO_BSELLER = order.id,
                        MENSAGEM = "Falha ao gravar novo pedido (Duplicidade encontrada)",
                        DATA_CRIACAO = DateTime.Now,
                        ROTINA = "ProcessarPedido",
                        IND_ERRO = true,

                        /*ERN 614 - */
                        DESCRICAO_INTEGRACAO = order.cart.OrderLines[0].productId + " - " + order.cart.OrderLines[0].name,
                        DESTINO_INTEGRACAO = "SIGA",
                        IND_EXIBE_LOG = true,
                        PARAMETROS_INTEGRACAO = " "

                    };


                    db.LOG_INTEGRACAO.Add(log);
                    db.SaveChanges();

                    retorno.success = false;
                    retorno.codigo = result.COD_PEDIDO;
                    retorno.message = "Falha ao gravar novo pedido (Duplicidade encontrada)";

                    return retorno;
                }



                var pedido = new PEDIDO();

                pedido.COD_PEDIDO_BSELLER = Convert.ToInt64(order.id);
                pedido.DAT_CRIACAO = order.dateCreated;
                pedido.DAT_ATUALIZACAO = order.lastUpdated;
                pedido.SITUACAO = order.status;
                pedido.COD_SITUACAO_SIGA = (Int32)Listas.StatusPedidoSiga.Pendente;
                pedido.COD_SITUACAO_BSELLER = SituacaoBSeller(order.status);
                pedido.DAT_INCLUSAO_API = DateTime.Now;
                pedido.VAL_TOTAL = order.cart.totalAmount;
                pedido.VAL_DESCONTO = order.cart.totalDiscountAmount;
                pedido.CUPOM_DESCONTO = order.cart.cupom ?? "--";

                if (order.cart != null)
                {
                    foreach (var prom in order.cart.promotions)
                    {

                        pedido.PROMOCOES_APLICADAS = prom.name + "/";

                    }
                }


                if (pedido.PROMOCOES_APLICADAS != null)
                    pedido.PROMOCOES_APLICADAS = pedido.PROMOCOES_APLICADAS.Substring(0, pedido.PROMOCOES_APLICADAS.Length - 1);


                db.PEDIDO.Add(pedido);
                db.SaveChanges();



                var cliente = new PEDIDO_CLIENTE();

                cliente.COD_PEDIDO = pedido.COD_PEDIDO;
                cliente.COD_CLIENTE_BSELLER = order.customer.id;
                cliente.CPF_CNPJ = order.customer.document;
                cliente.APELIDO = order.customer.nickName;
                cliente.NOME = order.customer.name;
                cliente.DAT_CADASTRO_CLIENTE = DateTime.Now;

                cliente.DAT_ANIVERSARIO = order.customer.birthdate.Year <= 1900 ? Convert.ToDateTime("01/01/1978") : order.customer.birthdate;

                cliente.DDD_TELEFONE_1 = order.customer.phone.ddd.ToString();
                cliente.TELEFONE_1 = order.customer.phone.number;

                cliente.IND_PESSOA_FISICA = order.customer.isPhysicalPerson;

                cliente.EMAIL = order.customer.email;

                cliente.SEXO = order.customer.gender.Substring(0, 1);

                db.PEDIDO_CLIENTE.Add(cliente);
                db.SaveChanges();




                //Endereco de envio
                var enderecoEnvio = new PEDIDO_ENDERECO
                {
                    IDENTIFICADOR_ENDERECO = order.addresses.shipping.id,
                    NOME_ENDERECO = "Envio",
                    NOME_CONTATO = order.addresses.shipping.contactName,

                    COD_PEDIDO = pedido.COD_PEDIDO,



                    BAIRRO = order.addresses.shipping.province,
                    CIDADE = order.addresses.shipping.city,
                    ESTADO = order.addresses.shipping.state,
                    CEP = order.addresses.shipping.zipcode,
                    ENDERECO = order.addresses.shipping.address,
                    NUMERO = order.addresses.shipping.number,
                    COMPLEMENTO = order.addresses.shipping.complement

                };

                db.PEDIDO_ENDERECO.Add(enderecoEnvio);
                db.SaveChanges();


                //Endereco de cobrança
                var enderecoCobranca = new PEDIDO_ENDERECO
                {
                    IDENTIFICADOR_ENDERECO = order.addresses.billing.id,
                    NOME_ENDERECO = "Cobrança",
                    NOME_CONTATO = order.addresses.billing.contactName,

                    COD_PEDIDO = pedido.COD_PEDIDO,

                    BAIRRO = order.addresses.billing.province,
                    CIDADE = order.addresses.billing.city,
                    ESTADO = order.addresses.billing.state,
                    CEP = order.addresses.billing.zipcode,
                    ENDERECO = order.addresses.billing.address,
                    NUMERO = order.addresses.billing.number,
                    COMPLEMENTO = order.addresses.billing.complement

                };

                db.PEDIDO_ENDERECO.Add(enderecoCobranca);
                db.SaveChanges();



                var i = 1;

                var numCarrinho = order.cart.cartId.ToString();
                foreach (var item in order.cart.OrderLines)
                {
                    var ItemCarrinho = new PEDIDO_CARRINHO();
                    ItemCarrinho.NUM_CARRINHO = numCarrinho;
                    ItemCarrinho.COD_PEDIDO = pedido.COD_PEDIDO;
                    ItemCarrinho.COD_PRODUTO = item.productId;
                    ItemCarrinho.NUM_ITEM = i;
                    ItemCarrinho.QUANTIDADE = item.quantity;
                    ItemCarrinho.VAL_DESCONTO = item.discount;
                    ItemCarrinho.VAL_ORIGINAL = item.listPrice;
                    ItemCarrinho.VAL_VENDA = item.salePrice;
                    ItemCarrinho.VAL_FINAL = item.totalAmount;
                    ItemCarrinho.COD_SITUACAO_INTEGRACAO_SIGA = (int)Listas.StatusPedidoSiga.Pendente;
                    ItemCarrinho.NOME_ITEM = item.name;


                    ProdutoBSeller produto = new Produtos().ConsultarIDexterno(item.productId.ToString());
                    if (produto != null)
                    {
                        ItemCarrinho.NOME_ITEM = produto.name;
                        ItemCarrinho.COD_PRODUTO_EXTERNO = produto.externalId;
                    }


                    db.PEDIDO_CARRINHO.Add(ItemCarrinho);
                    db.SaveChanges();

                    var log = new LOG_INTEGRACAO
                    {
                        COD_PEDIDO_BSELLER = order.id,
                        MENSAGEM = "Recebimento do Pedido",
                        DATA_CRIACAO = DateTime.Now,
                        ROTINA = "ProcessarPedido",
                        IND_ERRO = false,
                        COD_PRODUTO = item.productId,
                        COD_PEDIDO_API = pedido.COD_PEDIDO,
                        NOME_USUARIO = nommeUsuario,

                        /*ERN 614 - */
                        DESCRICAO_INTEGRACAO = order.cart.OrderLines[0].productId + " - " + order.cart.OrderLines[0].name,
                        DESTINO_INTEGRACAO = "SIGA",
                        IND_EXIBE_LOG = true,
                        PARAMETROS_INTEGRACAO = " "

                    };
                    db.LOG_INTEGRACAO.Add(log);
                    db.SaveChanges();

                    i++;



                }


                var pagto = new PEDIDO_PAGAMENTO();
                pagto.COD_PEDIDO = pedido.COD_PEDIDO;
                pagto.FORMA_PAGAMENTO = order.payment.paymentType;
                //pagto.ID_TRANSACAO = order.payment.transactionId ?? "--";

                //if(order.payment.income !=null)
                //    pagto.NUM_AUTORIZACAO = order.payment.income.authorizationCode ?? "--";

                pagto.NUM_PARCELAS = order.payment.creditCard.parcels;
                pagto.SEQ_PAGAMENTO = 1;
                pagto.VAL_PAGAMENTO = order.payment.amount;

                if (pagto.VAL_PAGAMENTO >= 1)
                    pagto.VAL_PARCELAS = pagto.VAL_PAGAMENTO / Convert.ToDecimal(pagto.NUM_PARCELAS);
                else
                    pagto.VAL_PARCELAS = pagto.VAL_PAGAMENTO;

                //pagto.NUM_ACQUIRER_TRANSACTION_ID = order.payment.paymentProps.Find(p => p.key == "acquirerTransactionId").value ?? "--";
                //pagto.NUM_PROOF_OF_SALE = order.payment.paymentProps.Find(p => p.key == "proofOfSale").value ?? "--";
                //pagto.RETURN_CODE = order.payment.paymentProps.Find(p => p.key == "returnCode").value ?? "--";

                db.PEDIDO_PAGAMENTO.Add(pagto);
                db.SaveChanges();

                if (order.payment.paymentType == "CREDIT_CARD")
                {

                    pagto.FORMA_PAGAMENTO = "Cartão de Crédito";
                    pagto.NUM_PARCELAS = order.payment.creditCard.parcels;
                    pagto.VAL_JUROS = order.payment.creditCard.interestAmount;
                    pagto.TX_JUROS = order.payment.creditCard.interestRate;
                    if (pagto.VAL_PAGAMENTO >= 1)
                        pagto.VAL_PARCELAS = pagto.VAL_PAGAMENTO / Convert.ToDecimal(pagto.NUM_PARCELAS);
                    else
                        pagto.VAL_PARCELAS = pagto.VAL_PAGAMENTO;
                    db.SaveChanges();

                    var cartao = new CARTAO();
                    cartao.COD_PEDIDO_PAGAMENTO = pagto.COD_PEDIDO_PAGAMENTO;
                    cartao.NOME_CLIENTE = order.payment.creditCard.ownerName;
                    cartao.NUM_CARTAO = order.payment.creditCard.number;
                    cartao.NUM_PIN = order.payment.creditCard.pin;
                    cartao.EXPIRACAO_CARTAO = order.payment.creditCard.expirationDate;
                    cartao.NOME_BANDEIRA = order.payment.creditCard.brand;

                    db.CARTAO.Add(cartao);
                    db.SaveChanges();


                }


                retorno.codigo = pedido.COD_PEDIDO;
                retorno.success = true;
                retorno.message = "Pedido gravado com sucesso";
                return retorno;

            }
            catch (Exception ex)
            {
                var log = new LOG_INTEGRACAO
                {
                    COD_PEDIDO_BSELLER = order.id,
                    MENSAGEM = ex.Message,
                    DATA_CRIACAO = DateTime.Now,
                    ROTINA = "ProcessarPedido",
                    IND_ERRO = true,
                    NOME_USUARIO = nommeUsuario,


                    /*ERN 614 - */
                    DESCRICAO_INTEGRACAO = order.cart.OrderLines[0].productId + " - " + order.cart.OrderLines[0].name,
                    DESTINO_INTEGRACAO = "SIGA",
                    IND_EXIBE_LOG = true,
                    PARAMETROS_INTEGRACAO = " "
                };
                db.LOG_INTEGRACAO.Add(log);
                db.SaveChanges();


                return null;
            }






        }

        public bool AtualizarStatusSigaPedidoPeloCodigoBSeller(Pedido pedido, int status, String descricaoNotificacaoBSeller)
        {

            var CodPed = Convert.ToInt64(pedido.Codigo);

            if (descricaoNotificacaoBSeller == "CANCELLED")
                descricaoNotificacaoBSeller = "PAYMENT_DECLINED"; //MANTER A DESCRIÇÃO DE PAGAMENTO RECUSADO


            var stPed = (from pd in db.PEDIDO
                         where pd.COD_PEDIDO_BSELLER == CodPed
                         select pd).FirstOrDefault();

            if (stPed != null)
            {
                stPed.DAT_ATUALIZACAO = DateTime.Now;
                stPed.COD_SITUACAO_SIGA = status;
                stPed.COD_SITUACAO_BSELLER = (int)Listas.StatusPedidoBSeller.PagamentoRecusado;
                stPed.SITUACAO = descricaoNotificacaoBSeller;
                db.SaveChanges();
                return true;
            }

            return false;

        }

        public bool AtualizarStatusSigaPedido(Pedido pedido, int status)
        {

            var CodPed = Convert.ToInt64(pedido.Codigo);


            var stPed = (from pd in db.PEDIDO
                         where pd.COD_PEDIDO == CodPed
                         select pd).FirstOrDefault();

            if (stPed != null)
            {
                stPed.DAT_ATUALIZACAO = DateTime.Now;
                stPed.COD_SITUACAO_SIGA = status;
                db.SaveChanges();
                return true;
            }

            return false;

        }

        public bool AtualizarStatusItemPedido(Produto produto, Pedido pedido, int status, string motivoCancelamento, string nomeUsuario)
        {

            var CodExt = Convert.ToInt64(produto.Codigo);
            var CodPed = Convert.ToInt64(pedido.Codigo);


            var item = (from p in db.PEDIDO_CARRINHO
                        where p.COD_PRODUTO == CodExt && p.COD_PEDIDO == CodPed
                        select p).FirstOrDefault();



            var ped = ObterPedidoPeloID(Convert.ToInt64(pedido.Codigo));


            if (item != null)
            {

                item.COD_SITUACAO_INTEGRACAO_SIGA = status;
                item.MOTIVO_CANCELAMENTO = motivoCancelamento;
                db.SaveChanges();

                if (status == (int)Listas.StatusPedidoSiga.Cancelado)
                {

                    var log = new LOG_INTEGRACAO
                    {
                        COD_PEDIDO_BSELLER = ped.COD_PEDIDO_BSELLER,
                        COD_PEDIDO_API = ped.COD_PEDIDO,
                        MENSAGEM = "Cancelamento do Pedido - " + motivoCancelamento,
                        DATA_CRIACAO = DateTime.Now,
                        ROTINA = "AtualizarStatusItemPedido",
                        IND_ERRO = false,
                        COD_PRODUTO = CodExt,
                        NOME_USUARIO = nomeUsuario


                    };
                    db.LOG_INTEGRACAO.Add(log);
                    db.SaveChanges();
                }


                //Se um item do pedido estiver com erro ou for cancelado , precisamos alterar o status do pedido
                if (status == (int)Listas.StatusPedidoSiga.ErroNoEnvio || status == (int)Listas.StatusPedidoSiga.Cancelado)
                {

                    var stPed = (from pd in db.PEDIDO
                                 where pd.COD_PEDIDO == ped.COD_PEDIDO
                                 select pd).FirstOrDefault();

                    if (stPed != null)
                    {

                        stPed.COD_SITUACAO_SIGA = status;
                        db.SaveChanges();
                    }
                }

            }
            else return false;

            return true;

        }

        public bool AtualizarStatusContrato(Produto produto, ContratoDTO contratoSiga, Pedido pedido)
        {


            var CodExt = Convert.ToInt64(produto.Codigo);
            var CodPed = Convert.ToInt64(pedido.Codigo);


            var prod = (from p in db.PEDIDO_CARRINHO
                        where p.COD_PRODUTO == CodExt && p.COD_PEDIDO == CodPed
                        select p).FirstOrDefault();
            if (prod != null && contratoSiga != null)
            {

                prod.COD_SITUACAO_INTEGRACAO_SIGA = (int)Listas.StatusPedidoSiga.EnviadoComSucesso;
                prod.COD_CONTRATO_SIGA = contratoSiga.IdContrato;
                db.SaveChanges();

                return AtualizarPedido(pedido, Convert.ToInt64(contratoSiga.IdContrato));
            }

            return false;
        }

        public bool AtualizarPedido(Pedido pedido, Int64 CodigoContrato)
        {

            var CodPed = Convert.ToInt64(pedido.Codigo);


            var Pedido = (from p in db.PEDIDO
                          where p.COD_PEDIDO == CodPed
                          select p).FirstOrDefault();
            if (Pedido != null)
            {
                Pedido.COD_CONTRATO_SIGA = CodigoContrato;
                db.SaveChanges();
                return true;
            }

            return false;



        }

        public PEDIDO_PAGAMENTO ObterPedidoPagamento(PEDIDO pedido)
        {

            var ped = (from pd in db.PEDIDO
                       where pd.COD_PEDIDO_BSELLER == pedido.COD_PEDIDO_BSELLER
                       select pd).FirstOrDefault();


            if (ped != null)
            {
                var result = (from p in db.PEDIDO_PAGAMENTO
                              where p.COD_PEDIDO == ped.COD_PEDIDO
                              select p).FirstOrDefault();


                return result;

            }

            return null;


            //var result = (from p in db.PEDIDO_PAGAMENTO
            //              where p.COD_PEDIDO == pedido.COD_PEDIDO
            //              select p).FirstOrDefault();


            // return result;

        }

        public Listas.StatusContratoItem VerificarSeItemCarrinhoPossuiContrato(Int32 codigoProduto, Int32 codigoPedido)
        {

            var pedido = (from ped in db.PEDIDO
                          where ped.COD_PEDIDO_BSELLER == codigoPedido
                          select ped).FirstOrDefault();
            if (pedido != null)
            {
                var prod = (from p in db.PEDIDO_CARRINHO
                            where p.COD_PRODUTO == codigoProduto && p.COD_PEDIDO == pedido.COD_PEDIDO && (p.COD_CONTRATO_SIGA == 0 || p.COD_CONTRATO_SIGA == null)
                            select p).FirstOrDefault();

                return prod != null ? Listas.StatusContratoItem.NaoEncontrado : Listas.StatusContratoItem.Encontrado;
            }
            return Listas.StatusContratoItem.PedidoInexistente;
        }

        public bool GerarContratoSIGA(Order order, PEDIDO pedido, string nomeUsuario)
        {
            try
            {
                var retorno = new Retorno();

                retorno.success = false;

                var Aluno = new AlunoDTO();

                var integraSiga = new WsIntegracaoPortalSoapClient();
                integraSiga.InnerChannel.OperationTimeout = new TimeSpan(0, 5, 0);


                Aluno.Cpf = order.customer.document;
                Aluno.Nome = order.customer.name;
                Aluno.Sexo = Convert.ToChar(order.customer.gender.Substring(0, 1));


                //Aluno.TelefoneResidencial = String.Concat(order.customer.phone.ddd, order.customer.phone.number);
                Aluno.DddTelefoneCelular = order.customer.phone.ddd.ToString();
                Aluno.TelefoneCelular = order.customer.phone.number ?? " ";

                Aluno.Email = order.customer.email ?? " ";

                if (order.customer.birthdate.Year <= 1900) // O cliente informa uma data inválida
                {
                    Aluno.DataNascimento = Convert.ToDateTime("02/10/1978");
                    log.IND_ERRO = false;
                    log.MENSAGEM = "AVISO: O Cliente informou uma data de nascimento inválida. Foi alterada para padrão.";
                    log.DESCRICAO_INTEGRACAO = order.customer.birthdate.ToShortDateString();
                    log.COD_PEDIDO_BSELLER = order.id;
                    log.ROTINA = "GerarContratoSIGA";

                    log.Inserir();
                }
                else
                {
                    Aluno.DataNascimento = order.customer.birthdate;
                }

                Aluno.Cep = order.addresses.shipping.zipcode;
                Aluno.IdEmpresaOrigem = 0;
                Aluno.Escolaridade = 5;
                Aluno.EstadoCivil = " ";
                Aluno.IdTipoAluno = 1;
                Aluno.IdProcedencia = 4;
                Aluno.NomePai = " ";
                Aluno.NomeMae = " ";
                Aluno.Logradouro = order.addresses.shipping.address;

                var endAluno = integraSiga.ObterEndereco(Aluno.Cep);
                if (endAluno == null)
                {
                    Aluno.Cep = "01510001";
                    endAluno = integraSiga.ObterEndereco(Aluno.Cep);
                    Aluno.Logradouro = "[VERIFICAR ENDEREÇO DO ALUNO]";

                    log.IND_ERRO = false;
                    log.MENSAGEM = "AVISO: CEP Alterado para padrão (01510-001). Motivo: Informado pelo aluno não foi encontrado na base SIGA.";
                    log.DESCRICAO_INTEGRACAO = order.customer.birthdate.ToShortDateString();
                    log.COD_PEDIDO_BSELLER = order.id;
                    log.ROTINA = "GerarContratoSIGA";
                    log.Inserir();

                }

                var cidade = integraSiga.ObterCidade(endAluno.Cidade.IdCidade.ToString());

                if (cidade == null)
                {
                    Aluno.Cep = "01510001";
                    endAluno = integraSiga.ObterEndereco(Aluno.Cep);
                    Aluno.Logradouro = "[VERIFICAR ENDEREÇO DO ALUNO]";

                    log.MENSAGEM = "AVISO: Endereço alterado para padrão. Motivo: CIDADE não encontrada na base SIGA.";
                    log.DESCRICAO_INTEGRACAO = "Informado pelo site:" + endAluno.Cidade.Nome;
                    log.IND_ERRO = false;
                    log.Inserir();
                }

                Aluno.Bairro = endAluno.Bairro.Length == 0 ? order.addresses.billing.province : endAluno.Bairro;

                if (endAluno.Bairro.Length >= 29)
                {
                    Aluno.Bairro = endAluno.Bairro.Substring(0, 29);
                }

                Aluno.IdCidade = endAluno.Cidade.IdCidade;
                Aluno.IdUF = endAluno.Cidade.Uf.IdUf;

#pragma warning disable 168
                decimal totalDecontoItens = 0;
#pragma warning restore 168

                foreach (var item in order.cart.OrderLines)
                {

                    var Contrato = new ContratoDTO();
                    codProduto = item.productId;
                    ProdutoBSeller produtoCarrinho = new Produtos().ConsultarIDexterno(item.productId.ToString(), false);

                    //verificar se o item do carrinho já possui contrato gerado
                    if (VerificarSeItemCarrinhoPossuiContrato(item.productId, order.id) == Listas.StatusContratoItem.Encontrado)
                    {
                        log.IND_ERRO = true;
                        log.MENSAGEM = "ERRO: Não foi gerado contrato para o item " + item.productId + " do pedido " + order.id + " - Motivo: Contrato já foi gerado no SIGA anteriormente";
                        log.DESCRICAO_INTEGRACAO = "-";
                        log.COD_PRODUTO = item.productId;
                        log.COD_PEDIDO_BSELLER = order.id;
                        log.ROTINA = "GerarContratoSIGA";
                        log.Inserir();


                        //esta atualização está aqui para acertar os problemas anteriores em que os contratos duplicados ficavam com erro no status
                        //se o contrato já foi enviado, no fluxo correto, o pedido com um item deve estar com sucesso
                        AtualizarStatusItemPedido(new Produto { Codigo = item.productId }, new Pedido { Codigo = pedido.COD_PEDIDO.ToString() }, (int)Listas.StatusPedidoSiga.EnviadoComSucesso, "", nomeUsuario);
                        //quando o carrinho permitir mais de um item, precisa rever esta parte
                        AtualizarStatusSigaPedido(new Pedido { Codigo = pedido.COD_PEDIDO.ToString() }, (int)Listas.StatusPedidoSiga.EnviadoComSucesso);

                        return false;
                    }

                    if (produtoCarrinho.brand != null)
                        produtoCarrinho.brand.id = 15;//new Produtos().ObterCodigoEmpresa(produtoCarrinho.brand.id); //atualiza o id da marca com o código da empresa que está na base
                    else
                    {
                        log.IND_ERRO = true;
                        log.MENSAGEM = "ERRO: Falha ao gerar contrato: Produto sem marca";
                        log.DESCRICAO_INTEGRACAO = "-";
                        log.COD_PRODUTO = item.productId;
                        log.COD_PEDIDO_BSELLER = order.id;
                        log.ROTINA = "GerarContratoSIGA";
                        log.Inserir();

                        AtualizarStatusItemPedido(new Produto { Codigo = item.productId }, new Pedido { Codigo = pedido.COD_PEDIDO.ToString() }, (int)Listas.StatusPedidoSiga.ErroNoEnvio, "", nomeUsuario);

                        return false;
                    }

                    Aluno.IdEmpresaOrigem = produtoCarrinho.brand.id;

                    if (produtoCarrinho != null)
                    {
                        Contrato.Aluno = Aluno;
                        Contrato.Emissao = DateTime.Now;
                        Contrato.IdTurma = produtoCarrinho.externalId;
                        Contrato.IdUsuario = 0;
                        Contrato.IdUsuarioEfetivacao = 0;
                        Contrato.IdEmpresa = produtoCarrinho.brand.id;
                        Contrato.IdEmpresaOrigem = produtoCarrinho.brand.id;
                        Contrato.ValorDescontoSite = order.cart.totalDiscountAmount;

                        if (item.discount > 0)
                        {
                            Contrato.ValorDescontoSite += item.discount;
                        }

                        Contrato.ValorGlobal = item.salePrice - Contrato.ValorDescontoSite;

                        if (Decimal.Compare(item.salePrice, (decimal)0.01) == 0)
                        {
                            Contrato.ValorCurso = 0;
                            Contrato.ValorVista = 0;
                            Contrato.ValorGlobal = 0;
                            Contrato.NumParcelas = 0;
                        }
                        else
                        {
                            Contrato.ValorCurso = item.salePrice;
                            Contrato.ValorVista = Contrato.ValorGlobal;
                            Contrato.NumParcelas = order.payment.creditCard.parcels;

                        }

                        Contrato.ValorEntrada = 0;

                        var codCurso = integraSiga.ObterIdCurso(Contrato.IdEmpresa, Contrato.IdTurma);
                        Contrato.IdCurso = codCurso;
                        Contrato.IdStatus = Convert.ToChar("A");
                        Contrato.IdTipoContrato = Convert.ToChar("N");
                        Contrato.IdPedido = order.id;

                        //Responsável financeiro para menores
                        var idade = DateTime.Now.Year - order.customer.birthdate.Year;
                        if (idade < 18)
                        {
                            try
                            {
                                Aluno.Responsavel = new ResponsavelDTO();

                                Aluno.Responsavel.Nome = order.addresses.billing.contactName;
                                Aluno.Responsavel.Cpf = order.addresses.billing.complement;
                                Aluno.Responsavel.EstadoCivil = " ";

                                var endResponsavel = integraSiga.ObterEndereco(order.addresses.billing.zipcode);
                                if (endResponsavel != null)
                                {
                                    Aluno.Responsavel.Cep = endResponsavel.Cep;
                                    Aluno.Responsavel.IdCidade = endResponsavel.Cidade.IdCidade;
                                    Aluno.Responsavel.Bairro = endResponsavel.Bairro;
                                    Aluno.Responsavel.IdUF = endResponsavel.Cidade.Uf.IdUf;
                                }
                                else return false;

                                Aluno.Responsavel.Endereco = order.addresses.billing.address;
                                //Fixei pois não existem no site e são obrigatórios no siga --precisa alterar isso depois
                                Aluno.Responsavel.Nascimento = Convert.ToDateTime("20/08/1956");
                                Aluno.Responsavel.EstadoCivil = "Casado";
                                Aluno.Responsavel.Sexo = Convert.ToChar("F");

                            }
                            catch (Exception ex)
                            {
                                log.IND_ERRO = true;
                                log.IND_EXIBE_LOG = true;
                                log.MENSAGEM = "ERRO: Falha na geração do contrato : " + ex.Message + " - Resp. Financeiro";
                                log.DESCRICAO_INTEGRACAO = "-";
                                log.COD_PRODUTO = item.productId;
                                log.COD_PEDIDO_BSELLER = order.id;
                                log.ROTINA = "GerarContratoSIGA";
                                log.Inserir();
                                return false;
                            }
                        }
                        //--------------------------------------------------------------------------

                        if (Contrato.NumParcelas > 0)
                        {
                            var transactionPayment = ObterPedidoPagamento(pedido);
                            var venctoPadrao = DateTime.Now;
                            var valorParcela = order.payment.amount / order.payment.creditCard.parcels;

                            Contrato.Parcelas = new ParcelaDTO[Contrato.NumParcelas];

                            for (int i = 0; i < Contrato.NumParcelas; i++)
                            {

                                Contrato.Parcelas[i] = new ParcelaDTO();

                                Contrato.Parcelas[i].IdEmpresa = produtoCarrinho.brand.id;
                                //Alterado pois o Siga grava o número do cartão no campo documento,
                                Contrato.Parcelas[i].NumeroCartao = transactionPayment.NUM_AUTORIZACAO ?? "--";
                                Contrato.Parcelas[i].NumeroParcela = i + 1;
                                Contrato.Parcelas[i].Vencimento = venctoPadrao.AddMonths(i + 1);
                                Contrato.Parcelas[i].VencimentoAceito = venctoPadrao.AddMonths(i + 1);
                                Contrato.Parcelas[i].ValorParcela = valorParcela;

                                Contrato.Parcelas[i].IdTipLancamento = 1;

                                //Buscando estas informações do BD pois não são consultadas na Order
                                Contrato.Parcelas[i].TidNsu = transactionPayment.NUM_ACQUIRER_TRANSACTION_ID ?? "--";
                                Contrato.Parcelas[i].NumAutorizacao = transactionPayment.NUM_AUTORIZACAO ?? "--";

                                switch (order.payment.creditCard.brand)
                                {
                                    case "VISA":
                                        Contrato.Parcelas[i].Bandeira = (int)BandeiraCartao.VISA;
                                        break;
                                    case "MASTERCARD":
                                        Contrato.Parcelas[i].Bandeira = (int)BandeiraCartao.MASTERCARD;
                                        break;
                                }

                                Contrato.Parcelas[i].IdFormaPagamento = 12; //Cartão internet - Fixo no SIGA

                            }
                        }

                        var novoContrato = integraSiga.EmitirContrato(Contrato);

                        var validacao = validacaoDadosAluno(Contrato, item.productId);

                        if (validacao == false)
                        {
                            AtualizarStatusItemPedido(new Produto { Codigo = item.productId }, new Pedido { Codigo = pedido.COD_PEDIDO.ToString() }, (int)Listas.StatusPedidoSiga.ErroNoEnvio, "", nomeUsuario);
                            return validacao;
                        }

                        retorno.message = novoContrato.MsgErro;

                        if (novoContrato.IdContrato != 0)
                        {
                            retorno.success = true;
                            if (AtualizarStatusContrato(new Produto { Codigo = item.productId }, novoContrato, new Pedido { Codigo = pedido.COD_PEDIDO.ToString() }))
                            {
                                log.IND_ERRO = false;
                                log.IND_EXIBE_LOG = true;
                                log.MENSAGEM = "OK: Contrato gerado com sucesso: " + novoContrato.IdContrato;
                                log.DESCRICAO_INTEGRACAO = "-";
                                log.COD_PRODUTO = item.productId;
                                log.COD_PEDIDO_BSELLER = order.id;
                                log.ROTINA = "GerarContratoSIGA";
                                log.Inserir();

                                AtualizarStatusSigaPedido(new Pedido { Codigo = pedido.COD_PEDIDO.ToString() }, (int)Listas.StatusPedidoSiga.EnviadoComSucesso);
                            }
                        }
                        else
                        {
                            log.IND_ERRO = true;
                            log.IND_EXIBE_LOG = true;
                            log.MENSAGEM = "ERRO: Não foi possível gerar o contrato no SIGA. Motivo: " + novoContrato.MsgErro;
                            log.DESCRICAO_INTEGRACAO = "-";
                            log.COD_PRODUTO = item.productId;
                            log.COD_PEDIDO_BSELLER = order.id;
                            log.ROTINA = "GerarContratoSIGA";
                            log.Inserir();
                            if (retorno.message.Contains("O Aluno já esta matriculado"))
                            {
                                AtualizarStatusItemPedido(new Produto { Codigo = item.productId }, new Pedido { Codigo = pedido.COD_PEDIDO.ToString() }, (int)Listas.StatusPedidoSiga.EnviadoComSucesso, "", nomeUsuario); // pedido reenviado pelo site, devemos deixar o status como sucesso e não erro
                                AtualizarStatusSigaPedido(new Pedido { Codigo = pedido.COD_PEDIDO.ToString() }, (int)Listas.StatusPedidoSiga.EnviadoComSucesso);
                            }
                            else
                            {
                                AtualizarStatusItemPedido(new Produto { Codigo = item.productId }, new Pedido { Codigo = pedido.COD_PEDIDO.ToString() }, (int)Listas.StatusPedidoSiga.ErroNoEnvio, "", nomeUsuario);
                            }
                        }
                    }
                    else
                    {
                        //Contrato não foi gerado --------------

                        log.COD_PEDIDO_BSELLER = order.id;
                        log.MENSAGEM = "ERRO: Falha ao gerar contrato. Empresa Inválida. ";
                        log.DATA_CRIACAO = DateTime.Now;
                        log.ROTINA = "GerarContratoSIGA";
                        log.IND_ERRO = true;
                        log.IND_EXIBE_LOG = true;
                        log.COD_PRODUTO = item.productId;
                        log.COD_PEDIDO_API = pedido.COD_PEDIDO;
                        new Logs().Inserir(log);

                        if (retorno.message.Contains("O Aluno já esta matriculado"))
                        {
                            AtualizarStatusItemPedido(new Produto { Codigo = item.productId }, new Pedido { Codigo = pedido.COD_PEDIDO.ToString() }, (int)Listas.StatusPedidoSiga.EnviadoComSucesso, "", nomeUsuario); // pedido reenviado pelo site, devemos deixar o status como sucesso e não erro
                            AtualizarStatusSigaPedido(new Pedido { Codigo = pedido.COD_PEDIDO.ToString() }, (int)Listas.StatusPedidoSiga.EnviadoComSucesso);
                        }
                        else
                        {
                            AtualizarStatusItemPedido(new Produto { Codigo = item.productId }, new Pedido { Codigo = pedido.COD_PEDIDO.ToString() }, (int)Listas.StatusPedidoSiga.ErroNoEnvio, "", nomeUsuario);
                        }
                    }
                }

                return true;

            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                {
                    if (((e.InnerException).InnerException).Message == "The operation has timed out")
                    {
                        log.COD_PEDIDO_BSELLER = order.id;
                        log.MENSAGEM = "ERRO: Timeout na geração do Contrato SIGA. ";
                        log.DATA_CRIACAO = DateTime.Now;
                        log.ROTINA = "GerarContratoSIGA";
                        log.IND_ERRO = true;
                        log.IND_EXIBE_LOG = true;
                        log.COD_PRODUTO = codProduto;
                        log.COD_PEDIDO_API = pedido.COD_PEDIDO;
                        new Logs().Inserir(log);
                        AtualizarStatusItemPedido(new Produto { Codigo = codProduto }, new Pedido { Codigo = pedido.COD_PEDIDO.ToString() }, (int)Listas.StatusPedidoSiga.ErroNoEnvio, "", nomeUsuario);
                    }
                    else
                    {
                        log.COD_PEDIDO_BSELLER = order.id;
                        log.MENSAGEM = "ERRO: " + e.Message;
                        log.DATA_CRIACAO = DateTime.Now;
                        log.ROTINA = "GerarContratoSIGA";
                        log.IND_ERRO = true;
                        log.IND_EXIBE_LOG = true;
                        log.COD_PRODUTO = codProduto;
                        log.COD_PEDIDO_API = pedido.COD_PEDIDO;
                        new Logs().Inserir(log);
                        AtualizarStatusItemPedido(new Produto { Codigo = codProduto }, new Pedido { Codigo = pedido.COD_PEDIDO.ToString() }, (int)Listas.StatusPedidoSiga.ErroNoEnvio, "", nomeUsuario);
                    }
                }
                else
                {

                    log.COD_PEDIDO_BSELLER = order.id;
                    log.MENSAGEM = "ERRO: " + e.Message;
                    log.DATA_CRIACAO = DateTime.Now;
                    log.ROTINA = "GerarContratoSIGA";
                    log.IND_ERRO = true;
                    log.IND_EXIBE_LOG = true;
                    log.COD_PRODUTO = codProduto;
                    log.COD_PEDIDO_API = pedido.COD_PEDIDO;
                    new Logs().Inserir(log);
                    AtualizarStatusItemPedido(new Produto { Codigo = codProduto }, new Pedido { Codigo = pedido.COD_PEDIDO.ToString() }, (int)Listas.StatusPedidoSiga.ErroNoEnvio, "", nomeUsuario);

                }

                return false;
            }
        }

        public bool validacaoDadosAluno(ContratoDTO contrato, int produtoId)
        {
            var idade = DateTime.Now.Year - contrato.Aluno.DataNascimento.Year;

            if (contrato.Aluno.Cpf.Length > 11)
            {

                log.IND_ERRO = true;
                log.IND_EXIBE_LOG = true;
                log.MENSAGEM = "ERRO: Cpf do Aluno acima do limite permitido. 11 caracteres.";
                log.DESCRICAO_INTEGRACAO = "-";
                log.COD_PRODUTO = produtoId;
                log.COD_PEDIDO_BSELLER = contrato.IdPedido;
                log.ROTINA = "GerarContratoSIGA";

                log.Inserir();

                return false;
            }
            if (contrato.Aluno.Nome.Length > 50)
            {
                log.IND_ERRO = true;
                log.IND_EXIBE_LOG = true;
                log.MENSAGEM = "ERRO: Nome do Aluno acima do limite permitido. 50 caracteres.";
                log.DESCRICAO_INTEGRACAO = "-";
                log.COD_PRODUTO = produtoId;
                log.COD_PEDIDO_BSELLER = contrato.IdPedido;
                log.ROTINA = "GerarContratoSIGA";

                log.Inserir();

                return false;
            }
            if (idade < 18)
            {
                if (contrato.Aluno.Responsavel.Nome.Length > 50)
                {
                    log.IND_ERRO = true;
                    log.IND_EXIBE_LOG = true;
                    log.MENSAGEM = "ERRO: Nome do Responsável do Aluno acima do limite permitido. 50 caracteres.";
                    log.DESCRICAO_INTEGRACAO = "-";
                    log.COD_PRODUTO = produtoId;
                    log.COD_PEDIDO_BSELLER = contrato.IdPedido;
                    log.ROTINA = "GerarContratoSIGA";

                    log.Inserir();

                    return false;
                }
                if (contrato.Aluno.Responsavel.Cpf.Length > 11)
                {
                    log.IND_ERRO = true;
                    log.IND_EXIBE_LOG = true;
                    log.MENSAGEM = "ERRO: Cpf do Responsável do Aluno acima do limite permitido. 11 caracteres.";
                    log.DESCRICAO_INTEGRACAO = "-";
                    log.COD_PRODUTO = produtoId;
                    log.COD_PEDIDO_BSELLER = contrato.IdPedido;
                    log.ROTINA = "GerarContratoSIGA";

                    log.Inserir();

                    return false;
                }
            }

            return true;
        }

        public string SituacaoSIGA(int codigoSituacao)
        {

            switch (codigoSituacao)
            {

                case 0:
                    return "Pendente";
                case 1:
                    return "Erro no Envio";
                case 2:
                    return "Enviado com Sucesso";
                case 3:
                    return "Cancelado";
                case 4:
                    return "Não Enviado";
                default:
                    return "";

            }


        }

        public Int32 SituacaoBSeller(string Situacao)
        {



            switch (Situacao)
            {

                case "NEW_ORDER":
                    return 10;

                case "AWAITING_PAYMENT":
                    return 20;

                case "PAYMENT_DECLINED":
                    return 30;

                case "PAYMENT_APPROVED":
                    return 40;

                case "CANCELLED":
                    return 50;

                default:
                    return 0;

            }



        }

        private string TranslateSituacaoBSeller(string statusPedidoBSeller)
        {

            switch (statusPedidoBSeller)
            {

                case "NEW_ORDER":
                    return "Novo Pedido";

                case "AWAITING_PAYMENT":
                    return "Aguardando Pagamento";

                case "PAYMENT_DECLINED":
                    return "Pagamento Recusado";

                case "PAYMENT_APPROVED":
                    return "Pagamento Aprovado";

                case "CANCELLED":
                    return "Cancelado";

                default:
                    return "";

            }


        }

        private Boolean Gravar(Pedido pedido)
        {
            try
            {


                var p = ObterPedido(Convert.ToInt64(pedido.Codigo));
                if (p == null)
                {

                    var NovoPedido = new PEDIDO
                    {
                        COD_PEDIDO = Convert.ToInt64(pedido.Codigo)
                     ,
                        DAT_ATUALIZACAO = pedido.DataAtualizacao
                     ,
                        DAT_CRIACAO = pedido.DataCriacao
                     ,
                        SITUACAO = pedido.Situacao
                     ,
                        CANAL_VENDA = pedido.CanalVenda
                     ,
                        TAG_CLIENTE = pedido.TagCliente
                     ,
                        DAT_ENVIO_BSELLER = DateTime.Now

                    };

                    db.PEDIDO.Add(NovoPedido);
                    db.SaveChanges();

                }
                else
                {

                    AtualizarPedido(pedido);


                }

                return true;

            }
            catch (Exception)
            {

                return false;

            }



        }

        public List<HistoricoItemPedido> ObterHistorico(Int64 CodigoItem, Int64 CodigoPedido)
        {

            var lista = new List<HistoricoItemPedido>();

            var listaHst = (from h in db.LOG_INTEGRACAO
                            where h.COD_PEDIDO_BSELLER == CodigoPedido && h.COD_PRODUTO == CodigoItem
                            select h).ToList();

            if (listaHst != null)
            {

                foreach (var hist in listaHst)
                {

                    var item = new HistoricoItemPedido();
                    item.Data = hist.DATA_CRIACAO.ToString();
                    item.Descricao = hist.MENSAGEM;
                    item.Usuario = hist.NOME_USUARIO;

                    item.Status = hist.IND_ERRO == true ? "Erro no envio" : "Ok";

                    lista.Add(item);

                }



            }

            return lista;


        }

        public List<GridPedido> ObterPedidosGRID(string DataMin, string DataMax, bool Pendente, bool ErroEnvio, bool EnviadoSucesso, bool Cancelado, bool NaoEnviado)
        {

            var lista = new List<GridPedido>();

            var pedidos = (from p in db.SP_GET_PEDIDOS_POR_DATA_E_STATUS(DataMin, DataMax, Pendente, ErroEnvio, EnviadoSucesso, Cancelado, NaoEnviado)
                           select p).ToList();


            foreach (var pedido in pedidos)
            {

                var gp = new GridPedido();
                gp.Codigo = pedido.COD_PEDIDO;
                gp.CodigoBSeller = pedido.COD_PEDIDO_BSELLER.ToString();
                gp.CodigoAlunoBSeller = Convert.ToInt64(pedido.COD_CLIENTE_BSELLER);
                gp.DataPedido = Convert.ToDateTime(pedido.DAT_CRIACAO_PEDIDO);
                gp.Documento = pedido.DOCUMENTO;
                gp.Nome = pedido.NOME_CLIENTE;
                gp.SituacaoBSeller = pedido.SITUACAO_PEDIDO_BSELLER;
                gp.CodigoSituacaoSIGA = Convert.ToInt32(pedido.COD_SITUACAO_SIGA);
                gp.SituacaoSIGA = pedido.SITUACAO_PEDIDO_SIGA;
                gp.ValorVenda = pedido.VAL_TOTAL.ToString();
                gp.ToolTipMessage = pedido.TT_MSG;


                lista.Add(gp);
            }


            return lista;



        }

        public PEDIDO ObterPedidoPeloID(Int64 CodigoPedido)
        {

            try
            {
                return (from p in db.PEDIDO
                        where p.COD_PEDIDO == CodigoPedido
                        select p).FirstOrDefault();
            }
            catch (Exception) { return null; }
        }

        public PEDIDO ObterPedido(Int64 CodigoPedidoBSeller)
        {

            try
            {
                var pedido = (from p in db.PEDIDO
                              where p.COD_PEDIDO_BSELLER == CodigoPedidoBSeller
                              select p).FirstOrDefault();

                if (pedido != null)
                {
                    var Pedido = new PEDIDO();
                    Pedido.COD_PEDIDO = pedido.COD_PEDIDO;
                    Pedido.COD_PEDIDO_BSELLER = pedido.COD_PEDIDO_BSELLER;
                    Pedido.COD_CONTRATO_SIGA = pedido.COD_CONTRATO_SIGA;
                    Pedido.CANAL_VENDA = pedido.CANAL_VENDA;
                    Pedido.COD_SITUACAO_BSELLER = pedido.COD_SITUACAO_BSELLER;
                    Pedido.COD_SITUACAO_SIGA = pedido.COD_SITUACAO_SIGA;
                    Pedido.DAT_ATUALIZACAO = pedido.DAT_ATUALIZACAO;
                    //25-09-2015
                    Pedido.COD_CONTRATO_SIGA = pedido.COD_CONTRATO_SIGA;

                    return Pedido;
                }


                return null;
            }
            catch (Exception) { return null; }
        }

        public PEDIDO ObterPedidoSiga(Int64 CodigoPedido)
        {

            try
            {
                var pedido = (from p in db.PEDIDO
                              where p.COD_PEDIDO == CodigoPedido
                              select p).FirstOrDefault();

                if (pedido != null)
                {
                    var Pedido = new PEDIDO();
                    Pedido.COD_PEDIDO = pedido.COD_PEDIDO;
                    Pedido.COD_PEDIDO_BSELLER = pedido.COD_PEDIDO_BSELLER;
                    Pedido.COD_CONTRATO_SIGA = pedido.COD_CONTRATO_SIGA;
                    Pedido.CANAL_VENDA = pedido.CANAL_VENDA;
                    Pedido.COD_SITUACAO_BSELLER = pedido.COD_SITUACAO_BSELLER;
                    Pedido.COD_SITUACAO_SIGA = pedido.COD_SITUACAO_SIGA;
                    Pedido.DAT_ATUALIZACAO = pedido.DAT_ATUALIZACAO;
                    //25-09-2015
                    Pedido.COD_CONTRATO_SIGA = pedido.COD_CONTRATO_SIGA;

                    return Pedido;
                }


                return null;
            }
            catch (Exception) { return null; }
        }

        public int ObterStatusItemPedido(Int64 CodigoPedido, Int64 CodigoProduto)
        {

            try
            {

                var status = (from i in db.VW_SITUACAO_PEDIDO
                              where i.COD_PEDIDO_BSELLER == CodigoPedido && i.COD_PRODUTO == CodigoProduto
                              select i.COD_SITUACAO_INTEGRACAO_SIGA).FirstOrDefault();
                return status ?? 99;
            }
            catch (Exception) { return 99; }
        }

        public PedidoSite ObterDetalhePedido(Int64 CodigoPedido)
        {

            try
            {
#pragma warning disable 168
                var integraSiga = new WsIntegracaoPortalSoapClient();
#pragma warning restore 168
                var pedido = new PedidoSite();
                pedido.Itens = new List<ItensPedido>();
                pedido.Pagamento = new List<Dominio.API.Pagamento>();


                var item = (from p in db.PEDIDO
                            where p.COD_PEDIDO == CodigoPedido
                            select p).FirstOrDefault();

                if (item != null)
                {

                    pedido.Codigo = item.COD_PEDIDO;
                    pedido.CodigoBSeller = Convert.ToInt64(item.COD_PEDIDO_BSELLER);
                    pedido.DataAtualizacao = Convert.ToDateTime(item.DAT_ATUALIZACAO);
                    pedido.DataPedido = Convert.ToDateTime(item.DAT_CRIACAO);
                    pedido.Desconto = Convert.ToDecimal(item.VAL_DESCONTO);
                    pedido.Frete = 0;
                    pedido.ValorPedido = Convert.ToDecimal(item.VAL_TOTAL);
                    pedido.Situacao = TranslateSituacaoBSeller(item.SITUACAO.TrimEnd());
                    pedido.CupomUtilizado = item.CUPOM_DESCONTO ?? "--";
                    pedido.Promocoes = item.PROMOCOES_APLICADAS ?? "--";


                    var Listaitens = (from i in db.PEDIDO_CARRINHO
                                      where i.COD_PEDIDO == pedido.Codigo
                                      select i).ToList();

                    if (Listaitens != null)
                    {

                        foreach (var it in Listaitens)
                        {
                            var ip = new ItensPedido();
                            ip.CodigoPedido = Convert.ToInt64(it.COD_PEDIDO);
                            ip.CodigoProdutoBSeller = Convert.ToInt64(it.COD_PRODUTO);

                            ip.ContratoSIGA = it.COD_CONTRATO_SIGA.ToString();


                            ip.Desconto = Convert.ToDecimal(it.VAL_DESCONTO);
                            ip.Sequencial = Convert.ToInt32(it.NUM_ITEM);
                            ip.Situacao = SituacaoSIGA(Convert.ToInt32(it.COD_SITUACAO_INTEGRACAO_SIGA));
                            ip.CodigoSituacao = Convert.ToInt32(it.COD_SITUACAO_INTEGRACAO_SIGA);
                            ip.ValorUnitario = Convert.ToDecimal(it.VAL_ORIGINAL);
                            ip.ValorTotal = Convert.ToDecimal(it.VAL_FINAL);
                            ip.Desconto = Convert.ToDecimal(it.VAL_DESCONTO);
                            ip.ValorVenda = Convert.ToDecimal(it.VAL_FINAL);
                            ip.Quantidade = Convert.ToInt32(it.QUANTIDADE);
                            ip.Descricao = it.NOME_ITEM;
                            ip.CodigoProduto = it.COD_PRODUTO_EXTERNO ?? "";

                            pedido.Itens.Add(ip);
                        }

                    }



                    var listaPagamento = (from p in db.VW_PEDIDO_PAGAMENTO
                                          where p.COD_PEDIDO == pedido.Codigo
                                          select p).ToList();

                    if (listaPagamento != null)
                    {

                        foreach (var pagto in listaPagamento)
                        {

                            var pagamento = new Dominio.API.Pagamento();

                            pagamento.Bandeira = pagto.NOME_BANDEIRA;
                            pagamento.FormaDePagamento = pagto.FORMA_PAGAMENTO;
                            pagamento.NumeroCartao = pagto.NUM_CARTAO.Remove(4, 8).Insert(4, " XXXX XXXX ");
                            pagamento.QuantidadeParcelas = Convert.ToInt32(pagto.NUM_PARCELAS);
                            pagamento.ValorTotal = Convert.ToDecimal(pagto.VAL_PAGAMENTO);
                            pagamento.TaxaJuros = pagto.TX_JUROS;
                            pagamento.ValorJuros = pagto.VAL_JUROS;
                            pagamento.ValorParcela = Convert.ToDecimal(pagto.VAL_PARCELAS);
                            pagamento.idTransacao = pagto.ID_TRANSACAO;
                            pagamento.idAutorizacao = pagto.NUM_AUTORIZACAO ?? "--";
                            pagamento.idTransacaoAdquirente = pagto.NUM_ACQUIRER_TRANSACTION_ID ?? "--";


                            pedido.Pagamento.Add(pagamento);
                        }

                    }






                }
                return pedido;

            }
            catch (Exception) { return null; }
        }

        public Retorno ObterDetalhePedidoPeloContrato(Int64 CodigoContrato)
        {

            var retorno = new Retorno();
            retorno.success = false;
            try
            {
#pragma warning disable 168
                var integraSiga = new WsIntegracaoPortalSoapClient();
#pragma warning restore 168
                var pedido = new PedidoSite();
                pedido.Itens = new List<ItensPedido>();
                pedido.Pagamento = new List<Dominio.API.Pagamento>();


                var item = (from p in db.PEDIDO
                            where p.COD_CONTRATO_SIGA == CodigoContrato
                            select p).FirstOrDefault();

                if (item != null)
                {

                    var rotina = new Rotinas().GetByID((int)Listas.Rotinas.ObterPedido);
                    var produto = (Order)new EventosREST().Request(item.COD_PEDIDO_BSELLER.ToString(), rotina, null, false);
                    if (produto != null)
                    {
                        retorno.success = true;
                        retorno.message = "Consulta realizada com sucesso.";
                        retorno.ResponseServer = produto;
                    }
                    else
                    {
                        retorno.success = false;
                        retorno.codigo = Convert.ToInt64(item.COD_CONTRATO_SIGA);
                        retorno.message = "Falha ao consultar o pedido na BSELLER - COD: " + item.COD_PEDIDO_BSELLER;
                    }

                }
                else
                {
                    retorno.success = false;
                    retorno.message = "Contrato não encotrado no integrador";
                }


                return retorno;

            }
            catch (Exception ex)
            {
                retorno.message = ex.Message;
                retorno.success = false;
                return retorno;
            }
        }

        public PEDIDO AtualizarPedido(Pedido pedido)
        {
            try
            {

                Int64 CodigoPedido = Convert.ToInt64(pedido.Codigo);

                var ped = (from p in db.PEDIDO
                           where p.COD_PEDIDO == CodigoPedido
                           select p).FirstOrDefault();

                if (ped != null)
                {

                    ped.DAT_REENVIO_BSELLER = DateTime.Now;
                    ped.SITUACAO = pedido.Situacao;
                    ped.DAT_ATUALIZACAO = pedido.DataAtualizacao;
                    ped.COD_SITUACAO_BSELLER = pedido.CodigoSitucaoBSELLER;

                    if (ped.COD_SITUACAO_BSELLER == (int)Listas.StatusPedidoBSeller.PagamentoRecusado || ped.COD_SITUACAO_BSELLER == (int)Listas.StatusPedidoBSeller.Cancelado)
                        ped.COD_SITUACAO_SIGA = (int)Listas.StatusPedidoSiga.NaoEnviado;

                    db.SaveChanges();

                    new LOG_INTEGRACAO { COD_PEDIDO_API = ped.COD_PEDIDO, COD_PEDIDO_BSELLER = ped.COD_PEDIDO_BSELLER, DATA_CRIACAO = DateTime.Now, IND_ERRO = false, MENSAGEM = "OK: Pedido atualizado com sucesso.", ROTINA = "AtualizarPedido", NOME_USUARIO = "Envio automático/BSELLER" };


                    var atualizaPagto = (from p in db.PEDIDO_PAGAMENTO
                                         where p.COD_PEDIDO == ped.COD_PEDIDO
                                         select p).FirstOrDefault();

                    if (atualizaPagto != null)
                    {
                        atualizaPagto.NUM_AUTORIZACAO = pedido.Pagamento.NumeroAutorizacao ?? "Não Informado BSeller";
                        atualizaPagto.ID_TRANSACAO = pedido.Pagamento.IdTransacao ?? "--";
                        atualizaPagto.NUM_ACQUIRER_TRANSACTION_ID = pedido.Pagamento.AcquirerTransactionID;
                        atualizaPagto.NUM_PROOF_OF_SALE = pedido.Pagamento.NumProofOfSale;
                        atualizaPagto.RETURN_CODE = pedido.Pagamento.ReturnCode;

                    }
                    db.SaveChanges();


                    return ped;

                }

                return null;





            }
            catch (Exception ex)
            {

                new LOG_INTEGRACAO { COD_PEDIDO_BSELLER = Convert.ToInt64(pedido.Codigo), DATA_CRIACAO = DateTime.Now, IND_ERRO = true, MENSAGEM = "FALHA AO ATUALIZAR PEDIDO - " + ex.Message, ROTINA = "AtualizarPedido" };
                return null;
            }
        }

        public Pedido ProcessarPedido(string xmlPedido, string NameSpace)
        {

            try
            {
                XDocument doc = XDocument.Parse(xmlPedido);
                XElement pedidoBSeller = doc.Root;
                XNamespace pd = NameSpace;


#pragma warning disable 168
                var pedidoElements = pedidoBSeller.Elements();
#pragma warning restore 168


                var order = from i in doc.Descendants(pd + "order")
                            select i;

                var pedido = new Pedido();
                if (order.Count() > 0)
                {

                    pedido.Codigo = (from i in order.Descendants(pd + "id")
                                     select i).ElementAt(0).Value;
                    pedido.DataCriacao = Convert.ToDateTime((from i in order.Descendants(pd + "dateCreated")
                                                             select i).ElementAt(0).Value);
                    pedido.DataAtualizacao = Convert.ToDateTime((from i in order.Descendants(pd + "lastUpdated")
                                                                 select i).ElementAt(0).Value);
                    pedido.Situacao = (from i in order.Descendants(pd + "status")
                                       select i).ElementAt(0).Value;

                    // ----------- Dados do Cliente

                    pedido.Cliente = new Cliente();
                    var cliente = (from i in order.Descendants(pd + "customer")
                                   select i);
                    pedido.Cliente.Codigo = (from i in cliente.Descendants(pd + "id")
                                             select i).ElementAt(0).Value;
                    pedido.Cliente.PessoaFisica = Convert.ToBoolean((from i in cliente.Descendants(pd + "isPhysicalPerson")
                                                                     select i).ElementAt(0).Value);
                    pedido.Cliente.Nome = (from i in cliente.Descendants(pd + "name")
                                           select i).ElementAt(0).Value;
                    pedido.Cliente.Apelido = (from i in cliente.Descendants(pd + "nickName")
                                              select i).ElementAt(0).Value;
                    pedido.Cliente.CpfCNPJ = (from i in cliente.Descendants(pd + "document")
                                              select i).ElementAt(0).Value;
                    pedido.Cliente.Email = (from i in cliente.Descendants(pd + "email")
                                            select i).ElementAt(0).Value;
                    pedido.Cliente.Sexo = (from i in cliente.Descendants(pd + "gender")
                                           select i).ElementAt(0).Value;
                    pedido.Cliente.Nascimento = Convert.ToDateTime((from i in cliente.Descendants(pd + "birthdate")
                                                                    select i).ElementAt(0).Value);

                    var ddd = (from i in cliente.Descendants(pd + "ddd")
                               select i).ElementAt(0).Value;
                    var numberFone = (from i in cliente.Descendants(pd + "number")
                                      select i).ElementAt(0).Value;

                    pedido.Cliente.TelefoneResidencial = String.Concat(ddd, numberFone);

                    var enderecos = (from i in order.Descendants(pd + "addresses")
                                     select i);

                    var enderecoEnvio = (from i in enderecos.Descendants(pd + "shipping")
                                         select i);

                    pedido.EnderecoEnvio = new Endereco();
                    pedido.EnderecoEnvio.Codigo = (from i in enderecoEnvio.Descendants(pd + "id")
                                                   select i).ElementAt(0).Value;
                    pedido.EnderecoEnvio.NomeContato = (from i in enderecoEnvio.Descendants(pd + "contactName")
                                                        select i).ElementAt(0).Value;
                    pedido.EnderecoEnvio.Identificador = (from i in enderecoEnvio.Descendants(pd + "addressName")
                                                          select i).ElementAt(0).Value;
                    pedido.EnderecoEnvio.Logradouro = (from i in enderecoEnvio.Descendants(pd + "address")
                                                       select i).ElementAt(0).Value;
                    pedido.EnderecoEnvio.Numero = (from i in enderecoEnvio.Descendants(pd + "number")
                                                   select i).ElementAt(0).Value;
                    pedido.EnderecoEnvio.Bairro = (from i in enderecoEnvio.Descendants(pd + "province")
                                                   select i).ElementAt(0).Value;
                    pedido.EnderecoEnvio.Complemento = (from i in enderecoEnvio.Descendants(pd + "complement")
                                                        select i).ElementAt(0).Value;
                    pedido.EnderecoEnvio.Cidade = (from i in enderecoEnvio.Descendants(pd + "city")
                                                   select i).ElementAt(0).Value;
                    pedido.EnderecoEnvio.Estado = (from i in enderecoEnvio.Descendants(pd + "state")
                                                   select i).ElementAt(0).Value;
                    pedido.EnderecoEnvio.CodigoPostal = (from i in enderecoEnvio.Descendants(pd + "zipcode")
                                                         select i).ElementAt(0).Value;
                    pedido.EnderecoEnvio.PontoDeReferencia = (from i in enderecoEnvio.Descendants(pd + "reference")
                                                              select i).ElementAt(0).Value;
                    var enderecoCobranca = (from i in enderecos.Descendants(pd + "billing")
                                            select i);

                    pedido.EnderecoCobranca = new Endereco();
                    pedido.EnderecoCobranca.Codigo = (from i in enderecoCobranca.Descendants(pd + "id")
                                                      select i).ElementAt(0).Value;
                    pedido.EnderecoCobranca.NomeContato = (from i in enderecoCobranca.Descendants(pd + "contactName")
                                                           select i).ElementAt(0).Value;
                    pedido.EnderecoCobranca.Identificador = (from i in enderecoCobranca.Descendants(pd + "addressName")
                                                             select i).ElementAt(0).Value;
                    pedido.EnderecoCobranca.Logradouro = (from i in enderecoCobranca.Descendants(pd + "address")
                                                          select i).ElementAt(0).Value;
                    pedido.EnderecoCobranca.Numero = (from i in enderecoCobranca.Descendants(pd + "number")
                                                      select i).ElementAt(0).Value;
                    pedido.EnderecoCobranca.Bairro = (from i in enderecoCobranca.Descendants(pd + "province")
                                                      select i).ElementAt(0).Value;
                    pedido.EnderecoCobranca.Complemento = (from i in enderecoCobranca.Descendants(pd + "complement")
                                                           select i).ElementAt(0).Value;
                    pedido.EnderecoCobranca.Cidade = (from i in enderecoCobranca.Descendants(pd + "city")
                                                      select i).ElementAt(0).Value;
                    pedido.EnderecoCobranca.Estado = (from i in enderecoCobranca.Descendants(pd + "state")
                                                      select i).ElementAt(0).Value;
                    pedido.EnderecoCobranca.CodigoPostal = (from i in enderecoCobranca.Descendants(pd + "zipcode")
                                                            select i).ElementAt(0).Value;
                    pedido.EnderecoCobranca.PontoDeReferencia = (from i in enderecoCobranca.Descendants(pd + "reference")
                                                                 select i).ElementAt(0).Value;



                    var carrinho = from i in doc.Descendants(pd + "cart")
                                   select i;
                    pedido.Carrinho = new CarrinhoCompra();
                    pedido.Carrinho.Codigo = (from i in doc.Descendants(pd + "cartId")
                                              select i).ElementAt(0).Value;


                    var ttDescCarrinho = (from i in carrinho.Descendants(pd + "totalDiscountAmount")
                                          select i);

                    pedido.Carrinho.ValorTotalDescontos = Convert.ToDecimal(ttDescCarrinho.ElementAt(0).Value) / 100;
                    var vlrTotalCarrinho = (from i in carrinho.Descendants(pd + "totalAmount")
                                            select i);
                    pedido.Carrinho.ValorTotal = Convert.ToDecimal(vlrTotalCarrinho.ElementAt(0).Value) / 100;
                    pedido.Carrinho.ItensPedido = new List<ItemPedido>();
                    var ListaItensPedido = (from i in doc.Descendants(pd + "orderLines")
                                            select i);



                    foreach (var item in ListaItensPedido)
                    {

                        var itemP = from i in item.Descendants(pd + "orderLine")
                                    select i;

                        var itemPedido = new ItemPedido();
                        itemPedido.Nome = (from i in itemP.Descendants(pd + "name")
                                           select i).ElementAt(0).Value;
                        itemPedido.Variacao = (from i in itemP.Descendants(pd + "variation")
                                               select i).ElementAt(0).Value;
                        itemPedido.CodigoSKU = (from i in itemP.Descendants(pd + "skuId")
                                                select i).ElementAt(0).Value;
                        itemPedido.CodigoProduto = (from i in itemP.Descendants(pd + "productId")
                                                    select i).ElementAt(0).Value;
                        itemPedido.Quantidade = Convert.ToInt16((from i in itemP.Descendants(pd + "quantity")
                                                                 select i).ElementAt(0).Value);
                        itemPedido.Kit = Convert.ToBoolean((from i in itemP.Descendants(pd + "isKit")
                                                            select i).ElementAt(0).Value);
                        itemPedido.PrecoDaLista = Convert.ToDecimal((from i in itemP.Descendants(pd + "listPrice")
                                                                     select i).ElementAt(0).Value) / 100;
                        itemPedido.PrecoDeVenda = Convert.ToDecimal((from i in itemP.Descendants(pd + "salePrice")
                                                                     select i).ElementAt(0).Value) / 100;
                        itemPedido.Desconto = Convert.ToDecimal((from i in itemP.Descendants(pd + "discount")
                                                                 select i).ElementAt(0).Value) / 100;
                        itemPedido.ValorTotal = Convert.ToDecimal((from i in itemP.Descendants(pd + "totalAmount")
                                                                   select i).ElementAt(0).Value) / 100;

                        pedido.Carrinho.ItensPedido.Add(itemPedido);

                    }


                    var frete = (from i in doc.Descendants(pd + "freight")
                                 select i);


                    pedido.Carrinho.CustoFrete = Convert.ToDecimal((from i in frete.Descendants(pd + "amount")
                                                                    select i).ElementAt(0).Value) / 100;
                    pedido.Carrinho.DescontoFrete = Convert.ToDecimal((from i in frete.Descendants(pd + "discount")
                                                                       select i).ElementAt(0).Value) / 100;
                    pedido.Carrinho.ValorTotalFrete = Convert.ToDecimal((from i in frete.Descendants(pd + "totalAmount")
                                                                         select i).ElementAt(0).Value) / 100;

                    var entregas = (from i in doc.Descendants(pd + "deliveries")
                                    select i);

                    var entrega = (from i in entregas.Descendants(pd + "delivery")
                                   select i);

                    pedido.Entrega = new Entrega();

                    pedido.Entrega.Tipo = (from i in entrega.Descendants(pd + "deliveryType")
                                           select i).ElementAt(0).Value;
                    pedido.Entrega.DataPrevista = Convert.ToDateTime((from i in entrega.Descendants(pd + "estimate")
                                                                      select i).ElementAt(0).Value);
                    pedido.Entrega.Valor = Convert.ToDecimal((from i in entrega.Descendants(pd + "price")
                                                              select i).ElementAt(0).Value) / 100;


                    var pagamento = from i in doc.Descendants(pd + "payment")
                                    select i;
                    pedido.Pagamento = new Dominio.BSeller.Pagamento();
                    pedido.Pagamento.TipoPagamento = (from i in pagamento.Descendants(pd + "type")
                                                      select i).ElementAt(0).Value;

                    pedido.Pagamento.NomeGateway = (from i in pagamento.Descendants(pd + "gateway")
                                                    select i).ElementAt(0).Value;

                    pedido.Pagamento.TokenGateway = (from i in pagamento.Descendants(pd + "token")
                                                     select i).ElementAt(0).Value;

                    var vlrPag = (from i in pagamento.Descendants(pd + "amount")
                                  select i);
                    var transactionId = (from i in pagamento.Descendants(pd + "transactionId")
                                         select i);
                    var sequencialNumber = (from i in pagamento.Descendants(pd + "sequencialNumber")
                                            select i);
                    var redirectUrl = (from i in pagamento.Descendants(pd + "redirectUrl")
                                       select i);


                    if (vlrPag.Count() != 0)
                        pedido.Pagamento.ValorPagamento = vlrPag.ElementAt(0).Value;
                    if (transactionId.Count() != 0)
                        pedido.Pagamento.IdTransacao = transactionId.ElementAt(0).Value;
                    if (sequencialNumber.Count() != 0)
                        pedido.Pagamento.SequencialTransacao = sequencialNumber.ElementAt(0).Value;
                    if (redirectUrl.Count() != 0)
                        pedido.Pagamento.LinkBoleto = redirectUrl.ElementAt(0).Value;

                    var cartao = (from c in pagamento.Descendants(pd + "creditCard")
                                  select c);

                    if (cartao.Count() != 0)
                    {

                        var brand = (from i in pagamento.Descendants(pd + "brand")
                                     select i);
                        var ownerName = (from i in pagamento.Descendants(pd + "ownerName")
                                         select i);
                        var number = (from i in pagamento.Descendants(pd + "number")
                                      select i);
                        var pin = (from i in pagamento.Descendants(pd + "pin")
                                   select i);
                        var expirationDate = (from i in pagamento.Descendants(pd + "expirationDate")
                                              select i);
                        var parcels = (from i in pagamento.Descendants(pd + "parcels")
                                       select i);
                        var interestRate = (from i in pagamento.Descendants(pd + "interestRate")
                                            select i);
                        var interestAmount = (from i in pagamento.Descendants(pd + "interestAmount")
                                              select i);
                        var totalAmount = (from i in pagamento.Descendants(pd + "totalAmount")
                                           select i);


                        pedido.Pagamento.Bandeira = brand.ElementAt(0).Value;
                        pedido.Pagamento.NomeClienteCartao = ownerName.ElementAt(0).Value;
                        pedido.Pagamento.NumeroClienteCartao = number.ElementAt(0).Value;
                        pedido.Pagamento.CodigoSegurancaCartao = pin.ElementAt(0).Value;
                        pedido.Pagamento.DataExpiracaoCartao = expirationDate.ElementAt(0).Value;
                        pedido.Pagamento.Parcelas = parcels.ElementAt(0).Value;
                        pedido.Pagamento.TaxaJuros = Convert.ToDecimal(interestRate.ElementAt(0).Value);
                        pedido.Pagamento.ValorJuros = Convert.ToDecimal(interestAmount.ElementAt(0).Value);
                        pedido.Pagamento.ValorTotal = Convert.ToDecimal(totalAmount.ElementAt(0).Value);


                    }




                }

#pragma warning disable 168
                var pedidoReturn = Gravar(pedido);
#pragma warning restore 168

                return pedido;

            }
            catch (Exception ex)
            {
                new LOG_INTEGRACAO { COD_PEDIDO_BSELLER = 0, DATA_CRIACAO = DateTime.Now, IND_ERRO = true, MENSAGEM = "FALHA NO PROCESSAMENTO DO PEDIDO.VERIFIQUE XML - " + ex.Message, XML = xmlPedido, ROTINA = "ProcessarPedido", PARAMETROS_INTEGRACAO = " " };
                return null;
            }

        }

        public Boolean AtualizarStatusItensPedido(Pedido pedido, int codigoStatusSiga, string nomeUsuario)
        {
            var nomeStatus = SituacaoSIGA(codigoStatusSiga);
            var codPedidoBSeller = Convert.ToInt64(pedido.Codigo);

            var ped = (from p in db.PEDIDO
                       where p.COD_PEDIDO_BSELLER == codPedidoBSeller
                       select p).FirstOrDefault();

            if (ped != null)
            {

                var itensPedido = (from i in db.PEDIDO_CARRINHO
                                   where i.COD_PEDIDO == ped.COD_PEDIDO
                                   select i).ToList();

                if (itensPedido != null)
                {

                    foreach (var item in itensPedido)
                    {
                        item.COD_SITUACAO_INTEGRACAO_SIGA = codigoStatusSiga;
                        db.SaveChanges();

                        new Logs().Inserir(new LOG_INTEGRACAO
                        {
                            COD_PEDIDO_BSELLER = ped.COD_PEDIDO_BSELLER,
                            MENSAGEM = "OK: Status do item alterado para " + nomeStatus,
                            DATA_CRIACAO = DateTime.Now,
                            ROTINA = "AtualizarStatusItensPedido",
                            COD_PRODUTO = item.COD_PRODUTO,
                            COD_PEDIDO_API = item.COD_PEDIDO,
                            NOME_USUARIO = nomeUsuario,
                            IND_ERRO = false


                        });



                    }
                }


                var pedidoPagamento = (from pp in db.PEDIDO_PAGAMENTO
                                       where pp.COD_PEDIDO == ped.COD_PEDIDO
                                       select pp).FirstOrDefault();


                if (pedidoPagamento != null)
                {
                    pedidoPagamento.ID_TRANSACAO = pedido.Pagamento.IdTransacao ?? " ";
                    pedidoPagamento.NUM_AUTORIZACAO = pedido.Pagamento.NumeroAutorizacao ?? " ";

                    db.SaveChanges();

                }

            }

            return true;
        }

        public Order ConsultarPedido(string CodigoPedidoBSeller)
        {

            try
            {

                var rotina = new Rotinas().GetByID((int)Listas.Rotinas.ObterPedido);
                return (Order)new EventosREST().Request(CodigoPedidoBSeller, rotina, null, false);
            }
            catch (Exception) { return null; }
        }

        public Pedido ConsultarPedidoPeloCodigo(String CodigoPedidoBseller)
        {

            try
            {

                var strConsultaPedido = String.Format("https://secure.damasio.com.br/commerce-api/api/order/{0}", CodigoPedidoBseller);
                var rest = new EventosREST().Request(strConsultaPedido, "GET");

                return ProcessarPedido(rest, "http://kanlo.com.br/order");

            }

            catch (Exception) { return null; }

        }

        public string AtualizaDadosAlunoPedido(Aluno aluno)
        {
            try
            {
                var retorno = ValidacoesAtualizaDadosAlunoPedido(aluno);
                if (retorno != "")
                {
                    return retorno;
                }

                Int64 Codigo = Convert.ToInt64(aluno.ID);
                var integraSiga = new WsIntegracaoPortalSoapClient();

                var alu = (from p in db.PEDIDO_CLIENTE where p.ID == Codigo select p).FirstOrDefault();

                if (alu != null)
                {
                    //alu.COD_CLIENTE_BSELLER = aluno.CodigoBSeller;
                    alu.CPF_CNPJ = Validacoes.TrataCpfCnpj(aluno.Documento);
                    alu.DAT_ANIVERSARIO = Convert.ToDateTime(aluno.DataNascimento);
                    alu.DDD_TELEFONE_1 = Validacoes.TrataTelefone(aluno.Telefone1, 1);
                    alu.TELEFONE_1 = Validacoes.TrataTelefone(aluno.Telefone1, 2);
                    alu.DDD_TELEFONE_2 = Validacoes.TrataTelefone(aluno.Telefone2, 1);
                    alu.TELEFONE_2 = Validacoes.TrataTelefone(aluno.Telefone2, 2);
                    alu.EMAIL = aluno.Email;
                    alu.SEXO = aluno.Sexo;
                    alu.NOME = aluno.Nome;
                    alu.REENVIO_SIGA = true;

                    db.SaveChanges();

                    var endereco = (from p in db.PEDIDO_ENDERECO
                                    where p.COD_PEDIDO == alu.COD_PEDIDO
                                    select p).ToList();


                    foreach (var pedidoEndereco in endereco)
                    {
                        switch (pedidoEndereco.NOME_ENDERECO)
                        {
                            case "Envio":
                                pedidoEndereco.CPF_CNPJ = Validacoes.TrataCpfCnpj(aluno.Documento);
                                pedidoEndereco.EMAIL = aluno.Email;
                                pedidoEndereco.BAIRRO = aluno.Bairro;
                                pedidoEndereco.CEP = Validacoes.TrataCep(aluno.CEP);
                                pedidoEndereco.NUMERO = Validacoes.TrataNumero(aluno.Numero);
                                pedidoEndereco.COMPLEMENTO = Validacoes.TrataNumero(aluno.Complemento);
                                pedidoEndereco.NOME_CONTATO = aluno.Nome;

                                var endAluno = integraSiga.ObterEndereco(pedidoEndereco.CEP);
                                if (endAluno == null)
                                {
                                    pedidoEndereco.CIDADE = aluno.Cidade;
                                    pedidoEndereco.ENDERECO = aluno.Logradouro;
                                    pedidoEndereco.BAIRRO = aluno.Bairro;
                                    pedidoEndereco.ESTADO = "";
                                }
                                else
                                {
                                    pedidoEndereco.CIDADE = endAluno.Cidade.Nome;
                                    pedidoEndereco.ENDERECO = endAluno.Rua;
                                    pedidoEndereco.BAIRRO = endAluno.Bairro;
                                    pedidoEndereco.ESTADO = endAluno.Cidade.Uf.Nome;
                                }

                                pedidoEndereco.REENVIO_SIGA = true;
                                break;
                            case "Cobrança":
                                pedidoEndereco.CPF_CNPJ = Validacoes.TrataCpfCnpj(aluno.Responsavel.Documento);
                                pedidoEndereco.EMAIL = aluno.Responsavel.Email;
                                pedidoEndereco.CEP = Validacoes.TrataCep(aluno.Responsavel.CEP);
                                pedidoEndereco.NUMERO = Validacoes.TrataNumero(aluno.Responsavel.Numero);
                                pedidoEndereco.COMPLEMENTO = Validacoes.TrataNumero(aluno.Responsavel.Complemento);
                                pedidoEndereco.NOME_CONTATO = aluno.Responsavel.Nome;

                                var endAlunoCobr = integraSiga.ObterEndereco(pedidoEndereco.CEP);
                                if (endAlunoCobr == null)
                                {
                                    pedidoEndereco.CIDADE = aluno.Responsavel.Cidade;
                                    pedidoEndereco.ENDERECO = aluno.Responsavel.Logradouro;
                                    pedidoEndereco.BAIRRO = aluno.Responsavel.Bairro;
                                    pedidoEndereco.ESTADO = "";
                                }
                                else
                                {
                                    pedidoEndereco.CIDADE = endAlunoCobr.Cidade.Nome;
                                    pedidoEndereco.ENDERECO = endAlunoCobr.Rua;
                                    pedidoEndereco.BAIRRO = endAlunoCobr.Bairro;
                                    pedidoEndereco.ESTADO = endAlunoCobr.Cidade.Uf.Nome;
                                }

                                pedidoEndereco.REENVIO_SIGA = true;
                                break;
                        }

                        db.SaveChanges();
                    }
                }

                return "";
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        private string ValidacoesAtualizaDadosAlunoPedido(Aluno aluno)
        {
            bool retorno;

            if (string.IsNullOrEmpty(aluno.Nome))
            {
                return "Nome é obrigatório";
            }

            var campo = Validacoes.TrataCpfCnpj(aluno.Documento);

            if (campo == null)
            {
                return "CPF é obrigatório.";
            }
            switch (campo.Length)
            {
                case 11:
                    if (Validacoes.ValidaCPF(campo) == false)
                    {
                        return "CPF é inválido.";
                    }
                    break;
                case 14:
                    if (Validacoes.ValidaCNPJ(campo) == false)
                    {
                        return "CNPJ é inválido.";
                    }
                    break;
            }

            if (string.IsNullOrEmpty(aluno.DataNascimento))
            {
                return "Data de Nascimento é obrigatória.";
            }

            retorno = Validacoes.ValidarData(aluno.DataNascimento);
            if (retorno == false)
            {
                return "Data de Nascimento é inválida.";
            }

            if (string.IsNullOrEmpty(aluno.Sexo))
            {
                return "Sexo é obrigatório";
            }

            if (string.IsNullOrEmpty(aluno.Email))
            {
                return "E-mail é obrigatório.";
            }

            retorno = Validacoes.ValidarEmail(aluno.Email);
            if (retorno == false)
            {
                return "E-mail é inválido.";
            }

            if (string.IsNullOrEmpty(aluno.Telefone1))
            {
                return "Telefone 1 é obrigatório";
            }

            if (string.IsNullOrEmpty(aluno.Logradouro))
            {
                return "Endereço é obrigatório";
            }

            if (string.IsNullOrEmpty(aluno.Numero))
            {
                return "Número é obrigatório";
            }

            if (string.IsNullOrEmpty(aluno.Bairro))
            {
                return "Bairro é obrigatório";
            }

            if (string.IsNullOrEmpty(aluno.Cidade))
            {
                return "Cidade é obrigatória";
            }

            campo = Validacoes.TrataCep(aluno.CEP);
            if (campo == null)
            {
                return "CEP é obrigatório.";
            }

            if (Validacoes.ValidaCEP(aluno.CEP) == false)
            {
                return "CEP é inválido.";
            }

            if (string.IsNullOrEmpty(aluno.Responsavel.Nome))
            {
                return "Nome do Responsável é obrigatório";
            }


            campo = Validacoes.TrataCpfCnpj(aluno.Responsavel.Documento);
            if (campo == null)
            {
                return "CPF do Responsável é obrigatório.";
            }
            switch (campo.Length)
            {
                case 11:
                    if (Validacoes.ValidaCPF(campo) == false)
                    {
                        return "CPF do Responsável é inválido.";
                    }
                    break;
                case 14:
                    if (Validacoes.ValidaCNPJ(campo) == false)
                    {
                        return "CNPJ do Responsável é inválido.";
                    }
                    break;
            }

            if (string.IsNullOrEmpty(aluno.Responsavel.Email))
            {
                return "E-mail do Responsável é obrigatório.";
            }

            retorno = Validacoes.ValidarEmail(aluno.Responsavel.Email);
            if (retorno == false)
            {
                return "E-mail do Responsável é inválido.";
            }

            if (string.IsNullOrEmpty(aluno.Responsavel.Logradouro))
            {
                return "Endereço do Responsável é obrigatório";
            }

            if (string.IsNullOrEmpty(aluno.Responsavel.Numero))
            {
                return "Número do Responsável é obrigatório";
            }

            if (string.IsNullOrEmpty(aluno.Responsavel.Bairro))
            {
                return "Bairro do Responsável é obrigatório";
            }

            if (string.IsNullOrEmpty(aluno.Responsavel.Cidade))
            {
                return "Cidade do Responsável é obrigatória";
            }

            campo = Validacoes.TrataCep(aluno.Responsavel.CEP);
            if (campo == null)
            {
                return "CEP Responsável é obrigatório.";
            }

            if (Validacoes.ValidaCEP(aluno.Responsavel.CEP) == false)
            {
                return "CEP Responsável é inválido.";
            }


            //if (string.IsNullOrEmpty(aluno.Responsavel.DataNascimento))
            //{
            //    return "Data de Nascimento do Responsável é obrigatória.";
            //}

            //retorno = Validacoes.ValidarData(aluno.Responsavel.DataNascimento);
            //if (retorno == false)
            //{
            //    return "Data de Nascimento do Responsável é inválida.";
            //}

            return "";
        }
    }
}
