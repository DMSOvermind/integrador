﻿using DAMASIO.Integracao.Dominio.API;
using DAMASIO.Integracao.Dominio.BSeller;
using DAMASIO.Integracao.Utils;
using DAMASIO.Integracao.DAL;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using DAMASIO.Integracao.DAL;
using DAMASIO.Integracao.wsSiga;
using System.Web.Configuration;
using RestSharp.Authenticators;

namespace DAMASIO.Integracao.Negocio
{
    public class Produtos
    {
        INTEGRACAOEntities db = new INTEGRACAOEntities();
        Logs log = new Logs { DATA_CRIACAO = DateTime.Now, NOME_USUARIO = "Envio Automático/BSELLER", IND_ERRO = true, IND_EXIBE_LOG = true, DESTINO_INTEGRACAO = "SIGA", PARAMETROS_INTEGRACAO = " " };

        public int ObterCodigoEmpresa(Int32 codigoMarca)
        {
            var config = (from c in db.CONFIG_EMPRESA_MARCA
                          where c.COD_MARCA_BSELLER == codigoMarca
                          select c).FirstOrDefault();

            return config != null ? Convert.ToInt32(config.COD_EMPRESA_SIGA) : 0;
        }

        public Int64 ObterCodigoContratoDoPedido(Int64 codPedidoBSeller)
        {


            var contrato = (from c in db.PEDIDO
                            where c.COD_PEDIDO == codPedidoBSeller
                            select c).FirstOrDefault();

            return Convert.ToInt64(contrato);

        }

        public ProdutoBSeller ConsultarIDexterno(string codigo, bool FichaTecnica)
        {
            try
            {
                string codigoTurmaFormatado = String.Concat("00000", codigo);
                codigoTurmaFormatado = codigoTurmaFormatado.Substring(codigoTurmaFormatado.Length - 5);

                var rotina = new Rotinas().GetByID((int)Listas.Rotinas.ObterDadosProdutoPeloIdExterno);
                var produto = (ProdutoBSeller)new EventosREST().Request(codigoTurmaFormatado, rotina, null, FichaTecnica);

                if (produto == null || produto.id.ToString().Length == 0)
                {
                    log.COD_PRODUTO = Convert.ToInt32(codigo);
                    log.DATA_CRIACAO = DateTime.Now;
                    log.MENSAGEM = "ERRO API BSELLER: Falha ao consultar o produto pela API";
                    log.NOME_USUARIO = "Envio automático";
                    log.ROTINA = "ConsultarIDexterno";

                    new Logs().Inserir(log);
                }

                return produto;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public ProdutoBSeller getInfoProdutoCompletoBSeller(string codigoTurmaFormatado)
        {
            try
            {

                var client = new RestClient("https://secure.damasio.com.br");
                client.Authenticator = new HttpBasicAuthenticator(WebConfigurationManager.AppSettings["BSELLER_USERNAME"], WebConfigurationManager.AppSettings["BSELLER_PASSWORD"]);

                var request = new RestRequest("/commerce-api/api/product/external/" + codigoTurmaFormatado + "?showinfo=true", Method.GET);

                IRestResponse<ProdutoBSeller> response = client.Execute<ProdutoBSeller>(request);

                return response.Data;

            }
            catch (Exception) { return null; }
        }

        public ProdutoBSeller ConsultarIDexterno(string codigo)
        {
            try
            {
                string codigoTurmaFormatado = String.Concat("00000", codigo);
                codigoTurmaFormatado = codigoTurmaFormatado.Substring(codigoTurmaFormatado.Length - 5);

                var rotina = new Rotinas().GetByID((int)Listas.Rotinas.ObterDadosProdutoPeloIdExterno);
                var produto = (ProdutoBSeller)new EventosREST().Request(codigoTurmaFormatado, rotina, null, true);

                if (produto == null || produto.id.ToString().Length == 0)
                {
                    log.COD_PRODUTO = Convert.ToInt32(codigo);
                    log.MENSAGEM = "ERRO API BSELLER: Falha ao consultar a turma (produto) pela API";
                    log.NOME_USUARIO = "Envio automático";
                    log.ROTINA = "ConsultarIDexterno";
                    log.DESCRICAO_INTEGRACAO = "-";
                    log.Inserir();
                    produto = null;
                }

                if (produto.id == 0)
                {
                    log.COD_PRODUTO = Convert.ToInt32(codigo);
                    log.MENSAGEM = "ERRO API BSELLER: Falha ao consultar a turma (produto) produto pela API";
                    log.NOME_USUARIO = "Envio automático";
                    log.ROTINA = "ConsultarIDexterno";
                    log.DESCRICAO_INTEGRACAO = "-";
                    log.Inserir();
                    produto = null;

                }

                return produto;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public ProdutoBSeller ConsultarID(string codigo)
        {
            var rotina = new Rotinas().GetByID((int)Listas.Rotinas.ObterDadosProdutoPeloIdBSELLER);
            var produto = (ProdutoBSeller)new EventosREST().Request(codigo, rotina, null, true);

            return produto;
        }

        public Produto ConsultarPeloID(Int64 codigo)
        {
            var strConsultaProduto = String.Format("https://damasioapi.bseller.com.br/commerce-api/api/product/{0}?showInfo=false", codigo);
            var rest = new EventosREST().Request(strConsultaProduto, "GET");

            XDocument doc = XDocument.Parse(rest);
            XElement produtoBSeller = doc.Root;

            var produtoElements = produtoBSeller.Elements();

            var produto = new Produto();
            produto.Codigo = Convert.ToInt64(produtoElements.ElementAt(0).Value);
            produto.CodigoExterno = produtoElements.ElementAt(1).Value;
            produto.Nome = produtoElements.ElementAt(2).Value;
            produto.Descricao = produtoElements.ElementAt(5).Value;
            produto.DisponivelParaVenda = Convert.ToBoolean(produtoElements.ElementAt(13).Value);
            produto.Oculto = Convert.ToBoolean(produtoElements.ElementAt(14).Value);
            produto.Kit = Convert.ToBoolean(produtoElements.ElementAt(16).Value);
            produto.DataCadastro = Convert.ToDateTime(produtoElements.ElementAt(3).Value);
            produto.DataUltimaAlteracao = Convert.ToDateTime(produtoElements.ElementAt(4).Value);

            var precos = produtoElements.ElementAt(7).Descendants();
            produto.ValorVenda = Convert.ToDecimal(precos.ElementAt(0).Value) / 100;
            produto.ValorOriginal = Convert.ToDecimal(precos.ElementAt(1).Value) / 100;

            var categoria = produtoElements.ElementAt(8);
            produto.NomeCategoria = categoria.Descendants().ElementAt(0).Value;
            produto.CodigoCategoria = Convert.ToInt64(categoria.Attribute("id").Value);

            var fornecedor = produtoElements.ElementAt(10);
            produto.NomeFornecedor = fornecedor.Descendants().ElementAt(0).Value;
            produto.CodigoFornecedor = Convert.ToInt64(fornecedor.Attribute("id").Value);

            return produto;
        }

        public PEDIDO_PAGAMENTO ObterPedidoPagamento(PEDIDO pedido)
        {
            var ped = (from pd in db.PEDIDO
                       where pd.COD_PEDIDO_BSELLER == pedido.COD_PEDIDO_BSELLER
                       select pd).FirstOrDefault();

            if (ped != null)
            {
                var result = (from p in db.PEDIDO_PAGAMENTO
                              where p.COD_PEDIDO == ped.COD_PEDIDO
                              select p).FirstOrDefault();


                return result;

            }

            return null;
        }

        public bool GerarContratoProdutoSIGA(Order order, ProdutoBSeller produto, Int64 CodigoPedidoAPI, string nomeUsuario)
        {
            /*ROTINA UTILIZADA PARA REENVIO DE CONTRATO*/
            //var retorno = new Retorno();
            try
            {
                log.COD_PRODUTO = Convert.ToInt32(produto.id);
                log.NOME_USUARIO = nomeUsuario;
                log.ROTINA = "GerarContratoProdutoSIGA";
                log.IND_ERRO = true;
                log.COD_PEDIDO_BSELLER = order.id;


                var Aluno = new AlunoDTO();
                var integraSiga = new WsIntegracaoPortalSoapClient();
                var pedidoClienteSiga = db.PEDIDO_CLIENTE.SingleOrDefault(x => x.COD_PEDIDO == CodigoPedidoAPI);
                var pedidoEnderecoSiga = db.PEDIDO_ENDERECO.Where(x => x.COD_PEDIDO == CodigoPedidoAPI).ToList();

                if (pedidoClienteSiga.REENVIO_SIGA == true && pedidoEnderecoSiga.Any(x => x.REENVIO_SIGA == true))
                {
                    Aluno.Cpf = pedidoClienteSiga.CPF_CNPJ;
                    Aluno.Nome = pedidoClienteSiga.NOME;
                    Aluno.Sexo = Convert.ToChar(pedidoClienteSiga.SEXO);
                    Aluno.DddTelefoneResidencial = pedidoClienteSiga.DDD_TELEFONE_1;
                    Aluno.TelefoneResidencial = pedidoClienteSiga.TELEFONE_1;
                    Aluno.DddTelefoneCelular = pedidoClienteSiga.DDD_TELEFONE_2;
                    Aluno.TelefoneCelular = pedidoClienteSiga.TELEFONE_2;
                    Aluno.Email = pedidoClienteSiga.EMAIL;

                    if (Aluno.Email == " ")
                    {
                        log.MENSAGEM = "AVISO: Email inválido.";
                        log.DESCRICAO_INTEGRACAO = "Informado pelo site:" + pedidoClienteSiga.DAT_ANIVERSARIO.Value.ToShortDateString();
                        log.IND_ERRO = false;
                        log.Inserir();
                    }

                    if (pedidoClienteSiga.DAT_ANIVERSARIO.Value.Year <= 1900) // O cliente informa uma data inválida
                    {
                        Aluno.DataNascimento = Convert.ToDateTime("02/10/1978");
                        log.MENSAGEM = "AVISO: Cliente informou data de nascimento inválida. Foi ajustada para data padrão.";
                        log.DESCRICAO_INTEGRACAO = "Informado pelo site:" + pedidoClienteSiga.DAT_ANIVERSARIO.Value.ToShortDateString();
                        log.IND_ERRO = false;
                        log.Inserir();
                    }
                    else
                    {
                        Aluno.DataNascimento = pedidoClienteSiga.DAT_ANIVERSARIO.Value;
                    }

                    //Responsável financeiro para menores
                    var idade = DateTime.Now.Year - pedidoClienteSiga.DAT_ANIVERSARIO.Value.Year;
                    if (idade < 18)
                    {
                        try
                        {
                            var responsavel = pedidoEnderecoSiga.FirstOrDefault(x => x.NOME_ENDERECO == "Cobrança");
                            Aluno.Responsavel = new ResponsavelDTO();

                            Aluno.Responsavel.Nome = responsavel.NOME_CONTATO;
                            Aluno.Responsavel.Cpf = responsavel.CPF_CNPJ;
                            Aluno.Responsavel.EstadoCivil = " ";

                            var endResponsavel = integraSiga.ObterEndereco(responsavel.CEP);
                            if (endResponsavel != null)
                            {

                                Aluno.Responsavel.Cep = endResponsavel.Cep;
                                Aluno.Responsavel.IdCidade = endResponsavel.Cidade.IdCidade;
                                Aluno.Responsavel.Bairro = endResponsavel.Bairro;
                                Aluno.Responsavel.IdUF = endResponsavel.Cidade.Uf.IdUf;
                            }
                            else
                            {
                                return false;
                            }

                            Aluno.Responsavel.Endereco = responsavel.ENDERECO;
                            //Fixei pois não existem no site e são obrigatórios no siga --precisa alterar isso depois
                            Aluno.Responsavel.Nascimento = Convert.ToDateTime("20/08/1956");
                            Aluno.Responsavel.EstadoCivil = "Casado";
                            Aluno.Responsavel.Sexo = Convert.ToChar("F");

                            //Contrato.NumDocRespFinanceiro = order.addresses.billing.complement;
                            //Contrato.NomeRespFinanceiro = order.addresses.billing.contactName;

                        }
                        catch (Exception ex)
                        {
                            log.IND_ERRO = true;
                            log.IND_EXIBE_LOG = true;
                            log.MENSAGEM = "ERRO: Falha na geração do contrato : " + ex.Message + " - Resp. Financeiro";
                            log.DESCRICAO_INTEGRACAO = "-";
                            log.COD_PEDIDO_BSELLER = order.id;
                            log.ROTINA = "GerarContratoSIGA";
                            log.Inserir();
                            return false;
                        }
                    }
                    //--------------------------------------------------------------------------
                    else
                    {
                        var pedidoEndereco = pedidoEnderecoSiga.FirstOrDefault(x => x.NOME_ENDERECO == "Envio");

                        Aluno.Cep = pedidoEndereco.CEP;
                        Aluno.IdEmpresaOrigem = 0;
                        Aluno.Escolaridade = 5;
                        Aluno.EstadoCivil = " ";
                        Aluno.IdTipoAluno = 1;
                        Aluno.IdProcedencia = 4;
                        Aluno.NomePai = " ";
                        Aluno.NomeMae = " ";

                        var endAluno = integraSiga.ObterEndereco(Aluno.Cep);
                        if (endAluno == null)
                        {
                            Aluno.Cep = "01510001";
                            endAluno = integraSiga.ObterEndereco(Aluno.Cep);
                            Aluno.Logradouro = "[VERIFICAR ENDEREÇO DO ALUNO]";

                            log.MENSAGEM = "AVISO: Endereço alterado para padrão. Motivo: CEP não encontrado na base SIGA.";
                            log.DESCRICAO_INTEGRACAO = "Informado pelo site:" + Aluno.Cep;
                            log.IND_ERRO = false;
                            log.Inserir();
                        }


                        var cidade = integraSiga.ObterCidade(endAluno.Cidade.IdCidade.ToString());

                        if (cidade == null)
                        {
                            Aluno.Cep = "01510001";
                            endAluno = integraSiga.ObterEndereco(Aluno.Cep);
                            Aluno.Logradouro = "[VERIFICAR ENDEREÇO DO ALUNO]";

                            log.MENSAGEM = "AVISO: Endereço alterado para padrão. Motivo: CIDADE não encontrada na base SIGA.";
                            log.DESCRICAO_INTEGRACAO = "Informado pelo site:" + endAluno.Cidade.Nome;
                            log.IND_ERRO = false;
                            log.Inserir();
                        }

                        Aluno.Logradouro = endAluno.Rua;
                        Aluno.Bairro = endAluno.Bairro.Length == 0 ? pedidoEndereco.BAIRRO : endAluno.Bairro;

                        if (endAluno.Bairro.Length >= 29)
                        {
                            Aluno.Bairro = endAluno.Bairro.Substring(0, 29);
                        }

                        Aluno.IdCidade = endAluno.Cidade.IdCidade;
                        Aluno.IdUF = endAluno.Cidade.Uf.IdUf;
                    }
                }
                else
                {
                    Aluno.Cpf = order.customer.document;
                    Aluno.Nome = order.customer.name;
                    Aluno.Sexo = Convert.ToChar(order.customer.gender.Substring(0, 1));
                    //Aluno.TelefoneResidencial = String.Concat(order.customer.phone.ddd, order.customer.phone.number);
                    Aluno.DddTelefoneCelular = order.customer.phone.ddd.ToString();
                    Aluno.TelefoneCelular = order.customer.phone.number ?? " ";
                    Aluno.Email = order.customer.email ?? " ";
                    //Aluno.DataNascimento = order.customer.birthdate;

                    if (Aluno.Email == " ")
                    {
                        log.MENSAGEM = "AVISO: Email inválido.";
                        log.DESCRICAO_INTEGRACAO = "Informado pelo site:" + order.customer.birthdate.ToShortDateString();
                        log.IND_ERRO = false;
                        log.Inserir();
                    }

                    if (order.customer.birthdate.Year <= 1900) // O cliente informa uma data inválida
                    {
                        Aluno.DataNascimento = Convert.ToDateTime("02/10/1978");
                        log.MENSAGEM = "AVISO: Cliente informou data de nascimento inválida. Foi ajustada para data padrão.";
                        log.DESCRICAO_INTEGRACAO = "Informado pelo site:" + order.customer.birthdate.ToShortDateString();
                        log.IND_ERRO = false;
                        log.Inserir();
                    }
                    else
                    {
                        Aluno.DataNascimento = order.customer.birthdate;
                    }

                    Aluno.Cep = order.addresses.shipping.zipcode;
                    Aluno.IdEmpresaOrigem = 0;
                    Aluno.Escolaridade = 5;
                    Aluno.EstadoCivil = " ";
                    Aluno.IdTipoAluno = 1;
                    Aluno.IdProcedencia = 4;
                    Aluno.NomePai = " ";
                    Aluno.NomeMae = " ";
                    Aluno.Logradouro = order.addresses.shipping.address;

                    var endAluno = integraSiga.ObterEndereco(Aluno.Cep);
                    if (endAluno == null)
                    {
                        Aluno.Cep = "01510001";
                        endAluno = integraSiga.ObterEndereco(Aluno.Cep);
                        Aluno.Logradouro = "[VERIFICAR ENDEREÇO DO ALUNO]";

                        log.MENSAGEM = "AVISO: Endereço alterado para padrão. Motivo: CEP não encontrado na base SIGA.";
                        log.DESCRICAO_INTEGRACAO = "Informado pelo site:" + Aluno.Cep;
                        log.IND_ERRO = false;
                        log.Inserir();
                    }

                    Aluno.Bairro = endAluno.Bairro.Length == 0 ? order.addresses.billing.province : endAluno.Bairro;

                    if (endAluno.Bairro.Length >= 29)
                    {
                        Aluno.Bairro = endAluno.Bairro.Substring(0, 29);
                    }

                    Aluno.IdCidade = endAluno.Cidade.IdCidade;
                    Aluno.IdUF = endAluno.Cidade.Uf.IdUf;

                }

                var Contrato = new ContratoDTO();

                ProdutoBSeller produtoCarrinho = produto;
                if (produtoCarrinho.brand != null)
                 //   produtoCarrinho.brand.id = new Produtos().ObterCodigoEmpresa(produtoCarrinho.brand.id); //atualiza o id da marca com o código da empresa que está na base

                //Tratamento de quando o brand da bseller vier definido como 0 atribuir 15, valor default de venda.
                //if (produtoCarrinho.brand.id == 0)
                //{
                    produtoCarrinho.brand.id = 15;
                //}
                else
                {
                    log.MENSAGEM = "ERRO: Falha ao gerar contrato. Motivo:Produto (Turma) sem marca (Empresa) definida.";
                    log.IND_ERRO = true;
                    log.IND_EXIBE_LOG = true;
                    log.COD_PEDIDO_BSELLER = order.id;
                    log.Inserir();

                    new Pedidos().AtualizarStatusItemPedido(new Produto { Codigo = produtoCarrinho.id }, new Pedido { Codigo = CodigoPedidoAPI.ToString() }, (int)Listas.StatusPedidoSiga.ErroNoEnvio, "", nomeUsuario);

                    return false;
                }

                Aluno.IdEmpresaOrigem = produtoCarrinho.brand.id;

                var CodigoExternoProduto = Convert.ToInt32(produtoCarrinho.externalId);

                var cartItem = (from c in order.cart.OrderLines
                                where c.productId == CodigoExternoProduto
                                select c).FirstOrDefault();

                if (cartItem != null)
                {
                    //if (retorno.success == true)
                    //{

                    Contrato.Aluno = Aluno;
                    Contrato.Emissao = DateTime.Now;
                    Contrato.IdTurma = produtoCarrinho.externalId;
                    Contrato.IdUsuario = 0;
                    Contrato.IdUsuarioEfetivacao = 0;
                    Contrato.IdEmpresa = produtoCarrinho.brand.id;
                    Contrato.IdEmpresaOrigem = produtoCarrinho.brand.id;
                    Contrato.ValorGlobal = cartItem.salePrice - cartItem.discount;
                    //Contrato.ValorCurso = cartItem.salePrice;
                    //Contrato.ValorVista = cartItem.salePrice;

                    var codCurso = integraSiga.ObterIdCurso(Contrato.IdEmpresa, Contrato.IdTurma);
                    Contrato.IdCurso = codCurso;


                    if (Decimal.Compare(cartItem.salePrice, (decimal)0.01) == 0)
                    {
                        Contrato.ValorCurso = 0;
                        Contrato.ValorVista = 0;
                        Contrato.ValorGlobal = 0;
                        Contrato.NumParcelas = 0;
                    }
                    else
                    {
                        Contrato.ValorCurso = cartItem.salePrice;
                        Contrato.ValorVista = Contrato.ValorGlobal;
                        Contrato.NumParcelas = order.payment.creditCard.parcels;

                    }

                    Contrato.ValorEntrada = 0;
                    Contrato.ValorDescontoSite = order.cart.totalDiscountAmount;
                    Contrato.IdStatus = Convert.ToChar("A");
                    Contrato.IdTipoContrato = Convert.ToChar("N");
                    Contrato.IdPedido = order.id;
                    //Responsável financeiro para menores
                    var idade = DateTime.Now.Year - order.customer.birthdate.Year;
                    if (idade < 18)
                    {

                        try
                        {
                            Contrato.NumDocRespFinanceiro = order.addresses.billing.complement;
                            Contrato.NomeRespFinanceiro = order.addresses.billing.contactName ?? "Não informado";
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    }

                    if (Contrato.NumParcelas > 0)
                    {
                        var transactionPayment = ObterPedidoPagamento(new PEDIDO { COD_PEDIDO_BSELLER = order.id });

                        //var venctoPadrao = DateTime.Now;
                        DateTime venctoPadrao = order.dateCreated; // alterado para contemplar os pedidos reenviados fora da data

                        var valorParcela = order.payment.amount / order.payment.creditCard.parcels;

                        Contrato.Parcelas = new ParcelaDTO[Contrato.NumParcelas];

                        for (int i = 0; i < Contrato.NumParcelas; i++)
                        {
                            Contrato.Parcelas[i] = new ParcelaDTO();

                            Contrato.Parcelas[i].IdEmpresa = produtoCarrinho.brand.id;
                            //Contrato.Parcelas[i].NumeroCartao = order.payment.creditCard.number;
                            //Alterado pois o Siga grava o número do cartão no campo documento,
                            Contrato.Parcelas[i].NumeroCartao = transactionPayment.NUM_AUTORIZACAO ?? "--";

                            Contrato.Parcelas[i].NumeroParcela = i + 1;
                            Contrato.Parcelas[i].Vencimento = venctoPadrao.AddMonths(i + 1);
                            Contrato.Parcelas[i].VencimentoAceito = venctoPadrao.AddMonths(i + 1);
                            Contrato.Parcelas[i].ValorParcela = valorParcela;

                            Contrato.Parcelas[i].IdTipLancamento = 1;

                            //Buscando estas informações do BD pois não são consultadas na Order
                            Contrato.Parcelas[i].TidNsu = transactionPayment.NUM_ACQUIRER_TRANSACTION_ID ?? "--";
                            Contrato.Parcelas[i].NumAutorizacao = transactionPayment.NUM_AUTORIZACAO ?? "--";

                            //if (order.payment.income != null)
                            //    Contrato.Parcelas[i].NumAutorizacao = order.payment.income.authorizationCode ?? "--";


                            switch (order.payment.creditCard.brand)
                            {
                                case "VISA":
                                    Contrato.Parcelas[i].Bandeira = (int)BandeiraCartao.VISA;
                                    break;
                                case "MASTERCARD":
                                    Contrato.Parcelas[i].Bandeira = (int)BandeiraCartao.MASTERCARD;
                                    break;
                            }

                            Contrato.Parcelas[i].IdFormaPagamento = 12; //Cartão internet - Fixo no SIGA
                        }
                    }

                    var novoContrato = integraSiga.EmitirContrato(Contrato);

                    var validacao = new Pedidos().validacaoDadosAluno(Contrato, cartItem.productId);

                    if (validacao == false)
                    {
                        new Pedidos().AtualizarStatusItemPedido(new Produto { Codigo = cartItem.productId }, new Pedido { Codigo = CodigoPedidoAPI.ToString() }, (int)Listas.StatusPedidoSiga.ErroNoEnvio, "", nomeUsuario);
                        return validacao;
                    }

                    if (novoContrato.IdContrato != 0)
                    {

                        if (new Pedidos().AtualizarStatusContrato(new Produto { Codigo = cartItem.productId }, novoContrato, new Pedido { Codigo = CodigoPedidoAPI.ToString() }))
                        {
                            log.MENSAGEM = "SUCESSO: Contrato gerado com sucesso no SIGA. Código: " + novoContrato.IdContrato;
                            log.DESCRICAO_INTEGRACAO = "-";
                            log.IND_ERRO = false;
                            log.COD_PEDIDO_BSELLER = order.id;
                            log.COD_PRODUTO = cartItem.productId;
                            log.Inserir();

                            new Pedidos().AtualizarStatusSigaPedido(new Pedido { Codigo = CodigoPedidoAPI.ToString() }, (int)Listas.StatusPedidoSiga.EnviadoComSucesso);
                        }
                    }
                    else
                    {

                        log.MENSAGEM = "ERRO: Contrato não foi gerado. Motivo: " + novoContrato.MsgErro;
                        log.DESCRICAO_INTEGRACAO = "-";
                        log.IND_ERRO = true;
                        log.IND_EXIBE_LOG = true;
                        log.COD_PEDIDO_BSELLER = order.id;
                        log.COD_PRODUTO = cartItem.productId;
                        log.COD_PEDIDO_API = CodigoPedidoAPI;
                        log.Inserir();
                        if (novoContrato.MsgErro.Contains("O Aluno já esta matriculado"))
                        {
                            new Pedidos().AtualizarStatusItemPedido(new Produto { Codigo = cartItem.productId }, new Pedido { Codigo = CodigoPedidoAPI.ToString() }, (int)Listas.StatusPedidoSiga.EnviadoComSucesso, "", nomeUsuario);
                            new Pedidos().AtualizarStatusSigaPedido(new Pedido { Codigo = CodigoPedidoAPI.ToString() }, (int)Listas.StatusPedidoSiga.EnviadoComSucesso);
                        }
                        else
                        {
                            new Pedidos().AtualizarStatusItemPedido(new Produto { Codigo = cartItem.productId }, new Pedido { Codigo = CodigoPedidoAPI.ToString() }, (int)Listas.StatusPedidoSiga.ErroNoEnvio, "", nomeUsuario);
                        }
                    }
                }
                else
                {
                    log.MENSAGEM = "ERRO: Falha ao gerar contrato. Motivo: Empresa Inválida.";
                    log.DESCRICAO_INTEGRACAO = "-";
                    log.IND_ERRO = true;
                    log.IND_EXIBE_LOG = true;
                    log.COD_PEDIDO_BSELLER = order.id;
                    log.COD_PRODUTO = cartItem.productId;
                    log.COD_PEDIDO_API = CodigoPedidoAPI;
                    log.Inserir();

                    new Pedidos().AtualizarStatusItemPedido(new Produto { Codigo = cartItem.productId }, new Pedido { Codigo = CodigoPedidoAPI.ToString() }, (int)Listas.StatusPedidoSiga.ErroNoEnvio, "", nomeUsuario);
                }
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
