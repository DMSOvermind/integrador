﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAMASIO.Integracao.DAL;
using DAMASIO.Integracao.Dominio.dto;
using DAMASIO.Integracao.Dominio.API;


namespace DAMASIO.Integracao.Negocio
{
    public class Logs : LOG_INTEGRACAO
    {
        INTEGRACAOEntities db = new INTEGRACAOEntities();

        public List<GridLog> ObterLogs(bool erro, bool novo, bool integradoSucesso, bool integracaoPendente, bool trasacoesDestinoSiga, bool transacoesDestinoSite, string dataMin, string dataMax)
        {
            var lista = new List<GridLog>();

            try
            {

                var Listalog = db.SP_GET_LOGS_POR_DATA(erro, novo, integradoSucesso, integracaoPendente, trasacoesDestinoSiga, transacoesDestinoSite, dataMin, dataMax).ToList();

                //List<VW_HDR_LOGS_INDEX> Listalog = (from l in db.SP_GET_LOGS_POR_DATA
                //                               select l).Take(3000).ToList();


                foreach (var log in Listalog)
                {

                    var linha = new GridLog();
                    linha.Codigo = Convert.ToInt32(log.CODIGO);
                    linha.DataLog = Convert.ToDateTime(log.DATA_CRIACAO);
                    linha.DataUltimaAtualizacao = Convert.ToDateTime(log.DATA_ULT_ATUALIZACAO);
                    linha.Solicitacao = log.TIPO_SOLICITACAO;

                    if (log.DESTINO_INTEGRACAO == "SITE")
                        linha.Turma = String.Concat(log.CODIGO.ToString(), " - ", log.DESCRICAO_INTEGRACAO ?? " ");
                    else
                        linha.Turma = log.DESCRICAO_INTEGRACAO ?? " ";

                    linha.Destino = log.DESTINO_INTEGRACAO ?? " - ";
                    linha.Pendente = (log.STATUS_SOLICITACAO == "ERRO") ? true : false;
                    linha.Status = log.STATUS_SOLICITACAO;
                    if (log.STATUS_SOLICITACAO == "PROCESSANDO")
                    {
                        linha.Status = "NOVO";
                        linha.Pendente = true;
                    }

                    lista.Add(linha);

                }
                return lista;

            }
            catch (Exception) { return null; }




        }

        public List<HistoricoItemLog> ObterHistoricoLog(Int64 codigo, string destinoIntegracao, string descricao)
        {
            var lista = new List<HistoricoItemLog>();

            try
            {
                var Listalog = new List<LOG_INTEGRACAO>();

                if (destinoIntegracao == "SITE")
                {
                    Listalog = (from l in db.LOG_INTEGRACAO
                                where l.COD_PRODUTO == codigo && l.IND_EXIBE_LOG && l.DESTINO_INTEGRACAO == "SITE"
                                orderby l.DATA_CRIACAO
                                select l).ToList();
                }
                else
                {
                    Listalog = (from l in db.LOG_INTEGRACAO
                                where l.COD_PEDIDO_BSELLER == codigo && l.IND_EXIBE_LOG && l.DESTINO_INTEGRACAO == "SIGA"
                                orderby l.DATA_CRIACAO
                                select l).ToList();
                }

                var linha = new HistoricoItemLog();
                if (Listalog != null)
                {
                    linha.CodigoProduto = Convert.ToInt32(Listalog[0].COD_PRODUTO);
                    linha.DataCriacao = Convert.ToDateTime(Listalog[0].DATA_CRIACAO);
                    //linha.Turma = String.Concat(Listalog[0].COD_PRODUTO.ToString() ?? " ", " - ", Listalog[0].DESCRICAO_INTEGRACAO ?? " ");
                    linha.Turma = descricao;
                    linha.Destino = Listalog[0].DESTINO_INTEGRACAO ?? " - ";

                    switch (Listalog[0].ROTINA)
                    {
                        case "AtualizarInfoTurma":
                            linha.Solicitacao = "Alteração Info. Comercial";
                            break;
                        case "AtualizarStatusContrato":
                            linha.Solicitacao = "Atualização Pedido";
                            break;
                        case "AtualizarStatusItemPedido":
                            linha.Solicitacao = "Atualização Pedido";
                            break;
                        case "AtualizarStatusItensPedido":
                            linha.Solicitacao = "Atualização Pedido";
                            break;
                        case "AtualizarValorTurma":
                            linha.Solicitacao = "Atualização Valor Turma";
                            break;
                        case "ConsultarIDexterno":
                            linha.Solicitacao = "Consulta API BSeller";
                            break;
                        case "GerarContratoSIGA":
                            linha.Solicitacao = "Geração Contrato SIGA";
                            break;
                        case "ProcessarPedido":
                            linha.Solicitacao = "Geração Contrato SIGA";
                            break;
                        case "ReenviarItem":
                            linha.Solicitacao = "Geração Contrato SIGA";
                            break;
                        default:
                            linha.Solicitacao = "-";
                            break;
                    }

                    linha.Items = new List<Log>();

                    foreach (var log in Listalog)
                    {
                        var itemLog = new Log();
                        itemLog.Codigo = log.COD_LOG;
                        itemLog.DataCriacao = Convert.ToDateTime(log.DATA_CRIACAO);
                        itemLog.Mensagem = log.MENSAGEM;
                        itemLog.Usuario = log.NOME_USUARIO;
                        itemLog.IndicadorStatus = Convert.ToBoolean(log.IND_ERRO);
                        itemLog.ParametroIntegracao = log.XML ?? log.PARAMETROS_INTEGRACAO;

                        itemLog.DescricaoStatus = "OK";

                        if (log.IND_ERRO == true)
                            itemLog.DescricaoStatus = "ERRO";
                        else
                        {
                            if (log.IND_ERRO == false && log.DESTINO_INTEGRACAO == "SIGA" && log.COD_CONTRATO_SIGA == 0) { itemLog.DescricaoStatus = "EM PROCESSAMENTO"; }

                        }

                        linha.Items.Add(itemLog);
                    }


                    lista.Add(linha);

                }




                return lista;

            }
            catch (Exception) { return null; }
        }

        public Boolean Inserir(LOG_INTEGRACAO log)
        {

            try
            {

                var li = new LOG_INTEGRACAO();
                li.COD_PRODUTO = log.COD_PRODUTO;
                li.DATA_CRIACAO = log.DATA_CRIACAO;
                li.IND_ERRO = log.IND_ERRO;
                li.MENSAGEM = log.MENSAGEM;
                li.NOME_USUARIO = log.NOME_USUARIO;
                li.ROTINA = log.ROTINA;
                li.XML = log.XML;
                li.COD_CONTRATO_SIGA = log.COD_CONTRATO_SIGA;
                li.COD_NOTIFICACAO = log.COD_NOTIFICACAO;
                li.COD_PEDIDO_API = log.COD_PEDIDO_API;
                li.COD_PEDIDO_BSELLER = log.COD_PEDIDO_BSELLER;
                li.IND_EXIBE_LOG = log.IND_EXIBE_LOG;

                li.DESCRICAO_INTEGRACAO = log.DESCRICAO_INTEGRACAO ?? "-";
                li.DESTINO_INTEGRACAO = log.DESTINO_INTEGRACAO ?? "-";
                li.PARAMETROS_INTEGRACAO = log.PARAMETROS_INTEGRACAO ?? "-";

                db.LOG_INTEGRACAO.Add(li);
                db.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public Boolean Inserir()
        {
            try
            {

                var li = new LOG_INTEGRACAO();
                li.COD_PRODUTO = COD_PRODUTO;
                li.DATA_CRIACAO = DATA_CRIACAO;
                li.IND_ERRO = IND_ERRO;
                li.MENSAGEM = MENSAGEM;
                li.NOME_USUARIO = NOME_USUARIO;
                li.ROTINA = ROTINA;
                li.XML = XML;
                li.COD_CONTRATO_SIGA = COD_CONTRATO_SIGA;
                li.COD_NOTIFICACAO = COD_NOTIFICACAO;
                li.COD_PEDIDO_API = COD_PEDIDO_API;
                li.COD_PEDIDO_BSELLER = COD_PEDIDO_BSELLER;
                li.DESCRICAO_INTEGRACAO = DESCRICAO_INTEGRACAO ?? "-";
                li.DESTINO_INTEGRACAO = DESTINO_INTEGRACAO ?? "-";
                li.PARAMETROS_INTEGRACAO = PARAMETROS_INTEGRACAO ?? "-";
                li.IND_EXIBE_LOG = IND_EXIBE_LOG;

                db.LOG_INTEGRACAO.Add(li);
                db.SaveChanges();
                return true;

            }
            catch (DbEntityValidationException)
            {
                return false;
            }

        }
    }
}

