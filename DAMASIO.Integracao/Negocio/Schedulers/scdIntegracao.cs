﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FluentScheduler;
using System.Web.Hosting;

namespace DAMASIO.Integracao.Negocio.Schedulers
{
    public class scdIntegracao : IJob, IRegisteredObject
    {

        private readonly object _lock = new object();
        private bool _shuttingDown;

        public scdIntegracao()
        {

            HostingEnvironment.RegisterObject(this);

        }
        public void Execute()
        {
            lock (_lock)
            {
                if (_shuttingDown)
                    return;

                // Descrever aqui a rotina

                //-------------------------
            }
        }

        public void Stop(bool immediate)
        {
            // Utilizar para cancelar o Job
            lock (_lock)
            {
                _shuttingDown = true;
            }

            HostingEnvironment.UnregisterObject(this);
        }


    }
}
