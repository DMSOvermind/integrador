﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAMASIO.Integracao.DAL;
using DAMASIO.Integracao.Dominio.API;


namespace DAMASIO.Integracao.Negocio
{
    public class Empresas
    {
        INTEGRACAOEntities db = new INTEGRACAOEntities();

        public Empresa ObterPeloCodigoSiga(Int16 Codigo) {


            try {

                

                var emp = (from e in db.CONFIG_EMPRESA_MARCA
                           where e.COD_EMPRESA_SIGA == Codigo
                           select e).FirstOrDefault();

                if (emp != null)
                {
                    Empresa empresa = new Empresa();
                    empresa.Codigo = emp.COD_EMPRESA_MARCA;
                    empresa.CodigoSiga = Convert.ToInt16(emp.COD_EMPRESA_SIGA);
                    empresa.CodigoMarcaBSeller = Convert.ToInt16(emp.COD_MARCA_BSELLER);
                    return empresa;
                }
                else return null;

                
            
            }
            catch (Exception ex) { return null; }


        
        
        
        }





    }
}
