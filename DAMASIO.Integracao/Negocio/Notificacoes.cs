﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAMASIO.Integracao.DAL;
using DAMASIO.Integracao.Dominio.dto;
using DAMASIO.Integracao.Dominio.BSeller;

namespace DAMASIO.Integracao.Negocio
{
    public class Notificacoes
    {
        INTEGRACAOEntities db = new INTEGRACAOEntities();

        Logs log = new Logs { DATA_CRIACAO = DateTime.Now, NOME_USUARIO = "Envio Automático/BSELLER", IND_ERRO = true, IND_EXIBE_LOG = false, DESTINO_INTEGRACAO = "SIGA", PARAMETROS_INTEGRACAO = "-", DESCRICAO_INTEGRACAO = "-" };

        public NOTIFICACAO Inserir(NotificacaoBSeller notificacao)
        {
            try
            {
                var Notificacao = new NOTIFICACAO
                {
                    COD_PEDIDO = notificacao.Codigo,
                    DATA_STATUS_BSELLER = notificacao.HoraAtualizacao,
                    DATA_INCLUSAO = DateTime.Now,
                    STATUS_PEDIDO = notificacao.Status,
                    XML_ORIGINAL = notificacao.Xml
                };

                db.NOTIFICACAO.Add(Notificacao);
                db.SaveChanges();

                log.COD_PEDIDO_BSELLER = Convert.ToInt64(notificacao.Codigo);
                log.DATA_CRIACAO = DateTime.Now;
                log.IND_ERRO = false;
                log.ROTINA = "NOTIFICACAO";
                log.MENSAGEM = "NOTIFICACAO";
                log.COD_NOTIFICACAO = Notificacao.COD_NOTIFICACAO;
                log.XML = notificacao.Xml;

                new Logs().Inserir(log);

                return Notificacao;
            }
            catch (Exception ex)
            {
#pragma warning disable 168
                log.ROTINA = "NOTIFICACAO";
                log.IND_ERRO = true;
                log.MENSAGEM = ex.Message;

                return null;
            }
#pragma warning restore 168
        }
    }
}
