﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DAMASIO.Integracao.Dominio.dto
{
    public class XmlDTO
    {
        public String OrderNumber { get; set; }
        public String OrderState { get; set; }
        public String TimeStamp { get; set; }

    }
}
