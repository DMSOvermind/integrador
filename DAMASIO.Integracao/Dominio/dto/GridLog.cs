﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.dto
{
    public class GridLog
    {

        public Int32 Codigo { get; set; }
        public Int32 CodigoProduto { get; set; }
        public string Status { get; set; }
        public Boolean Pendente { get; set; }
        public DateTime DataLog { get; set; }
        public DateTime? DataUltimaAtualizacao { get; set; }
        public string Destino { get; set; }
        public string Solicitacao { get; set; }
        public string Turma { get; set; }
        public Boolean EmExecucao { get; set; }


    }
}
