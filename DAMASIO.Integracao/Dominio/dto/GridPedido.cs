﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.dto
{
    public class GridPedido
    {
        public Int64 Codigo { get; set; }
        public string CodigoBSeller { get; set; }
        public DateTime DataPedido { get; set; }

        public Int64 CodigoAlunoBSeller { get; set; }
        public string Documento { get; set; }
        
        public string Nome { get; set; }
        public string SituacaoBSeller { get; set; }

        public int CodigoSituacaoSIGA { get; set; }
        public string SituacaoSIGA { get; set; }
        public string ValorVenda { get; set; }

        public string ToolTipMessage { get; set; }


    }
}
