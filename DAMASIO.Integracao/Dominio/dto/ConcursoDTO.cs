﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.dto
{
    public class ConcursoDTO
    {

        public Int32 Codigo {get;set;}
        public string Nome {get;set;}
        public string Status {get;set;}
        public string NomeBancaExaminadora {get;set;}
        public DateTime DataInicioInscricao {get;set;}
        public DateTime DataFimInscricao {get;set;}
        public decimal ValorTaxaInscricao {get;set;}
        public string UrlOrganizadora {get;set;}
        public string UrlEdital1 {get;set;}
        public string UrlEdital2 {get;set;}
        public string UrlEdital3 {get;set;}
        public string UrlEdital4 {get;set;}
        public string NomeLocalidade {get;set;}
        public int CodigoAbrangencia {get;set;}
        public string DescricaoAbrangencia {get;set;}
        public string ListaCurso {get;set;}
        public string ListaTurma {get;set;}
        public string ListaCargo {get;set;}
        public string ListaFase {get;set;}
        public string ListaGrupoCurso {get;set;}
        
    }
}
