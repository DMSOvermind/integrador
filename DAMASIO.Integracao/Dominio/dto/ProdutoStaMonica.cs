﻿using DAMASIO.Integracao.Dominio.BSeller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.dto
{
    public class ProdutoStaMonica
    {
        public string Nome { get; set; }
        public Int64 IdBseller { get; set; }
        public string CodigoTurma { get; set; }
        public string Descricao { get; set; }
        public List<image> Images { get; set; }
        public Categoria CategoriaPrincipal { get; set; } 



    }
}
