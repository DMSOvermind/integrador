﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.dto
{
    public class PesquisaConcursoDTO
    {

        public string NomeConcurso { get; set; }
        public string NomeCargo { get; set; }
        public Int32 Abrangencia { get; set; }
        public string Localidade { get; set; }
        public Int32 Escolariadade { get; set; }
        public Int32 Status { get; set; }
        public string SalarioDE { get; set; }
        public string SalarioATE { get; set; }
        public string Banca { get; set; }
        
        [DefaultValue(false)]
        public bool RetornarTodos { get; set; }

    }
}
