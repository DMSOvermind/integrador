﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.dto
{
    public class PesquisaUnidadeDTO
    {
        public Int32? CodigoUnidade { get; set; }
        public string Uf { get; set; }


    }
}
