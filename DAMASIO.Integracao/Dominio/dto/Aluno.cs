﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.dto
{
    public class Aluno
    {
        public Int64 ID { get; set; }
        public Int64 Codigo { get; set; }
        public Int64 CodigoBSeller { get; set; }
        public string Nome { get; set; }
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Email inválido.")]
        public string Email { get; set; }
        public string Documento { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string DataNascimento { get; set; }
        public string Sexo { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public string DataCadastro { get; set; }
        public string Telefone1 { get; set; }
        public string Telefone2 { get; set; }
        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string CEP { get; set; }


        //public string COBLogradouro { get; set; }
        //public string COBBairro { get; set; }
        //public string COBCidade { get; set; }
        //public string COBEstado { get; set; }
        //public string COBCEP { get; set; }
        //public string COBNumero { get; set; }
        //public string COBComplemento { get; set; }


        public Aluno Responsavel { get; set; }





    }
}
