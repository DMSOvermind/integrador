﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.dto
{
    public class ProdutoDTO
    {

        public Int64 Codigo{get;set;}
        public String Descricao { get; set; }
        public DateTime DataCadastro { get; set; }
        public Decimal ValorOriginal { get; set; }
        public Decimal ValorVenda { get; set; }

        public String CodigoExterno { get; set; }


    }
}
