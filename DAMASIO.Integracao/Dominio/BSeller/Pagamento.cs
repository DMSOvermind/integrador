﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class Pagamento
    {

        public String TipoPagamento {get;set;}
        public String MeioPagamento {get;set;}
        public String NomeGateway {get;set;}
        public String TokenGateway {get;set;}
        public String ValorPagamento {get;set;}
        public String IdTransacao {get;set;}
        public String SequencialTransacao {get;set;}
        public String NumeroAutorizacao { get; set; }
        public String MensagemAutorizacao { get; set; }
        public String LinkBoleto {get;set;}
        public String Bandeira {get;set;}
        public String NomeClienteCartao {get;set;}
        public String NumeroClienteCartao {get;set;}
        public String CodigoSegurancaCartao {get;set;}
        public String DataExpiracaoCartao {get;set;}
        public String Parcelas {get;set;}
        public decimal TaxaJuros {get;set;}
        public decimal ValorJuros { get; set; }
        public decimal ValorTotal { get; set; }


        public String AcquirerTransactionID { get; set; }
        public String NumProofOfSale { get; set; }
        public String ReturnCode { get; set; }
    }
}
