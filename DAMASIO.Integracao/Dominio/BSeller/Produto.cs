﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class Produto
    {
        public Int64 Codigo { get; set; }
        public Int64 SkuID { get; set; }

        public String Nome { get; set; }
        public String Descricao { get; set; }
        
        public DateTime DataCadastro { get; set; }
        public DateTime DataUltimaAlteracao { get; set; }

        public Decimal ValorOriginal { get; set; }
        public Decimal ValorVenda { get; set; }

        public String CodigoExterno { get; set; }

        public String DescricaoReduzida { get; set; }

        public Int64 CodigoFornecedor { get; set; }
        public String NomeFornecedor { get; set; }

        public Int64 CodigoCategoria { get; set; }
        public String NomeCategoria { get; set; }


        public bool DisponivelParaVenda { get; set; }
        public bool Oculto { get; set; }

        public bool Kit { get; set; }

    }
}
