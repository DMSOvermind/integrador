﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class CarrinhoCompra
    {

        public String Codigo { get; set; }
        public List<ItemPedido> ItensPedido { get; set; }
        public int TempoEntrega { get; set; }
        public decimal CustoFrete { get; set; }
        public decimal DescontoFrete { get; set; }
        public decimal ValorTotalFrete { get; set; }


        public Decimal ValorTotalDescontos { get; set; }
        public Decimal ValorTotal{ get; set; }

        public Entrega Entrega { get; set; }

        public Pagamento Pagamento { get; set; }

        public String Cupom { get; set; }
        public String CodigoPromocao { get; set; }
        public String NomePromocao { get; set; }


    }
}
