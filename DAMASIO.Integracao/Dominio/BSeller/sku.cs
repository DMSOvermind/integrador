﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class sku
    {

        public Int64 id { get; set; }
        public String variation { get; set; }

        public Dimensao dimension { get; set; }
        public Preco Price { get; set; }

    }
}
