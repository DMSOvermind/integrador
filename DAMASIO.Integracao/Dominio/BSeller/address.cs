﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class endereco
    {
        public string id { get; set; }
        public string contactName { get; set; }
        public string addressName { get; set; }
        public string address { get; set; }
        public string number { get; set; }
        public string province { get; set; }
        public string complement { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
        public string reference { get; set; }

    }
}
