﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class ProdutoBSeller
    {

        public Int64 id { get; set; }
        public String externalId { get; set; }
        public String name { get; set; }
        public DateTime dateCreated { get; set; }
        public DateTime lastUpdated { get; set; }
        public String description { get; set; }
        public String shortName { get; set; }
        
        

        public Preco price { get; set; }
        public Categoria category { get; set; }
        public List<Categoria> secondaryCategories { get; set; }

        public List<image> images { get; set; }

        public Marca brand { get; set; }

        public Boolean inventoryTracking { get; set; }
        public Boolean saleable { get; set; }
        public Boolean hidden { get; set; }

        public String variationMultiplicity { get; set; }

        public Boolean isKit { get; set; }

        public String catalogUrl { get; set; }

        public List<sku> skus { get; set; }

        public Info info { get; set; }



    }
}
