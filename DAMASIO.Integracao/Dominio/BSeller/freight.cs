﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class freight
    {
        public int time { get; set; }
        public decimal amount { get; set; }
        public decimal discount { get; set; }
        public decimal totalAmount { get; set; }

    }
}
