﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class Endereco
    {
        public String Codigo { get; set; }
        public String NomeContato { get; set; }
        public String Identificador { get; set; }
        public String Logradouro { get; set; }
        public String Numero { get; set; }
        public String Bairro { get; set; }
        public String Complemento { get; set; }
        public String Cidade { get; set; }
        public String Estado { get; set; }
        public String CodigoPostal { get; set; }
        public String PontoDeReferencia { get; set; }


    }
}
