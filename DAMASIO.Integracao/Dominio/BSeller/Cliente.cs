﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class Cliente
    {

        public String Codigo { get; set; }
        public String CodigoExterno { get; set; }
        public String Nome { get; set; }
        public String Apelido { get; set; }
        public String CpfCNPJ { get; set; }

        public Boolean PessoaFisica { get; set; }

        public String Email { get; set; }
        public Boolean Optin { get; set; }
        public String Sexo { get; set; }
        public DateTime Nascimento { get; set; }

        public String RazaoSocial { get; set; }
        public String InscricaoEstadual { get; set; }

        public Boolean SimplesNacional { get; set; }

        public String TagsCliente { get; set; }

        public String TelefoneResidencial { get; set; }
        public String TelefoneCelular { get; set; }
        public String TelefoneComercial { get; set; }

        public String DDD { get; set; }
        public String Numero { get; set; }

    }
}
