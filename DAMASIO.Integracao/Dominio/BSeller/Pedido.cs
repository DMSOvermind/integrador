﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class Pedido
    {

        public String Codigo { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataAtualizacao { get; set; }
        public String Situacao { get; set; }

        public int CodigoSitucaoBSELLER { get; set; }
        public int CodigoSitucaoSIGA { get; set; }


        public String CanalVenda { get; set; }
        public String TagCliente { get; set; }

        public Cliente Cliente { get; set; }
        public Endereco EnderecoCobranca { get; set; }
        public Endereco EnderecoEnvio { get; set; }

        public CarrinhoCompra Carrinho { get; set; }

        public Entrega Entrega { get; set; }

        public Pagamento Pagamento { get; set; }




    }
}
