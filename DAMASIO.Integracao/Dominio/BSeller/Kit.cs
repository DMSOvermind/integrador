﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class Kit
    {
        public String CodigoProduto { get; set; }
        public String CodigoSKU { get; set; }
        public String NomeProduto { get; set; }
        public String Variacao { get; set; }

        public Decimal PrecoUnitario { get; set; }
        public Decimal PercentualTotalKIT { get; set; }

        public int Quantidade { get; set; }

    }
}
