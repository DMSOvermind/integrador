﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class Order
    {

        public Int32 id { get; set; }
        public DateTime dateCreated { get; set; }
        public DateTime lastUpdated { get; set; }
        public String status { get; set; }

        public Customer customer { get; set; }
        
        public addresses addresses { get; set; }

        public Cart cart { get; set; }

        public Payment payment { get; set; }



    }
}
