﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class NotificacaoBSeller
    {
        public String Codigo { get; set; }
        public String Status { get; set; }
        public DateTime HoraAtualizacao { get; set; }
        public string Xml { get; set; }

    }
}
