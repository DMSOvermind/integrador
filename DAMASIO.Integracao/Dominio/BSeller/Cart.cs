﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class Cart
    {

        public Guid cartId { get; set; }
        public List<Orderline> OrderLines { get; set; }

        public List<Promotion> promotions { get; set; }

        public freight freight { get; set; }

        public Decimal totalDiscountAmount { get; set; }
        public Decimal totalInterestAmount { get; set; }
        public Decimal totalAmount { get; set; }
        public string cupom { get; set; }


    }
}
