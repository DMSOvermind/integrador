﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class Orderline
    {
        public string name { get; set; }
        public string variation { get; set; }
        public Int32 skuId { get; set; }
        public Int32 productId { get; set; }
        public int quantity { get; set; }
        public bool isKit { get; set; }
        public decimal listPrice { get; set; }
        public decimal salePrice { get; set; }
        public decimal discount { get; set; }
        public decimal totalAmount { get; set; }

    }
}
