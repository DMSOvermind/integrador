﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class Customer
    {
        public Int32 id { get; set; }
        public bool isPhysicalPerson { get; set; }
        public string name { get; set; }
        public string nickName { get; set; }
        public string document { get; set; }
        public string email { get; set; }
        public string gender { get; set; }
        public DateTime birthdate { get; set; }
        public Phone phone { get; set; }

        



    }
}
