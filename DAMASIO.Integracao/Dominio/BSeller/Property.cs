﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class Property
    {
        ////[XmlElement("name")]
        //public string name { get; set; }
        ////[XmlElement("value")]
        //public string value { get; set; }


        //Como a property não é serializada automaticamente, resolvi montar individualmente 
        // para cada property criada no site é necessário alterar esta classe

        public Propriedades CargaHoraria { get; set; }
        public Propriedades Conteudo { get; set; }
        public Propriedades Tipo { get; set; }
        public Propriedades Venda { get; set; }
        public Propriedades Curso { get; set; }

        public Propriedades Diferenciais { get; set; }
        public Propriedades Objetivos { get; set; }
        public Propriedades Publico { get; set; }
        public Propriedades Requisitos { get; set; }
        public Propriedades Video { get; set; }


    }
}
