﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class Payment
    {
        public string type { get; set; }
        public string paymentType { get; set; }
        public string gateway { get; set; }
        public string token { get; set; }

        public string transactionId { get; set; }
        public string sequencialNumber { get; set; }
        
        public CreditCard creditCard { get; set; }

        public decimal amount { get; set; }

        public income income { get; set; }

        public List<prop> paymentProps { get; set; }



    }
}
