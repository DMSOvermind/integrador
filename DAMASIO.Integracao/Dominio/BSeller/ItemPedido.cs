﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class ItemPedido
    {
        public String Nome { get; set; }
        public String Variacao { get; set; }
        public String CodigoSKU { get; set; }
        public String CodigoProduto { get; set; }
        public String IdAgregacao { get; set; }
        public String DonoAgregacao { get; set; }
        public Boolean Kit { get; set; }

        public int Quantidade { get; set; }
        public decimal PrecoDaLista { get; set; }
        public decimal PrecoDeVenda { get; set; }
        public decimal Desconto { get; set; }
        public decimal ValorTotal { get; set; }


    }
}
