﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class Entrega
    {
        public string Tipo { get; set; }
        public DateTime DataPrevista { get; set; }
        public decimal Valor { get; set; }
        public string Rastreamento { get; set; }

    }
}
