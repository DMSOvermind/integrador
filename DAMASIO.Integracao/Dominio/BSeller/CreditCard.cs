﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class CreditCard
    {
        public int parcels { get; set; }
        public decimal interestRate { get; set; }
        public decimal interestAmount { get; set; }
        public decimal totalAmount { get; set; }

        public string binNumber { get; set; }

        public string brand { get; set; }
        public string ownerName { get; set; }

        public string number { get; set; }
        public string pin { get; set; }
        public string expirationDate { get; set; }

    }
}
