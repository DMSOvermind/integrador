﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.BSeller
{
    public class Dimensao
    {
        public Int32 weight { get; set; }
        public Int32 width { get; set; }
        public Int32 height { get; set; }
        public Int32 depth { get; set; }

    }
}
