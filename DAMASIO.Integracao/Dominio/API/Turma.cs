﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class Turma
    {
        public string Codigo { get; set; }
        public string Descricao { get; set; }

    }
}
