﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class ConvenioUnidade
    {

        public Int32 Codigo { get; set; }
        public String Nome  { get; set; }
        public String Beneficio { get; set; }
        public String Vigencia { get; set; }


    }
}
