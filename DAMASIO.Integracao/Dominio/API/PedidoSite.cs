﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class PedidoSite
    {

        public Int64 CodigoBSeller { get; set; }
        public Int64 Codigo { get; set; }

        public DateTime DataPedido { get; set; }
        public DateTime DataAtualizacao { get; set; }

        public string Situacao { get; set; }


        public decimal ValorPedido { get; set; }
        public decimal Desconto { get; set; }
        public string CupomUtilizado { get; set; }
        public string Promocoes { get; set; }
        public decimal Frete { get; set; }

        public decimal ValorVenda { get; set; }


        public List<ItensPedido> Itens { get; set; }
        public List<Pagamento> Pagamento { get; set; }


    }
}
