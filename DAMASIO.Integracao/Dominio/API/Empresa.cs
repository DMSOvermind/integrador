﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class Empresa
    {

        public Int64 Codigo { get; set; }
        public String Nome { get; set; }
        public int  CodigoSiga { get; set; }
        public int CodigoMarcaBSeller { get; set; }


    }
}
