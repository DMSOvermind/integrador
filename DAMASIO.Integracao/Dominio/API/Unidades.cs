﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class Unidades
    {
        public String Codigo { get; set; }
        public String Estado { get; set; }

        public String Logradouro { get; set; }
        public String Número { get; set; }
        public String Complemento { get; set; }
        public String Bairro { get; set; }
        public String CEP { get; set; }
        public String Cidade { get; set; }


        public String Telefone1 { get; set; }
        public String Telefone2 { get; set; }
        public String Telefone3 { get; set; }

        public String Email { get; set; }

        public String TwitterPage { get; set; }
        public String FacebookPage { get; set; }
        public String GooglePage { get; set; }





    }


    

}
