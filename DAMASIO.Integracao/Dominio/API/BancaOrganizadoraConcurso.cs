﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class BancaOrganizadoraConcurso
    {

        public string Nome { get; set; }
        public string Url { get; set; }

    }
}
