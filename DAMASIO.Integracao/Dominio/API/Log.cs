﻿using DAMASIO.Integracao.DAL;
using DAMASIO.Integracao.Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class Log
    {

        public Int64 Codigo { get; set; }
        public DateTime DataCriacao { get; set; }
        public string Mensagem { get; set; }
        public string DescricaoStatus { get; set; }
        public bool IndicadorStatus { get; set; }
        public string ParametroIntegracao { get; set; }
        public string Usuario { get; set; }


    }
}
