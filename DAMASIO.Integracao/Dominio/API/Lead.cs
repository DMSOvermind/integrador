﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class Lead
    {

        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public string Mensagem { get; set; }
        public Boolean ExameOrdem { get; set; }
        public Boolean Graduacao { get; set; }
        public Boolean CarreirasPublicas { get; set; }
        public Boolean PosGraduacao { get; set; }
        public Boolean CarreirasInternacionais { get; set; }
        public Boolean CarreirasJuridicas { get; set; }
        public string CursoSelecionado { get; set; }
        public int CodigoUnidade { get; set; }
        



    }
}
