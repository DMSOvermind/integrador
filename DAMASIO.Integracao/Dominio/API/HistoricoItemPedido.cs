﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class HistoricoItemPedido
    {
        public string Data { get; set; }
        public string Descricao { get; set; }
        public string Status { get; set; }
        public string Usuario { get; set; }

    }
}
