﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class Listas
    {

        public enum Rotinas {
        
            AtualizarEstoqueBSeller = 1,
            ObterDadosProdutoPeloIdExterno = 2,
            AtualizarProduto = 3,
            AtualizarValorProduto = 4,
            ObterDadosProdutoPeloIdBSELLER = 5,
            ObterPedido = 6
        
        }

        public enum StatusContratoItem
        {

            NaoEncontrado = 0,
            Encontrado = 1,
            ItemInexistente = 2,
            PedidoInexistente = 3

        }



        public enum StatusPedidoSiga
        {
        
            Pendente = 0,
            ErroNoEnvio = 1,
            EnviadoComSucesso = 2,
            Cancelado = 3,
            NaoEnviado = 4
        
        }

        public enum StatusPedidoBSeller  
        {

            NovoPedido = 10,
            AguardandoPagamento = 20,
            PagamentoRecusado = 30,
            PagamentoAprovado = 40,
            Cancelado = 50  

        }





    }
}
