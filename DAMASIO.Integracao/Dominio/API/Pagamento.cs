﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class Pagamento
    {

        public string FormaDePagamento { get; set; }
        public int QuantidadeParcelas { get; set; }
        public decimal ValorParcela { get; set; }
        public decimal TaxaJuros { get; set; }
        public decimal ValorJuros { get; set; }
        public decimal ValorTotal { get; set; }
        public string NumeroCartao { get; set; }
        public string Bandeira { get; set; }
        public string idTransacao { get; set; }
        public string idAutorizacao { get; set; }
        public string idTransacaoAdquirente { get; set; }

    }
}
