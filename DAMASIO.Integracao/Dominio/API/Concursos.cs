﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class Concursos
    {

        public string NomeConcurso { get; set; }
        //public string NomeDoCargo { get; set; }
        //public int NumeroDeVagas { get; set; }
        //public decimal Salario { get; set; }
        public List<Cargo> Cargo { get; set; }
        //public string Escolaridade { get; set; }
        //public string Requisitos { get; set; }
        public List<FasesConcurso> Fases { get; set; }
        public string Status { get; set; }
        public string InicioInscricoes { get; set; }
        public string TerminoInscricoes { get; set; }
        //public string DataProva { get; set; }
        public decimal TaxaInscricao { get; set; }
        public string UrlOrganizadora { get; set; }
        public string UrlEdital_1 { get; set; }
        public string UrlEdital_2 { get; set; }
        public string UrlEdital_3 { get; set; }
        public string UrlEdital_4 { get; set; }
        public string Localidade { get; set; }
        public string Abrangencia { get; set; }

        public List<GrupoDeCurso> GrupoDeCurso { get; set; }
        public List<Curso> Cursos { get; set; }
        public List<Turma> Turmas { get; set; }


    }
}
