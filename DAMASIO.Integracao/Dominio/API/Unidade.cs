﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class Unidade
    {

        public Int32 Codigo { get; set; }
        public String Nome { get; set; }
        public String Logradouro { get; set; }
        public String Numero { get; set; }
        public String Complemento { get; set; }
        public String Bairro { get; set; }
        public String CEP { get; set; }
        public String Cidade { get; set; }
        public String Estado { get; set; }

        public String Telefone1 { get; set; }
        public String Telefone2 { get; set; }
        public String Telefone3 { get; set; }

        public String Email { get; set; }

        public List<ConvenioUnidade> Convenio { get; set; }

        public String Twitter { get; set; }
        public String Facebook { get; set; }
        public String GooglePlus { get; set; }



    }
}
