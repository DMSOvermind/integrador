﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class Cargo
    {
        public string Nome { get; set; }
        public decimal SalarioDE { get; set; }
        public decimal SalarioATE { get; set; }
        public Int32 NumeroDeVagas { get; set; }
        public string Requisitos { get; set; }
        public string Escolaridade { get; set; }

    }
}
