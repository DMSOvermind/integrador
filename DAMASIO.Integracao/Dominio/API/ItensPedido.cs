﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class ItensPedido
    {

        public Int64 CodigoPedido { get; set; }
        public Int64 CodigoProdutoBSeller { get; set; }
        public string CodigoProduto { get; set; }

        public string ContratoSIGA { get; set; }
        
        public string Descricao { get; set; }
        public int Quantidade { get; set; }
        public int Sequencial { get; set; }

        public decimal ValorUnitario { get; set; }
        public decimal Desconto { get; set; }

        public decimal ValorVenda { get; set; }
        public decimal ValorTotal { get; set; }

        public Int32 CodigoSituacao { get; set; }
        public string Situacao { get; set; }



    }
}
