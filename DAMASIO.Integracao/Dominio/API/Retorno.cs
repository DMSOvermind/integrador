﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class Retorno
    {

        public Int64 codigo { get; set; }
        public String message { get; set; }
        public Boolean success { get; set; }
        public String StatusCodeRequest { get; set; }
        public String MessageRequest { get; set; }
        public Object ResponseServer { get; set; }
        

    }
}
