﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMASIO.Integracao.Dominio.API
{
    public class HistoricoItemLog
    {

        public Int32 CodigoProduto { get; set; }
        public string Destino { get; set; }
        public string Solicitacao { get; set; }
        public string Turma { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataUltimaAtualizacao { get; set; }
        public List<Log> Items { get; set; }

    }
}
