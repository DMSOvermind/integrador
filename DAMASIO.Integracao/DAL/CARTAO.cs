//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAMASIO.Integracao.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class CARTAO
    {
        public long COD_CARTAO { get; set; }
        public Nullable<long> COD_PEDIDO_PAGAMENTO { get; set; }
        public string NOME_BANDEIRA { get; set; }
        public string NOME_CLIENTE { get; set; }
        public string NUM_CARTAO { get; set; }
        public string NUM_PIN { get; set; }
        public string EXPIRACAO_CARTAO { get; set; }
    
        public virtual PEDIDO_PAGAMENTO PEDIDO_PAGAMENTO { get; set; }
    }
}
