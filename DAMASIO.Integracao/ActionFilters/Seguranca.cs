﻿using DAMASIO.Integracao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace DAMASIO.Integracao.ActionFilters
{
    public class Seguranca : ActionFilterAttribute
    {

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.Authorization == null)
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }
            else
            {
                var token = actionContext.Request.Headers.Authorization.Parameter;
                var tokenDecodificado = Encoding.UTF8.GetString(Convert.FromBase64String(token));

                var login = tokenDecodificado.Substring(0, tokenDecodificado.IndexOf(":", StringComparison.Ordinal));
                var senha = tokenDecodificado.Substring(tokenDecodificado.IndexOf(":", StringComparison.Ordinal) + 1);

                var user = ValidarUsuario(login, senha);
                if (user)
                {

                    //var minhaIdentidade = new GenericIdentity(user.Email);
                    //var principal = new GenericPrincipal(minhaIdentidade, user.Permissoes);
                    //Thread.CurrentPrincipal = principal;

                    base.OnActionExecuting(actionContext);
                }
                else
                {
                    actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }
            }
        }

        private bool ValidarUsuario(string Login, string Senha) {

            var integraSiga = new wsSiga.WsIntegracaoPortalSoapClient();

            var encUsuario = Converters.Base64Encode(Login);
            var encPassword = Converters.Base64Encode(Senha);

            var acesso = integraSiga.VerificarAcessoIntegrador(encUsuario, encPassword);

            return acesso.TemAcesso;

        
        }



    }
}
