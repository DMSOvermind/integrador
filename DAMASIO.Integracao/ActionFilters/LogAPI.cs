﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using System.Diagnostics;
using System.Web.Routing;
using DAMASIO.Integracao.DAL;
using DAMASIO.Integracao.Negocio;
using System.Net;
using System.Web.Helpers;
using System.Net.Http;
using System.IO;

namespace DAMASIO.Integracao.ActionFilters
{

    public class LogAPI : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            GerarLogAction("OnActionExecuted", actionExecutedContext);
        }

        private static void GerarLogAction(string methodName, HttpActionExecutedContext values)
        {
            try
            {
                var objectContent = values.Response.Content as ObjectContent;

                if (objectContent != null)
                {
                    var type = objectContent.ObjectType;
                    var value = values.Request.Content.ReadAsStreamAsync().Result;
                    var httpContent = new StreamReader(value).ReadToEnd();
                    value.Position = 0;
                    var log = new LOG_INTEGRACAO
                                             {
                                                 DATA_CRIACAO = DateTime.Now,
                                                 XML = httpContent,
                                                 IND_ERRO = false,
                                                 ROTINA = "LOGAPI",
                                                 IND_EXIBE_LOG = false,
                                                 DESTINO_INTEGRACAO = "SIGA",
                                                 DESCRICAO_INTEGRACAO = "-",
                                                 PARAMETROS_INTEGRACAO = "-"
                                             };
                    new Logs().Inserir(log);
                }
            }
            catch (Exception ex)
            {

                var log = new LOG_INTEGRACAO
                                         {
                                             DATA_CRIACAO = DateTime.Now,
                                             MENSAGEM = ex.Message,
                                             IND_ERRO = true,
                                             ROTINA = "LOGAPI",
                                             IND_EXIBE_LOG = false,
                                             DESTINO_INTEGRACAO = "SIGA",
                                             DESCRICAO_INTEGRACAO = "-",
                                             PARAMETROS_INTEGRACAO = "-"
                                         };
            }
        }
    }
}
