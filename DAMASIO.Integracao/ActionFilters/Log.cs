﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;
using System.Web.Mvc;
using System.Web.Routing;

using DAMASIO.Integracao.DAL;
using DAMASIO.Integracao.Negocio;
using System.Net;

using System.Web.Helpers;

using System.Web;

namespace DAMASIO.Integracao.ActionFilters
{
    //public class RequestProprio : ActionFilterAttribute
    //{
    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        if (filterContext.HttpContext.Request.Url.Host != filterContext.HttpContext.Request.UrlReferrer.Host)
    //        {
    //            //Se o request não vier do próprio domínio, retorna um BAD Request
    //            new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Bad Request");
    //        }
    //    }
    //}



    public class Log : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            GerarLogAction("OnActionExecuting", filterContext);
        }


        private static void GerarLogAction(string methodName, ActionExecutingContext values)
        {

            try
            {

                // Obtem os parametros enviados
                var keys = values.RequestContext.HttpContext.Request.Headers.AllKeys;

                var requestVars = keys.Select(key => new KeyValuePair<string, string>(key, values.RequestContext.HttpContext.Request.Headers.Get(key)));

                var lista = new StringBuilder();

                foreach (var keyValuePair in requestVars)
                {
                    lista.Append(String.Format("{0} - {1} ///", keyValuePair.Key, keyValuePair.Value));
                }

                LOG_INTEGRACAO log = new LOG_INTEGRACAO { DATA_CRIACAO = DateTime.Now, MENSAGEM = lista.ToString() };

                new Logs().Inserir(log);

            }
            catch (Exception ex) {

                LOG_INTEGRACAO log = new LOG_INTEGRACAO { DATA_CRIACAO = DateTime.Now, MENSAGEM=ex.Message,IND_ERRO=true };
            
            }


            


        }


    }
}
